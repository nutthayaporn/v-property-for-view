const express = require("express");
const next = require("next");
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const nextI18NextMiddleware = require("next-i18next/middleware").default;
const nextI18next = require("./src/i18n");

const PORT = process.env.PORT || 3000;

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(nextI18NextMiddleware(nextI18next));
    server.get("*", (req, res) => {
      return handle(req, res);
    });
    server.listen(PORT, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${PORT}`);
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
