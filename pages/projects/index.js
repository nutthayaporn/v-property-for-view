import React from "react";
import anime from "animejs";
import _map from "lodash/map";
import _get from "lodash/get";
import _find from "lodash/find";
import _sortBy from "lodash/sortBy";

import {
  SectionProject,
  Layout,
  CardProject,
  MyWaypoint
} from "../../src/components";
import { toCodeColor } from "../../src/utils";
import { withMyTranslation } from "../../src/hoc";
import {
  fetchProjects,
  fetchProjectPriceList,
  fetchProjectLocationList,
  fetchProjectStatusList,
  fetchProjectTypeList
} from "../../src/services/projects";

const Project = props => {
  const {
    projects,
    priceRanges,
    locationNearBy,
    projectStatus,
    projectTypes,
    t
  } = props;

  return (
    <Layout projects={projects}>
      <div className="project-page">
        <SectionProject
          projects={projects}
          priceRanges={priceRanges}
          locationNearBy={locationNearBy}
          projectStatus={projectStatus}
          projectTypes={projectTypes}
          render={filterProject => {
            return (
              <MyWaypoint
                anime={[
                  () => {
                    anime({
                      targets: ".project-block-wrapper",
                      opacity: [0, 1],
                      translateX: [50, 0],
                      duration: 1000,
                      easing: "easeInOutCubic",
                      delay: function(el, i, l) {
                        return i * 150;
                      }
                    });
                  }
                ]}
              >
                <div className="container-project-silder">
                  <div className="container">
                    <div className="row">
                      {_map(filterProject, (project, index) => {
                        const image = _get(project, "thumbnail");
                        const logo = _get(project, "logo");
                        const tag = t(_get(project, "tag"));
                        const captions = _get(project, "captions");
                        const slug = _get(project, "slug");
                        const linkOut = _get(project, "linkOut");
                        const projectStatusId = _get(
                          project,
                          "projectStatus._id"
                        );
                        const currentStatus = _find(projectStatus, [
                          "_id",
                          projectStatusId
                        ]);
                        const fontColorStatus = toCodeColor(
                          _get(currentStatus, "fontColor")
                        );
                        const backgroundColorStatus = toCodeColor(
                          _get(currentStatus, "backgroundColor")
                        );
                        let link = `/projects/${slug}`;
                        if (linkOut) {
                          link = linkOut;
                        }
                        return (
                          <div className="col-12 col-md-6 col-lg-4 col-xl-3" key={index}>
                            <div
                              className="project-block-wrapper"
                              style={{
                                opacity: 0,
                                transform: "translateX(50px)"
                              }}
                            >
                              <a href={link} target="_blank">
                                <CardProject
                                  image={image}
                                  logo={logo}
                                  status={tag}
                                  captions={captions}
                                  slug={slug}
                                  fontColorStatus={fontColorStatus}
                                  backgroundColorStatus={backgroundColorStatus}
                                />
                              </a>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </MyWaypoint>
            );
          }}
        />
      </div>
    </Layout>
  );
};

Project.defaultProps = {};

Project.getInitialProps = async () => {
  const projects = await fetchProjects();
  const priceRanges = await fetchProjectPriceList();
  const locationNearBy = await fetchProjectLocationList();
  const projectStatus = await fetchProjectStatusList();
  const projectTypes = await fetchProjectTypeList();

  return { projects, priceRanges, locationNearBy, projectStatus, projectTypes };
};

export default withMyTranslation(Project);
