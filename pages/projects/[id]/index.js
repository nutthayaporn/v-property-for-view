import React from "react";
import _get from "lodash/get";
import _find from "lodash/find";
import _map from "lodash/map";
import _filter from "lodash/filter";
import _includes from "lodash/includes";

import { Layout } from "../../../src/components";
import { toCodeColor } from "../../../src/utils";
import { withMyTranslation } from "../../../src/hoc";
import { useTheme } from "../../../src/hooks/useTheme";

import {
  SectionBanner,
  SectionRegister,
  SectionMainConcept,
  SectionConcept,
  SectionProjectPlan,
  SectionRoomType,
  SectionLocation,
  SectionGallery,
  SectionVideo,
  SectionVirtualTour,
  SectionProgress,
  SectionRecommended
} from "../../../src/components/Project";

import {
  fetchProjects,
  fetchProjectBySlug,
  fetchProjectById,
  fetchProjectStatusList
} from "../../../src/services/projects";

const Project = props => {
  const { currentProject, projectStatus, projects, t } = props;
  const { mainColorTheme, buttonTextColorTheme } = useTheme();

  const showSections = _get(currentProject, "showSections");
  const displaySections = _map(showSections, (showSection, key) => {
    if (!showSection) return;
    return key;
  });

  const projectMainColor = toCodeColor(_get(currentProject, "mainColor"));
  const projectButtonTextColor = toCodeColor(
    _get(currentProject, "buttonTextColor")
  );
  mainColorTheme(projectMainColor);
  buttonTextColorTheme(projectButtonTextColor);

  const sectionBanner = _get(currentProject, "html.section.banner");
  const sectionRegister = _get(currentProject, "html.section.register");
  const sectionMainConcept = _get(currentProject, "html.section.mainConcept");
  const sectionConcept = _get(currentProject, "html.section.concept");
  const sectionProjectPlan = _get(currentProject, "html.section.projectPlan");
  const sectionLocation = _get(currentProject, "html.section.location");
  const sectionGallery = _get(currentProject, "html.section.gallery");
  const sectionVdo = _get(currentProject, "html.section.vdo");
  const sectionVirtualTour = _get(currentProject, "html.section.panorama");
  const sectionProgress = _get(currentProject, "html.section.construction");
  const sectionRecommended = _get(currentProject, "html.section.recommended");

  const logoImage = _get(currentProject, "logo");
  const emailReceiver = {
    to: _get(currentProject, "to"),
    cc: _get(currentProject, "cc"),
    bcc: _get(currentProject, "bcc")
  };

  return (
    <Layout projects={projects} logo={logoImage}>
      <div className="project-section-page">
        {_includes(displaySections, "banner") && (
          <SectionBanner data={sectionBanner} t={t} />
        )}
        {_includes(displaySections, "register") && (
          <SectionRegister
            data={sectionRegister}
            emailReceiver={emailReceiver}
            projectData={currentProject}
            t={t}
          />
        )}
        {_includes(displaySections, "main-concept") && (
          <SectionMainConcept data={sectionMainConcept} t={t} />
        )}
        {_includes(displaySections, "concept") && (
          <SectionConcept data={sectionConcept} t={t} />
        )}
        {_includes(displaySections, "project-plan") && (
          <SectionProjectPlan data={sectionProjectPlan} t={t} />
        )}
        {/* {_includes(displaySections, "room-type") && (
          <SectionRoomType data={sectionRoomType} t={t} />
        )} */}
        {_includes(displaySections, "gallery") && (
          <SectionGallery data={sectionGallery} t={t} />
        )}
        {_includes(displaySections, "vdo") && (
          <SectionVideo data={sectionVdo} t={t} />
        )}
        {_includes(displaySections, "panorama") && (
          <SectionVirtualTour data={sectionVirtualTour} t={t} />
        )}
        {_includes(displaySections, "location") && (
          <SectionLocation data={sectionLocation} t={t} />
        )}
        {_includes(displaySections, "construction") && (
          <SectionProgress data={sectionProgress} t={t} />
        )}
        {_includes(displaySections, "recommended") && (
          <SectionRecommended
            data={sectionRecommended}
            projectStatus={projectStatus}
            t={t}
          />
        )}
      </div>
    </Layout>
  );
};

Project.getInitialProps = async ({ query }) => {
  const mapField = async item => {
    let recommendedProject = _get(item, "recommended.recommendedProject");
    recommendedProject = await Promise.all(
      _map(recommendedProject, async project => {
        const id = _get(project, "_id");
        const result = await fetchProjectById(id);
        return result;
      })
    );
    return {
      ...item,
      mainColor: _get(item, "mainColor"),
      html: {
        section: {
          banner: _get(item, "banner"),
          register: _get(item, "register"),
          mainConcept: _get(item, "main-concept"),
          concept: _get(item, "concept"),
          projectPlan: _get(item, "project-plan"),
          // roomtype: _get(item, "room-type"),
          location: _get(item, "location"),
          gallery: _get(item, "gallery"),
          vdo: _get(item, "vdo"),
          panorama: _get(item, "panorama"),
          construction: _get(item, "construction"),
          recommended: {
            ..._get(item, "recommended"),
            recommendedProject
          }
        }
      }
    };
  };

  const slug = query.id;
  const currentProject = await fetchProjectBySlug(slug).then(item =>
    mapField(item)
  );
  const projectStatus = await fetchProjectStatusList();
  const projects = await fetchProjects();

  return { currentProject, projectStatus, projects };
};

export default withMyTranslation(Project);
