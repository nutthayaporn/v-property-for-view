import React from "react";
import { useRouter } from "next/router";

import { withMyTranslation } from "../../../src/hoc";
import { MyButton } from "../../../src/components";

const bg = "/static/images/thankyou-bg.jpg";

const Thankyou = props => {
  const { t } = props;
  const router = useRouter();
  return (
    <div className="thankyou-page" style={{ backgroundImage: `url(${bg})` }}>
      <h1>ขอบคุณที่ลงทะเบียน</h1>
      <p>
        กับโครงการของ V Property Development ทางเราจะติดต่อกลับท่านไปโดยเร็ว
      </p>
      <MyButton onClick={() => router.back()}>กลับสู่หน้าหลัก</MyButton>
    </div>
  );
};

Thankyou.getInitialProps = ({ req, res }) => {
  // const fullUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
  // console.log("fullUrl", fullUrl);
  // console.log("req.headers", req.headers);
  // if (fullUrl === req.headers.referer) {
  //   res.redirect("/");
  //   res.end();
  // }
};

export default withMyTranslation(Thankyou);
