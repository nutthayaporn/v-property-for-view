import React from "react";
import Head from "next/head";
import _map from "lodash/map";
import _get from "lodash/get";

import { Layout, MetaSocialShare } from "../src/components";
import { fetchHomePage } from "../src/services/homePage";
import SectionBanner from "../src/components/Home/SectionBanner";
import SectionProjects from "../src/components/Home/SectionProjects";
import SectionPromotion from "../src/components/Home/SectionPromotion";
import SectionAbout from "../src/components/Home/SectionAbout";
import SectionNews from "../src/components/Home/SectionNews";

import {
  fetchProjects,
  fetchProjectPriceList,
  fetchProjectLocationList,
  fetchProjectStatusList,
  fetchProjectTypeList
} from "../src/services/projects";
import { fetchPromotionList } from "../src/services/promotion";
import { fetchNewsList } from "../src/services/news";
import { withMyTranslation } from "../src/hoc";

const Index = props => {
  const {
    data,
    projects,
    priceRanges,
    locationNearBy,
    projectStatus,
    projectTypes,
    promotions,
    news,
    t
  } = props;

  const title =
    "V PROPERTY DEVELOPMENT ผู้นำด้านการพัฒนาคอนโดมิเนียมติดรถไฟฟ้า สุขุมวิท กรุงเทพมหานคร | บริษัท วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด โทร. 1298";
  const description =
    "V PROPERTY DEVELOPMENT ผู้นำด้านการพัฒนาคอนโดมิเนียมติดรถไฟฟ้า สุขุมวิท กรุงเทพมหานคร | บริษัท วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด โทร. 1298";
  const image = "https://www.kobkid.com/wp-content/uploads/2019/06/1.jpg";
  const path = process.env.baseUrl;

  const sectionBanner = _get(data, "banner");
  const sectionPromotion = _get(data, "promotion");
  const sectionNews = _get(data, "news");
  const sectionAbout = _get(data, "about");

  return (
    <Layout projects={projects}>
      <Head>
        <MetaSocialShare
          title={title}
          description={description}
          image={image}
          path={path}
        />
      </Head>
      <SectionBanner data={sectionBanner} t={t} />
      <SectionProjects
        projects={projects}
        priceRanges={priceRanges}
        locationNearBy={locationNearBy}
        projectStatus={projectStatus}
        projectTypes={projectTypes}
        t={t}
      />
      <SectionPromotion data={sectionPromotion} promotions={promotions} t={t} />
      <SectionAbout data={sectionAbout} t={t} />
      <SectionNews data={sectionNews} news={news} t={t} />
    </Layout>
  );
};

Index.getInitialProps = async ({ query }) => {
  let language = query.language;
  if (!language) {
    language = "en";
  }

  const data = await fetchHomePage();
  const projects = await fetchProjects();
  const priceRanges = await fetchProjectPriceList();
  const locationNearBy = await fetchProjectLocationList();
  const projectStatus = await fetchProjectStatusList();
  const projectTypes = await fetchProjectTypeList();
  const promotions = await fetchPromotionList();
  const news = await fetchNewsList();

  return {
    data,
    projects,
    priceRanges,
    locationNearBy,
    projectStatus,
    projectTypes,
    promotions,
    news
  };
};

export default withMyTranslation(Index);
