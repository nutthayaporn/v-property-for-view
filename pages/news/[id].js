import React, { useState } from "react";
import _find from "lodash/find";
import _get from "lodash/get";
import _map from "lodash/map";
import _slice from "lodash/slice";
import _findIndex from "lodash/findIndex";
import _size from "lodash/size";
import _isEmpty from "lodash/isEmpty";
import { useRouter } from "next/router";
import { Modal } from "antd";
import Head from "next/head";
import dayjs from "dayjs";

import {
  Layout,
  MyButton,
  BackgroundImage,
  Image,
  Icon,
  GalleryLightBox,
  Swiper,
  MetaSocialShare
} from "../../src/components";
import { Link } from "../../src/i18n";
import { fetchNewsList } from "../../src/services/news";
import { fetchProjects } from "../../src/services/projects";
import { withMyTranslation } from "../../src/hoc";
import { stripHtml } from "../../src/utils";

const News = props => {
  const { t, news, projects } = props;
  const [isOpenModal, setOpenModal] = useState(false);

  const router = useRouter();
  const id = router.query.id;
  const selectedNews = _find(news, ["id", id]);
  const selectedNewsIndex = _findIndex(news, ["id", id]);

  const publishedDate = dayjs(_get(selectedNews, "publishedDate")).format(
    "D MMMM YYYY"
  );
  const title = t(_get(selectedNews, "title"));
  const fullDescription = t(_get(selectedNews, "fullDescription"));
  const shortDescription = t(_get(selectedNews, "shortDescription"));
  const images = _get(selectedNews, "gallery");
  const showImages = _slice(images, 1, 5);
  const firstImage = _get(images, "0.url");
  const path = `${process.env.baseUrl}/news/${id}`;

  const prevNews = t(_get(news[selectedNewsIndex - 1], "title"));
  const nextNews = t(_get(news[selectedNewsIndex + 1], "title"));
  const prevNewsDate = dayjs(
    _get(news[selectedNewsIndex - 1], "publishedDate")
  ).format("D MMMM YYYY");
  const nextNewsDate = dayjs(
    _get(news[selectedNewsIndex + 1], "publishedDate")
  ).format("D MMMM YYYY");

  const handleClickPrevButton = () => {
    const index = _findIndex(news, ["id", id]);
    const nextIndex = index - 1;
    const nextNewsIndex = _get(news, `${nextIndex}.id`);
    // router.push(`/news/${nextNewsIndex}`);
    const href = `/news/${nextNewsIndex}`;
    const as = href;
    router.push(href, as, { shallow: true });
  };

  const handleClickNextButton = () => {
    const index = _findIndex(news, ["id", id]);
    const nextIndex = index + 1;
    const nextNewsIndex = _get(news, `${nextIndex}.id`);
    // router.push(`/news/${nextNewsIndex}`);
    const href = `/news/${nextNewsIndex}`;
    const as = href;
    router.push(href, as, { shallow: true });
  };

  const stripHtmlDescription = stripHtml(shortDescription);
  return (
    <Layout projects={projects}>
      <Head>
        <title>{title}</title>
        {/* <meta name="description" content={stripHtmlDescription} /> */}
        <MetaSocialShare
          title={title}
          description={stripHtmlDescription}
          image={firstImage}
          path={path}
          date={publishedDate}
        />
      </Head>
      <div className="news-detail-page">
        <Modal
          wrapClassName="modal-gallery-light-box"
          centered
          footer={null}
          visible={isOpenModal}
          onOk={() => setOpenModal(false)}
          onCancel={() => setOpenModal(false)}
          destroyOnClose
        >
          <GalleryLightBox images={images} />
        </Modal>
        <div className="container">
          <div className="back-block">
            <Link href="/news" as="/news">
              <div>
                <MyButton type="arrow-left" />
              </div>
            </Link>
            <p className="text-back">{t("Back to Promotion & News")}</p>
          </div>
          <div className="container-content">
            <div className="news-header">
              <p className="date">{publishedDate}</p>
              <p className="title">{title}</p>
            </div>

            <div className="news-images">
              <div className="news-images-desktop">
                <div className="container-first-image">
                  <Image src={firstImage} />
                </div>
                {!_isEmpty(showImages) && (
                  <div className="container-image-thumbnail">
                    <div className="row no-gutters" style={{ height: "100%" }}>
                      {_map(showImages, (item, index) => {
                        // const isLast = index === 2;
                        const isLast = _size(showImages) === index + 1;
                        const image = _get(item, "url");
                        return (
                          <div
                            key={index}
                            className={`col-3 ${isLast ? "last-image" : ""}`}
                            onClick={() => setOpenModal(true)}
                          >
                            <div className="image-thumbnail-block">
                              <Image src={image} />
                            </div>
                            {isLast && (
                              <div className="button-plus">
                                View More
                                {/* <MyButton type="plus" /> */}
                              </div>
                            )}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                )}
              </div>
              <div className="news-images-mobile">
                <Swiper
                  items={images}
                  uiRenderer={(item, key) => {
                    const image = _get(item, "url");
                    return (
                      <div className="image-mobile-block" key={key}>
                        <BackgroundImage image={image} />
                      </div>
                    );
                  }}
                />
              </div>
            </div>
            <div className="news-description">
              <div
                className="description"
                dangerouslySetInnerHTML={{ __html: fullDescription }}
              />
            </div>
            <div className="container-social">
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://social-plugins.line.me/lineit/share?url=" +
                      encodeURIComponent(path),
                    "facebook-share-dialog",
                    "width=600,height=500"
                  );
                  return false;
                }}
              >
                <Icon type="line" color="white" />
              </div>
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://www.facebook.com/sharer/sharer.php?u=" +
                      encodeURIComponent(path),
                    "facebook-share-dialog",
                    "width=626,height=436"
                  );
                  return false;
                }}
              >
                <Icon type="facebook" color="white" />
              </div>
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://twitter.com/intent/tweet?text=" +
                      encodeURIComponent(title) +
                      "&amp;url=" +
                      encodeURIComponent(path),
                    "Twitter-dialog",
                    "width=626,height=436"
                  );
                  return false;
                }}
              >
                <Icon type="twitter" color="white" />
              </div>
            </div>
            <div className="container-pagination">
              <div className="pagination-block left-block">
                {prevNews && (
                  <>
                    <div
                      className="button-block"
                      onClick={handleClickPrevButton}
                    >
                      <MyButton type="arrow-left" />
                    </div>
                    <div className="text-block">
                      <p className="date-pagination">{prevNewsDate}</p>
                      <p className="title-pagination">{prevNews}</p>
                    </div>
                  </>
                )}
              </div>
              {nextNews && (
                <>
                  <div className="pagination-block right-block">
                    <div
                      className="button-block"
                      onClick={handleClickNextButton}
                    >
                      <MyButton type="arrow-right" />
                    </div>
                    <div className="text-block">
                      <p className="date-pagination">{nextNewsDate}</p>
                      <p className="title-pagination">{nextNews}</p>
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

News.getInitialProps = async () => {
  const news = await fetchNewsList();
  const projects = await fetchProjects();
  return { news, projects };
};

export default withMyTranslation(News);
