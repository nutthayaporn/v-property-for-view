import React from "react";
import _get from "lodash/get";
import _filter from "lodash/filter";
import _find from "lodash/find";
import _map from "lodash/map";
import _orderBy from "lodash/orderBy";

import {
  Layout,
  BackgroundImage,
  SwiperPromotion,
  CardArticle,
  SwiperNews,
  Image
} from "../../src/components";
import { fetchNewsList } from "../../src/services/news";
import { fetchPromotionList } from "../../src/services/promotion";
import { fetchProjects } from "../../src/services/projects";
import { withMyTranslation } from "../../src/hoc";

const bg = "/static/images/bg-white.jpg";
const bgHeader = "/static/images/bg-section2.png";

const News = props => {
  const { news, promotions, projects, t } = props;
  const highlightNews = _find(news, { isHighlight: true });
  const highlightImage = _get(highlightNews, "thumbnail");
  const highlightDate = _get(highlightNews, "publishedDate");
  const highlightTitle = t(_get(highlightNews, "title"));
  const highlightNewsId = _get(highlightNews, "id");

  const otherNews = _filter(news, { isHighlight: false });
  return (
    <Layout projects={projects}>
      <div className="news-page">
        <div className="container-news-header">
          <Image className="bg-image" src={bgHeader} />
          <div className="container">
            <SwiperPromotion advertises={promotions} />
          </div>
        </div>
        <div id="all-news" className="container-all-news">
          <div className="background-image">
            <BackgroundImage image={bg} />
          </div>
          <div className="container">
            {highlightNews && (
              <div className="container-news-highlight">
                <CardArticle
                  image={highlightImage}
                  number={0}
                  date={highlightDate}
                  title={highlightTitle}
                  id={highlightNewsId}
                />
              </div>
            )}

            <div className="container-news-slider">
              <div className="all-news-desktop">
                <SwiperNews items={otherNews} />
              </div>
              <div className="all-news-mobile">
                <SwiperNews items={news} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

News.getInitialProps = async () => {
  const news = await fetchNewsList();
  const promotions = await fetchPromotionList();
  const projects = await fetchProjects();

  return { news, promotions, projects };
};

export default withMyTranslation(News);
