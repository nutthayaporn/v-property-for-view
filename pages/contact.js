import React, { useState, useEffect, useRef } from "react";
import { Form, Input, Button, Select, message } from "antd";
import ReCAPTCHA from "react-google-recaptcha";
import _get from "lodash/get";
import _map from "lodash/map";
import _isEmpty from "lodash/isEmpty";
import _filter from "lodash/filter";
import _find from "lodash/find";
import _toNumber from "lodash/toNumber";

import {
  Banner,
  Layout,
  Image,
  Icon,
  GoogleMaps,
} from "../src/components";
import { withMyTranslation } from "../src/hoc";
import { fetchContact } from "../src/services/contact";
import { fetchFormSubmit } from "../src/services/formSubmit";
import { fetchProjects } from "../src/services/projects";
import { fetchEmailReceiver } from "../src/services/emailReceiver";

const { Option } = Select;

const imageHeaderBackground = "/static/images/bg-section2.png";

const contact = (props) => {
  const recaptchaRef = useRef(null);
  const { contactData, projects, t } = props;
  const { getFieldDecorator, validateFields } = props.form;
  const [activeButton, setActiveButton] = useState("Google Map");
  const [directions, setDirections] = useState({});
  const [keyboardValue, setKeyboardValue] = useState("");
  const [placePredictions, setPlacePredictions] = useState([]);
  const [selectedPlace, setSelectedPlace] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    validateFields(async (err, values) => {
      if (!err) {
        let emailReceiver = await fetchEmailReceiver();
        emailReceiver = _find(emailReceiver, ["page", "contact-us"]);

        const data = {
          page: "contact-us",
          firstname: _get(values, "firstname"),
          lastname: _get(values, "lastname"),
          telephone: _get(values, "telephone"),
          email: _get(values, "email"),
          about: _get(values, "about"),
          subject: _get(values, "subject"),
          detail: _get(values, "detail"),
          to: _get(emailReceiver, "to"),
          cc: _get(emailReceiver, "cc"),
          bcc: _get(emailReceiver, "bcc"),
        };
        const recaptchaValue = recaptchaRef.current.getValue();
        if (!recaptchaValue) {
          return;
        }
        const result = await fetchFormSubmit(data);
        if (_get(result, "status") === "success") {
          message.success("Success");
          props.form.resetFields();
          recaptchaRef.current.reset();
        }
      }
    });
  };

  useEffect(() => {
    async function searchPlace() {
      if (!keyboardValue) return;
      if (selectedPlace) return;

      const autocompleteService = new google.maps.places.AutocompleteService();
      autocompleteService.getPlacePredictions(
        {
          input: keyboardValue,
          radius: 500,
          location: new google.maps.LatLng(
            _get(directions, "lat"),
            _get(directions, "lng")
          ),
        },
        (results, status) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            setPlacePredictions(results);
          } else {
            console.error(`error fetching AutocompleteService ${results}`);
          }
        }
      );
    }

    searchPlace();
  }, [keyboardValue, selectedPlace]);

  const mapTypes = [
    { name: "Graphic Map" },
    { name: "Google Map" },
    { name: "Get Direction" },
  ];

  const imageDesktopBackground = _get(contactData, "banner.backgroundImage");
  const titleBanner = t(_get(contactData, "banner.title"));

  const welcome = t(_get(contactData, "contact.welcome"));
  const companyName = t(_get(contactData, "contact.companyName"));
  const location = t(_get(contactData, "contact.location"));
  const contactInfos = _get(contactData, "contact.contactInfos");
  let subjects = _get(contactData, "contact.subjects");
  subjects = _filter(subjects, ["status", "Show"]);

  const lat = _get(contactData, "map.lat");
  const lng = _get(contactData, "map.lng");
  const graphicMap = _get(contactData, "map.graphicMap");

  return (
    <Layout projects={projects}>
      <div className="contact-page">
        <Banner
          titlePosition="left"
          imageDesktopBackground={imageDesktopBackground}
          imageMobileBackground={imageDesktopBackground}
          title={titleBanner}
        />
        <div className="content">
          <Image className="bg-image" src={imageHeaderBackground} />
          <div className="register-form">
            <div className="container">
              <div className="row">
                <div className="col-md-6 order-2 order-md-1">
                  <div className="left-block">
                    <div className="topic">{welcome}</div>
                    <div className="form-block">
                      <Form onSubmit={handleSubmit}>
                        <Form.Item>
                          <div className="title">Contact about *</div>
                          {getFieldDecorator("about", {
                            rules: [
                              {
                                required: true,
                                message: "This field is required.",
                              },
                            ],
                          })(<Input />)}
                        </Form.Item>
                        <Form.Item>
                          <div className="title">Subject *</div>
                          {getFieldDecorator("subject", {
                            rules: [
                              {
                                required: true,
                                message: "This field is required.",
                              },
                            ],
                          })(
                            <Select
                              suffixIcon={
                                <Icon
                                  type="caret-down"
                                  style={{ color: "#ffffff" }}
                                />
                              }
                            >
                              {_map(subjects, (subject, index) => {
                                const title = t(_get(subject, "title"));
                                const value = _get(subject, "_id");
                                return (
                                  <Option value={value} key={index}>
                                    {title}
                                  </Option>
                                );
                              })}
                            </Select>
                          )}
                        </Form.Item>
                        <Form.Item>
                          <div className="title">Detail *</div>
                          {getFieldDecorator("detail", {
                            rules: [
                              {
                                required: true,
                                message: "This field is required.",
                              },
                            ],
                          })(<Input />)}
                        </Form.Item>
                        <div className="row">
                          <div className="col col-12 col-md-6">
                            <Form.Item>
                              <div className="title">First name *</div>
                              {getFieldDecorator("firstname", {
                                rules: [
                                  {
                                    required: true,
                                    message: "This field is required.",
                                  },
                                ],
                              })(<Input />)}
                            </Form.Item>
                          </div>
                          <div className="col col-12 col-md-6">
                            <Form.Item>
                              <div className="title">Last name *</div>
                              {getFieldDecorator("lastname", {
                                rules: [
                                  {
                                    required: true,
                                    message: "This field is required.",
                                  },
                                ],
                              })(<Input />)}
                            </Form.Item>
                          </div>
                        </div>
                        <Form.Item>
                          <div className="title">Email * </div>
                          {getFieldDecorator("email", {
                            rules: [
                              {
                                required: true,
                                message: "This field is required.",
                              },
                            ],
                          })(<Input />)}
                        </Form.Item>
                        <Form.Item>
                          <div className="title">Contact Number *</div>
                          {getFieldDecorator("telephone", {
                            rules: [
                              {
                                required: true,
                                message: "This field is required.",
                              },
                            ],
                          })(<Input />)}
                        </Form.Item>
                        <Form.Item>
                          <div className="recapta-block">
                            <ReCAPTCHA
                              ref={recaptchaRef}
                              sitekey={process.env.googleRecaptaKey}
                            />
                          </div>
                        </Form.Item>
                        <Form.Item>
                          <Button
                            type="primary"
                            htmlType="submit"
                            className="register-form-button"
                          >
                            Send
                          </Button>
                        </Form.Item>
                      </Form>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 order-1 order-md-2">
                  <div className="right-block">
                    <h2>{companyName}</h2>
                    <p className="address">{location}</p>
                    <div className="contact-detail">
                      {_map(contactInfos, (contactInfo, index) => {
                        const title = t(_get(contactInfo, "title"));
                        const image = t(_get(contactInfo, "image"));
                        return (
                          <div className="contact-item" key={index}>
                            <Image className="img-icon" src={image} alt="" />
                            <p>{title}</p>
                          </div>
                        );
                      })}
                    </div>
                    <div className="map-container">
                      <div className="group-block">
                        <div className="button-group">
                          {_map(mapTypes, (mapType, index) => {
                            const name = _get(mapType, "name");
                            return (
                              <div
                                key={index}
                                className={`button-primary ${
                                  activeButton === name ? "active" : ""
                                }`}
                                onClick={() => setActiveButton(name)}
                              >
                                {name}
                              </div>
                            );
                          })}
                        </div>
                        <div className="direction-block">
                          {activeButton === "Get Direction" && (
                            <>
                              <Input
                                value={keyboardValue}
                                placeholder="Search Location"
                                allowClear={true}
                                onChange={(e) => {
                                  const value = e.target.value;
                                  setKeyboardValue(value);
                                  if (selectedPlace) {
                                    setPlacePredictions([]);
                                    setSelectedPlace("");
                                    setDirections({});
                                  }
                                  if (!value) {
                                    setPlacePredictions([]);
                                    setDirections({});
                                  }
                                }}
                              />
                              <Button>Go</Button>
                              {!selectedPlace && (
                                <div className="result-block">
                                  <ul>
                                    {_map(
                                      placePredictions,
                                      (placePrediction) => {
                                        const id = _get(placePrediction, "id");
                                        const description = _get(
                                          placePrediction,
                                          "description"
                                        );
                                        return (
                                          <li
                                            key={id}
                                            onClick={() => {
                                              const geocoder = new google.maps.Geocoder();
                                              geocoder.geocode(
                                                { address: description },

                                                function (results, status) {
                                                  if (status === "OK") {
                                                    const {
                                                      location,
                                                    } = results[0].geometry;
                                                    setDirections({
                                                      lat: location.lat(),
                                                      lng: location.lng(),
                                                    });
                                                    setKeyboardValue(
                                                      description
                                                    );
                                                    setSelectedPlace(
                                                      description
                                                    );
                                                  } else {
                                                    alert(
                                                      "Geocode was not successful for the following reason: " +
                                                        status
                                                    );
                                                  }
                                                }
                                              );
                                            }}
                                          >
                                            <p>{description}</p>
                                          </li>
                                        );
                                      }
                                    )}
                                  </ul>
                                </div>
                              )}
                            </>
                          )}
                        </div>
                      </div>
                      <div className="map-content">
                        {(activeButton === "Google Map" ||
                          activeButton === "Get Direction") && (
                          <div style={{ width: "100%", height: "100%" }}>
                            <GoogleMaps
                              defaultZoom={14}
                              lat={_toNumber(lat)}
                              lng={_toNumber(lng)}
                              directions={directions}
                            />
                          </div>
                        )}
                        {activeButton === "Graphic Map" && (
                          <Image className="bg-image-map" src={graphicMap} />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

const WrappedContact = Form.create({ name: "contact" })(contact);

WrappedContact.getInitialProps = async () => {
  const contactData = await fetchContact();
  const projects = await fetchProjects();
  return { contactData, projects };
};

export default withMyTranslation(WrappedContact);
