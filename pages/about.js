import React, { useState } from "react";
import _get from "lodash/get";
import _filter from "lodash/filter";
import _find from "lodash/find";
import _map from "lodash/map";
import { Tabs } from "antd";
import { useRouter } from "next/router";

import { Layout, Banner, Image } from "../src/components";
import VpropertyBrand from "../src/components/About/VpropertyBrand";
import AboutVproperty from "../src/components/About/AboutVproperty";
import VFamily from "../src/components/About/VFamily";

const { TabPane } = Tabs;

const imageDesktopBackground = "/static/images/pages/about/banner.jpg";
const imageMobileBackground = "/static/images/pages/about/banner.jpg";
const imageContentBackground1 = "/static/images/bg-section2.png";
const backgroundVpropertyBrand = "/static/images/pages/about/bg-about-1.png";

const items = [
  {
    id: "VPropertyBrand",
    name: "V Property Brand",
    content: <VpropertyBrand />
  },
  {
    id: "AboutVProperty",
    name: "About V Property",
    content: <AboutVproperty />
  },
  { id: "VFamily", name: "V Family", content: <VFamily /> }
];

const About = () => {
  const router = useRouter();
  const name = router.query.name || 'VPropertyBrand';
  const [activeKey, setActiveKey] = useState(name);
  const selectedTab = _find(items, ["id", name]);
  const selectedTabId = _get(selectedTab, "id");

  let background = imageContentBackground1;
  if (activeKey === "VPropertyBrand") {
    background = backgroundVpropertyBrand;
  }

  return (
    <Layout>
      <div className="about-page">
        <Banner
          titlePosition="right"
          imageDesktopBackground={imageDesktopBackground}
          imageMobileBackground={imageMobileBackground}
          title={"V Property"}
          subTitle={"About"}
        />
        <div className="about-page-container">
          <Image
            key={activeKey}
            className="bg-image-background-tab"
            src={background}
            lazyload="false"
          />
          <Tabs
            animated={false}
            defaultActiveKey={selectedTabId}
            onChange={key => setActiveKey(key)}
          >
            {_map(items, item => {
              const name = _get(item, "name");
              const id = _get(item, "id");
              const content = _get(item, "content");
              return (
                <TabPane tab={name} key={id} forceRender={false}>
                  {content}
                </TabPane>
              );
            })}
          </Tabs>
        </div>
      </div>
    </Layout>
  );
};
export default About;
