import React from "react";
import _get from "lodash/get";

import { Layout, Image } from "../src/components";
import { fetchProjects } from "../src/services/projects";
import { fetchDisclamer } from "../src/services/disclamer";
import { withMyTranslation } from "../src/hoc";

const imageBackground = "/static/images/bg-section2.png";

const Disclamer = props => {
  const { projects, data, t } = props;
  const title = t(_get(data, "title"));
  const description = t(_get(data, "fullDescription"));
  return (
    <Layout projects={projects}>
      <div className="disclamer-policy-page">
        <Image className="bg-image" src={imageBackground} />
        <div className="container">
          <div className="content">
            <h1>{title}</h1>
            <div
              dangerouslySetInnerHTML={{
                __html: description
              }}
            />
          </div>
        </div>
      </div>
    </Layout>
  );
};

Disclamer.getInitialProps = async () => {
  const projects = await fetchProjects();
  const data = await fetchDisclamer();
  return { projects, data };
};

export default withMyTranslation(Disclamer);
