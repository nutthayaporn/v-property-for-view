import React from "react";
import _get from "lodash/get";

import { Layout, Image } from "../src/components";
import { fetchProjects } from "../src/services/projects";
import { fetchDisclamer } from "../src/services/disclamer";
import { withMyTranslation } from "../src/hoc";

const imageBackground = "/static/images/bg-section2.png";

const ForeignBuyerGuide = props => {
  const { projects, data, t } = props;
  return (
    <Layout projects={projects}>
      <div className="disclamer-policy-page">
        <Image className="bg-image" src={imageBackground} />
        <div className="container">
          <div className="content">
            <div className="block">
              <h1>Foreign buyer guide</h1>
              <h2>Information for foreign buyers of condominiums</h2>
              <p>
                Under the 1991 condominium act, non-residents who purchase
                condominium units must transfer the funds to pay for the unit
                from overseas, coming into Thailand as foreign currency.
                Purchasers need to obtain a “Foreign Exchange Transaction Form”
                certificate for each payment from the beneficiary bank, and all
                these certificates must be shown to the Land Department in order
                to register the condominium. When the money is sent to Thailand,
                the purchaser needs to include in the transfer instruction that
                the purpose is to buy a condominium unit. Banks will only give
                “Foreign Exchange Transaction Form” certificates for amounts
                over US$ 20,001. For amounts below this, purchasers must obtain
                a credit note from the bank. If the foreign purchaser has
                Permanent Residence, they can purchase the condominium in Thai
                Baht (no need to use “Foreign Exchange Transaction Form”).
              </p>
            </div>

            <div className="block">
              <h2>Transferring money</h2>
              <ul className="ul-num">
                <li>
                  The money must be T/T (Telex-Transferred), with a document
                  identifying the name of the purchaser in the form of either
                  the sender’s or receiver’s name. For example: Mr. Smith wants
                  to buy a condominium in Thailand. He has his own account at an
                  overseas bank to buy the condominium, he has to instruct the
                  overseas bank to issue the document in his name as the sender
                  of that amount to his (savings) account in Thailand, or any
                  law firm (with the permission from the particular firm) after
                  which the appointed company will buy that condominium for him,
                  <br />
                  ( or )<br />
                  It is possible for the buyer to ask a third party to transfer
                  the money for him to buy the property by identifying himself
                  as the receiver.
                </li>
                <li>
                  The purchaser has to include the transfer instruction
                  indicating that the purpose of this money is to buy a
                  condominium unit.The bank that receives money in foreign
                  currency will issue the document (Foreign Exchange Transaction
                  Form) which contains the following information:
                  <ul className="ul-2">
                    <li>The transferred amount in foreign currency</li>
                    <li>The transferred amount in Thai Baht</li>
                    <li>The name of money sender</li>
                    <li>The name of money receiver</li>
                    <li>The purpose of transferring</li>
                  </ul>
                  Normally in the transfer process, the purchaser has to
                  instruct the gateway bank to identify the name of the sender
                  to be the same as the receiver’s. The Land Department will
                  accept the name of purchaser to be either sender or receiver
                  of the transferred money.
                </li>
              </ul>
              <p>**Remark**</p>
              <p>
                If the money receiver is an individual but wants to put the unit
                in the name of two people, the land department will allow them
                to do so. The amount of money must be equal to or higher than
                the selling price declared to the Land Department. In order to
                obtain a Foreign Exchange Transaction Form, the amount of money
                must be above US$20,100; lesser amounts require the purchaser to
                obtain a credit note from the bank.
              </p>
            </div>
            <div className="block">
              <h2>Why do we need to use Foreign Exchange Transaction Form?</h2>
              <ul className="ul-num">
                <li>
                  Because the Bank of Thailand controls the flow of foreign
                  currency, every single baht must have a source and a reason,
                  and the Land Department must perform in accordance with this
                  regulation. By this restriction, every foreign buyer must
                  transfer money from abroad.
                </li>
                <li>
                  Non-residents who sell the condominium and transfer the money
                  out of country do not have to pay the remittance tax (normally
                  the tax is around 30)
                </li>
                <li>
                  In case of transferring money out of the country, the tax-free
                  amount is determined by the initial amount transferred.
                </li>
              </ul>
            </div>
            <div className="block">
              <h2>Transfer of title deed</h2>
              <p>Documents needed : For Individual Purchaser</p>
              <ul className="ul-disc">
                <li>
                  Passport (copy if applicable, marriage/divorce certificate
                  including a letter of consent from your spouse if married
                  [Thai Language]).
                </li>
                <li>Foreign Exchange Transaction Form or credit note.</li>
                <li>
                  Power of attorney, if anyone but yourself is representing you,
                  in Thai script with a notarization by Notary Public or if
                  overseas by the Thai Consul.
                </li>
              </ul>
              <p>Documents needed: For Company Purchaser</p>
              <p>Corporate documents:</p>
              <ul className="ul-disc">
                <li>
                  <p>Certified copy of the certificate of incorporation</p>
                  <p>
                    (หนังสือรับรองการจดทะเบียนบริษัท มีอายุไม่เกิน 30 วัน
                    และรับรองสำเนาถูกต้อง)
                  </p>
                </li>
                <li>
                  <p>Certified copy of the memorandum of association</p>
                  <p>
                    (หนังสือบริคณห์สนธิ มีอายุไม่เกิน 30 วัน
                    และรับรองสำเนาถูกต้อง)
                  </p>
                </li>
                <li>
                  <p>Notaries certificate</p>
                  <p>(ถ้าเป็นบริษัทต่างชาติ เอกสารออกโดยสถานทูตของชาตินั้นๆ)</p>
                </li>
                <li>
                  <p>
                    List of shareholders (in English, but must be accompanied by
                    a Thai translation)
                  </p>
                  <p>(รายชื่อผู้ถือหุ้น)</p>
                </li>
                <li>
                  <p>
                    Minutes of directors’ meetings which have a resolution to
                    sell/purchase the property (specifying name and number and
                    agenda dealing with the finance of the purchase if the
                    company’s registered capital is less than the purchase
                    price)
                  </p>
                  <p>(รายงานการประชุม (อนุมัติเรื่องการซื้อขายครั้งนี้)</p>
                </li>
                <li>
                  <p>Letter of advice regarding specific signature</p>
                  <p>
                    (หนังสือตัวอย่างลายเซ็นกรรมการผู้มีอำนาจ มีอายุไม่เกิน 30
                    วัน และรับรองสำเนาถูกต้อง)
                  </p>
                </li>
                <li>
                  <p>
                    Power of attorney, if the authorized directors do not make
                    the transaction by themselves (Land Department form)
                  </p>
                  <p>
                    (หนังสือมอบอำนาจ แบบฟอร์มของกรมที่ดิน
                    ในกรณีกรรมการผู้มีอำนาจไม่ได้ไปทำนิติกรรมที่สำนักงานที่ดินด้วยตนเอง)
                  </p>
                </li>
                <li>
                  <p>Free debt letter</p>
                  <p>
                    (จดหมายปลอดหนี้
                    จากนิติบุคคลอาคารชุดซึ่งผู้ขายเป็นผู้ติดต่อทางนิติบุคคลอาคารชุด)
                  </p>
                </li>
                <li>
                  <p>Foreign co-owner letter</p>
                  <p>
                    (จดหมายสัดส่วนต่างชาติ จากนิติบุคคลอาคารชุด
                    ในกรณีที่ผู้ซื้อเป็นชาวต่างชาติเท่านั้น
                    ผู้ขายเป็นผู้ติดต่อทางนิติบุคคลอาคารชุด)
                  </p>
                </li>
                <li>
                  <p>
                    The certified copy of the I.D. Card of the authorized
                    directors
                  </p>
                  <p>(บัตรประชาชน พร้อมลายเซ็นรับรองสำเนาถูกต้องทุกแผ่น)</p>
                </li>
                <li>
                  <p>
                    The certified copy of the house registration of the
                    authorized directors
                  </p>
                  <p>(ทะเบียนบ้าน พร้อมลายเซ็นรับรองสำเนาถูกต้องทุกแผ่น)</p>
                </li>
                <li>
                  <p>Title deed of the condominium unit</p>
                  <p>
                    (หนังสือกรรมสิทธิ์ห้องชุดฉบับจริงของห้องชุดที่จะซื้อขายครั้งนี้
                    ซึ่งผู้ขายเป็นผู้เตรียม)
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

ForeignBuyerGuide.getInitialProps = async () => {
  const projects = await fetchProjects();
  const data = await fetchDisclamer();
  return { projects, data };
};

export default withMyTranslation(ForeignBuyerGuide);
