import React from "react";
import AspectRatio from "react-ratio";
import { Progress } from "antd";
import _map from "lodash/map";
import _get from "lodash/get";
import _toNumber from "lodash/toNumber";
import _kebabCase from "lodash/kebabCase";
import dayjs from "dayjs";

import { Link, Router } from "../src/i18n";
import { Layout, Banner, Image, BackgroundImage } from "../src/components";
import { withMyTranslation } from "../src/hoc";
import { fetchProjects } from "../src/services/projects";
import { fetchProjectProgress } from "../src/services/projectProgress";

const imageMobileBackground =
  "/static/images/pages/project-progress/banner-progress-mobile.jpg";
const imageContentBackground = "/static/images/bg-section2.png";

const ProjectProgress = (props) => {
  const { projects, progressData, t } = props;
  const imageDesktopBackground = _get(progressData, "banner.backgroundImage");
  const titleBanner = t(_get(progressData, "banner.title"));
  const subTitleBanner = t(_get(progressData, "banner.subTitle"));
  return (
    <Layout projects={projects}>
      <div className="project-progress-page">
        <Banner
          titlePosition="right"
          imageDesktopBackground={imageDesktopBackground}
          imageMobileBackground={imageDesktopBackground}
          title={titleBanner}
          subTitle={subTitleBanner}
        />
        <div className="project-progress-container-desktop">
          <Image
            className="bg-image"
            src={imageContentBackground}
            lazyload="false"
          />
          <div className="topic-container">
            <div className="container">
              <div className="row no-gutters justify-content-center">
                <div className="col-auto">
                  <h1 className="topic-label">{t("ความก้าวหน้าของโครงการ")}</h1>
                </div>
              </div>
            </div>
          </div>
          <div className="project-container">
            <div className="container">
              <div className="items">
                {_map(projects, (project, index) => {
                  const slug = _get(project, "slug");
                  const logo = _get(project, "logo");

                  const progress = _get(project, "construction");
                  const slogan = t(_get(progress, "tagLine"));
                  const overall = _get(progress, "overallPercent");
                  const image = _get(progress, "image");
                  let lastUpdate = _get(progress, "updated");
                  lastUpdate = dayjs(lastUpdate).format("D MMMM YYYY");
                  return (
                    // <Link
                    //   href={"/projects/[id]#section-progress"}
                    //   as={`/projects/${slug}#section-progress`}
                    //   key={index}
                    // >
                    <a
                      key={index}
                      onClick={() =>
                        Router.push(`/projects/${slug}#section-progress`)
                      }
                    >
                      <div className="item-block">
                        <div className="item">
                          <div className="bg-block">
                            <BackgroundImage image={image} />
                          </div>
                          <div className="item-content-block">
                            <div className="item-content">
                              <div className="logo-block">
                                <Image src={logo} />
                              </div>
                              <div className="slogan-block">
                                <p>{slogan}</p>
                              </div>
                              <div className="overall-block">
                                <div className="overall-bg"></div>
                                <div className="overall-content">
                                  <div className="overall-label">
                                    {t("Overall")}
                                  </div>
                                  <div className="progress-block">
                                    <Progress
                                      type="circle"
                                      percent={_toNumber(overall)}
                                      format={(percent) => `${percent}%`}
                                      strokeColor="#d4bb9d"
                                      strokeWidth={7}
                                      strokeLinecap="square"
                                    />
                                  </div>
                                  <div className="lastupdate-label">
                                    {t("Updated")}: {lastUpdate}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </a>
                    // </Link>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
        <div className="project-progress-container-mobile">
          {_map(projects, (project, index) => {
            const slug = _get(project, "slug");
            const logo = _get(project, "logo");

            const progress = _get(project, "construction");
            const slogan = t(_get(progress, "tagLine"));
            const overall = _get(progress, "overallPercent");
            const image = _get(progress, "image");
            let lastUpdate = _get(progress, "updated");
            lastUpdate = dayjs(lastUpdate).format("D MMMM YYYY");
            return (
              // <Link
              //   href={"/projects/[id]#section-progress"}
              //   as={`/projects/${slug}#section-progress`}
              //   key={index}
              // >
              <div
                key={index}
                className="item-block-mobile"
                onClick={() =>
                  Router.push(`/projects/${slug}#section-progress`)
                }
              >
                <div className="image-block">
                  <AspectRatio
                    ratio={16 / 9}
                    style={{
                      backgroundImage: `url('${process.env.serviceApiUrl}${image}')`,
                      backgroundSize: "cover",
                      backgroundPosition: "center",
                    }}
                  />
                </div>
                <div className="detail-block">
                  <div className="left-block">
                    <div className="logo-block">
                      <Image src={logo} />
                    </div>
                    <div className="slogan-block">
                      <p>{slogan}</p>
                    </div>
                  </div>
                  <div className="right-block">
                    <div className="overall-content">
                      <div className="overall-label">{t("Overall")}</div>
                      <div className="progress-block">
                        <Progress
                          type="circle"
                          percent={_toNumber(overall)}
                          format={(percent) => `${percent}%`}
                          strokeColor="#d4bb9d"
                          strokeWidth={7}
                          strokeLinecap="square"
                        />
                      </div>
                      <div className="lastupdate-label">
                        {t("Updated")}: {lastUpdate}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              // </Link>
            );
          })}
        </div>
      </div>
    </Layout>
  );
};

ProjectProgress.getInitialProps = async () => {
  const projects = await fetchProjects();
  const progressData = await fetchProjectProgress();

  return { projects, progressData };
};

export default withMyTranslation(ProjectProgress);
