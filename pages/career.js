import React, { useRef } from "react";
import _get from "lodash/get";
import _map from "lodash/map";
import _find from "lodash/find";
import { Collapse, Form, Input, Button, Icon, message, Upload } from "antd";
import ReCAPTCHA from "react-google-recaptcha";

import { Layout, Banner, Image, Swiper } from "../src/components";
import { fetchCareer } from "../src/services/career";
import { withMyTranslation } from "../src/hoc";
import { fetchFormSubmit } from "../src/services/formSubmit";
import { fetchProjects } from "../src/services/projects";
import { fetchUpload } from "../src/services/upload";
import { fetchEmailReceiver } from "../src/services/emailReceiver";

const { Panel } = Collapse;
const { Dragger } = Upload;

const imageContentBackground1 = "/static/images/bg-section2.png";

const career = props => {
  const recaptchaRef = useRef(null);
  const { careerData, projects, t } = props;
  const { getFieldDecorator, validateFields } = props.form;

  const handleSubmit = async e => {
    e.preventDefault();
    validateFields(async (err, values) => {
      if (!err) {
        let emailReceiver = await fetchEmailReceiver();
        emailReceiver = _find(emailReceiver, ["page", "career"]);

        let attachFiles = _get(values, "attachFiles");
        attachFiles = await Promise.all(
          _map(attachFiles, async attachFile => {
            const file = _get(attachFile, "originFileObj");
            let assetUrl = await fetchUpload(file);
            assetUrl = _get(assetUrl, "uploadedUrl");
            return assetUrl;
          })
        );

        const postData = {
          page: "career",
          firstname: _get(values, "firstname"),
          lastname: _get(values, "lastname"),
          telephone: _get(values, "telephone"),
          email: _get(values, "email"),
          careerid: _get(values, "careerid"),
          careertitle: _get(values, "careertitle.en"),
          attachFiles,
          to: _get(emailReceiver, "to"),
          cc: _get(emailReceiver, "cc"),
          bcc: _get(emailReceiver, "bcc")
        };
        const recaptchaValue = recaptchaRef.current.getValue();
        if (!recaptchaValue) {
          return;
        }
        const result = await fetchFormSubmit(postData);
        if (_get(result, "status") === "success") {
          message.success("Success");
          props.form.resetFields();
          recaptchaRef.current.reset();
        }
      }
    });
  };

  const normFile = e => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const beforeUploadFile = file => {
    const isPdf = file.type === "application/pdf";
    if (!isPdf) {
      message.error("You can only upload PDF file.");
    }
    const isOverSize = file.size / 1024 / 1024 < 5;
    if (!isOverSize) {
      message.error("Image must smaller than 5MB.");
    }
    return false;
  };

  const imageDesktopBackground = _get(careerData, "banner.backgroundImage");
  const titleBanner = t(_get(careerData, "banner.title"));
  const subTitleBanner = t(_get(careerData, "banner.subTitle"));

  const privilegeDescription = t(_get(careerData, "privilege.description"));
  const privileges = _get(careerData, "privilege.privileges");

  const positions = _get(careerData, "position.positions");
  const positionTitle = t(_get(careerData, "position.title"));

  const imageBlock1 = _get(careerData, "privilege.backgroundImage");

  return (
    <Layout projects={projects}>
      <div className="career-page">
        <Banner
          titlePosition="left"
          imageDesktopBackground={imageDesktopBackground}
          imageMobileBackground={imageDesktopBackground}
          title={titleBanner}
          subTitle={subTitleBanner}
        />
        <div className="block1">
          {imageBlock1 && (
            <Image className="block1-bg-image" src={imageBlock1} />
          )}

          <div className="container">
            <div className="title-carreer">{privilegeDescription}</div>
            <div className="container-points">
              <div className="desktop">
                {_map(privileges, (item, index) => {
                  const title = t(_get(item, "title"));
                  const description = t(_get(item, "description"));
                  return (
                    <div className="point-block" key={index}>
                      <p className="title">{title}</p>
                      <p className="description">{description}</p>
                    </div>
                  );
                })}
              </div>
              <div className="mobile">
                <Swiper
                  items={privileges}
                  uiRenderer={(item, index) => {
                    const title = t(_get(item, "title"));
                    const description = t(_get(item, "description"));
                    return (
                      <div className="point-block" key={index}>
                        <p className="title">{title}</p>
                        <p className="description">{description}</p>
                      </div>
                    );
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="block2">
          <Image className="bg-image" src={imageContentBackground1} />
          <div className="container">
            <h2>{positionTitle}</h2>
            <Collapse
              accordion
              bordered={false}
              expandIconPosition="right"
              expandIcon={({ isActive }) => (
                <Icon type="caret-down" rotate={isActive ? 180 : 0} />
              )}
            >
              {_map(positions, (position, index) => {
                const careerId = _get(position, "_id");
                const careerTitle = _get(position, "title");
                const name = t(_get(position, "title"));
                const description = t(_get(position, "description"));
                return (
                  <Panel header={name} key={index}>
                    <div className="row">
                      <div className="col-12 col-md-6">
                        <div className="left-block">
                          <div
                            dangerouslySetInnerHTML={{ __html: description }}
                          />
                        </div>
                      </div>
                      <div className="col-12 col-md-6">
                        <div className="form-block">
                          <Form onSubmit={handleSubmit}>
                            {getFieldDecorator("careerid", {
                              initialValue: careerId
                            })(<Input type="hidden" />)}
                            {getFieldDecorator("careertitle", {
                              initialValue: careerTitle
                            })(<Input type="hidden" />)}
                            <div className="row">
                              <div className="col col-12 col-lg-6">
                                <Form.Item>
                                  <div className="title">First name *</div>
                                  {getFieldDecorator("firstname", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "This field is required."
                                      }
                                    ]
                                  })(<Input />)}
                                </Form.Item>
                              </div>
                              <div className="col col-12 col-lg-6">
                                <Form.Item>
                                  <div className="title">Last name *</div>
                                  {getFieldDecorator("lastname", {
                                    rules: [
                                      {
                                        required: true,
                                        message: "This field is required."
                                      }
                                    ]
                                  })(<Input />)}
                                </Form.Item>
                              </div>
                            </div>
                            <Form.Item>
                              <div className="title">Email *</div>
                              {getFieldDecorator("email", {
                                rules: [
                                  {
                                    required: true,
                                    message: "This field is required."
                                  }
                                ]
                              })(<Input />)}
                            </Form.Item>
                            <Form.Item>
                              <div className="title">Contact Number *</div>
                              {getFieldDecorator("telephone", {
                                rules: [
                                  {
                                    required: true,
                                    message: "This field is required."
                                  }
                                ]
                              })(<Input />)}
                            </Form.Item>
                            <Form.Item>
                              <div className="title">Attach File Here *</div>
                              {getFieldDecorator("attachFiles", {
                                valuePropName: "fileList",
                                getValueFromEvent: normFile,
                                rules: [
                                  {
                                    required: true,
                                    message: "This field is required."
                                  }
                                ]
                              })(
                                <Dragger
                                  beforeUpload={beforeUploadFile}
                                  multiple={true}
                                  accept="image/*,.pdf"
                                >
                                  <p className="ant-upload-text">
                                    File size under 5 mb. / PDF
                                  </p>
                                </Dragger>
                              )}
                            </Form.Item>
                            <Form.Item style={{ textAlign: "center" }}>
                              <div className="recapta-block">
                                <ReCAPTCHA
                                  ref={recaptchaRef}
                                  sitekey={process.env.googleRecaptaKey}
                                />
                              </div>
                            </Form.Item>
                            <Form.Item style={{ textAlign: "center" }}>
                              <Button
                                type="primary"
                                htmlType="submit"
                                className="register-form-button"
                              >
                                Apply
                              </Button>
                            </Form.Item>
                          </Form>
                        </div>
                      </div>
                    </div>
                  </Panel>
                );
              })}
            </Collapse>
          </div>
        </div>
      </div>
    </Layout>
  );
};

const WrappedCareer = Form.create({ name: "career" })(career);

WrappedCareer.getInitialProps = async () => {
  const careerData = await fetchCareer();
  const projects = await fetchProjects();
  return { careerData, projects };
};

export default withMyTranslation(WrappedCareer);
