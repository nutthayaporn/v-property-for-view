import React from "react";
import App from "next/app";
import Head from "next/head";

import { Preload } from "../src/components";
import { appWithTranslation } from "../src/i18n";
import { ThemeProvider } from "../src/hooks/useTheme";

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    const title =
      "V PROPERTY DEVELOPMENT ผู้นำด้านการพัฒนาคอนโดมิเนียมติดรถไฟฟ้า สุขุมวิท กรุงเทพมหานคร | บริษัท วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด โทร. 1298";
    const description =
      "V PROPERTY DEVELOPMENT ผู้นำด้านการพัฒนาคอนโดมิเนียมติดรถไฟฟ้า สุขุมวิท กรุงเทพมหานคร | บริษัท วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด โทร. 1298";
    return (
      <>
        <Head>
          {this.props.styleTags}
          <title>{title}</title>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <link rel="icon" type="image/x-icon" href="/static/favicon.png" />
          <meta name="description" content={description} />
          <meta
            name="keywords"
            content="V PROPERTY DEVELOPMENT, วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์, คอนโดมิเนียม, สุขุมวิท"
          />
      
        </Head>
        <Preload />
        <div id="root">
          <ThemeProvider>
            <Component {...pageProps} />
          </ThemeProvider>
        </div>
      </>
    );
  }
}

export default appWithTranslation(MyApp);
