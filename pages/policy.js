import React from "react";
import _get from "lodash/get";

import { Layout, Image } from "../src/components";
import { fetchProjects } from "../src/services/projects";
import { fetchPolicy } from "../src/services/policy";
import { withMyTranslation } from "../src/hoc";

const imageBackground = "/static/images/bg-section2.png";

const Policy = props => {
  const { data, projects, t } = props;
  const title = t(_get(data, "title"));
  const description = t(_get(data, "fullDescription"));
  return (
    <Layout projects={projects}>
      <div className="disclamer-policy-page">
        <Image className="bg-image" src={imageBackground} />
        <div className="container">
          <div className="content">
<<<<<<< HEAD
            <h1>Privacy Policy</h1>
=======
            <h1>{title}</h1>
>>>>>>> 53d8fc6c01b2ebbbc4a94d9a832499cf181731e9
            <div
              dangerouslySetInnerHTML={{
                __html: description
              }}
            />
          </div>
        </div>
      </div>
    </Layout>
  );
};

Policy.getInitialProps = async () => {
  const projects = await fetchProjects();
  const data = await fetchPolicy();
  return { projects, data };
};

export default withMyTranslation(Policy);
