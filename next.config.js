const withCss = require("@zeit/next-css");
const withLess = require("@zeit/next-less");
const withSass = require("@zeit/next-sass");
const withImages = require("next-images");

const languages = ["th", "en"];
const defaultLanguage = "en";

module.exports = withCss(
  withLess(
    withSass(
      withImages({
        env: {
          baseUrl: "https://www.vproperty.co.th",
          serviceApiUrl: "https://api.vproperty.co.th",
          googleRecaptaKey: "6Ld6VcgUAAAAAIysrlhXuBcVAAayVasliG_RQb9e",
          googleMapsKey: "AIzaSyCABfOPDFtUAtc__TkbY8nhS-9rg-6bfwk",
          facebookLink: "https://www.facebook.com/pg/VPropertyDevelopmentTH",
          intragramLink: "https://www.instagram.com/vproperty_development",
          lineLink: "https://line.me/ti/p/~@vproperty",
          languages,
          defaultLanguage,
          facebookPageId: "1720237261616863",
        },
        exportTrailingSlash: false,
        lessLoaderOptions: {
          javascriptEnabled: true,
        },
      })
    )
  )
);
