const NextI18Next = require("next-i18next").default;

const localeSubpaths = "foreign";
const localeSubpathVariations = {
  none: {},
  foreign: {
    th: "th"
  },
  all: {
    en: "en",
    th: "th"
  }
};

module.exports = new NextI18Next({
  defaultLanguage: "en",
  otherLanguages: ["th"],
  localeSubpaths: localeSubpathVariations[localeSubpaths],
  localePath: "public/static/locales"
});
