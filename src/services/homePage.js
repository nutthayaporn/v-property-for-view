import _get from "lodash/get";

import { fetchJson } from "../utils";

const fetchHomePage = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/home/first-doc`
  ).then(res => _get(res, "data", {}));
  return result;
};

export { fetchHomePage };
