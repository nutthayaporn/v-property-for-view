import _get from "lodash/get";
import _map from "lodash/map";
import _filter from "lodash/filter";

import { fetchJson } from "../utils";

const fetchProjects = async () => {
  const result = await fetchJson(`${process.env.serviceApiUrl}/project/list`)
    .then(res => _get(res, "data", []))
    .then(items =>
      _map(items, item => {
        return {
          ...item,
          image: {
            banner: {
              mobile: _get(item, "thumbnail")
            }
          },
          logo: _get(item, "logo"),
          tag: _get(item, "projectStatus.title"),
          captions: _get(item, "highlightInfos"),
          slug: _get(item, "slug"),
          projectTypeId: _get(item, "projectType._id"),
          projectLocationId: _get(item, "projectLocation._id"),
          projectStatusId: _get(item, "projectStatus._id"),
          projectPrice: _get(item, "projectPrice")
        };
      })
    );
  return result;
};

const fetchProjectById = async id => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project/view/${id}`
  ).then(res => _get(res, "data", []));
  return result;
};

const fetchProjectBySlug = async slug => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project/view-slug/${slug}`
  ).then(res => _get(res, "data", []));
  return result;
};

const fetchProjectTypeList = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project/type/list`
  )
    .then(res => _get(res, "data", []))
    .then(items =>
      _map(items, item => {
        return {
          ...item,
          id: _get(item, "_id"),
          name: _get(item, "title")
        };
      })
    )
    .then(items => _filter(items, ["status", "Show"]));
  return result;
};

const fetchProjectStatusList = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project/status/list`
  )
    .then(res => _get(res, "data", []))
    .then(items =>
      _map(items, item => {
        return {
          ...item,
          id: _get(item, "_id"),
          name: _get(item, "title")
        };
      })
    )
    .then(items => _filter(items, ["status", "Show"]));
  return result;
};

const fetchProjectRoomTypes = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project/room-type/list`
  )
    .then(res => _get(res, "data", []))
    .then(items => _filter(items, ["status", "Show"]));
  return result;
};

const fetchProjectPriceList = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project/price/list`
  )
    .then(res => _get(res, "data", []))
    .then(items =>
      _map(items, item => {
        return {
          ...item,
          id: _get(item, "_id"),
          price: `${_get(item, "minPrice")}-${_get(item, "maxPrice")} MB`
        };
      })
    )
    .then(items => _filter(items, ["status", "Show"]));
  return result;
};

const fetchProjectLocationList = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project/location/list`
  )
    .then(res => _get(res, "data", []))
    .then(items =>
      _map(items, item => {
        return {
          ...item,
          id: _get(item, "_id"),
          name: _get(item, "title")
        };
      })
    )
    .then(items => _filter(items, ["status", "Show"]));
  return result;
};

const fetchProjectBudgetList = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project/budget/list`
  ).then(res => _get(res, "data", []));
  return result;
};

export {
  fetchProjects,
  fetchProjectById,
  fetchProjectBySlug,
  fetchProjectTypeList,
  fetchProjectStatusList,
  fetchProjectRoomTypes,
  fetchProjectPriceList,
  fetchProjectLocationList,
  fetchProjectBudgetList
};
