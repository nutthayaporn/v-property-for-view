import _get from "lodash/get";

import { fetchJson } from "../utils";

const fetchEmailReceiver = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/email-receiver/list`
  ).then(res => _get(res, "data", []));
  return result;
};

export { fetchEmailReceiver };
