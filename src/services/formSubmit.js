import _get from "lodash/get";

import { fetchPostJson } from "../utils";

const fetchFormSubmit = async data => {
  const url = `${process.env.serviceApiUrl}/form-submit/create`;
  const postData = JSON.stringify(data);
  const result = await fetchPostJson(url, postData);
  return result;
};

export { fetchFormSubmit };
