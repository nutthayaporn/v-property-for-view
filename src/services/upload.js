import _get from "lodash/get";
import axios from "axios";

const fetchUpload = async file => {
  const url = `${process.env.serviceApiUrl}/upload`;
  let data = new FormData();
  const name = _get(file, "name");
  const fileName = `form_submit/career/${name}_${new Date().getTime()}`;
  data.append("file", file);
  data.append("fileName", fileName);
  const result = await axios({
    method: "POST",
    url,
    data,
    config: { headers: { "Content-Type": "multipart/form-data" } }
  }).then(res => {
    if (_get(res, "data.status") === "success")
      return _get(res, "data.data", []);
    return;
  });
  return result;
};

export { fetchUpload };
