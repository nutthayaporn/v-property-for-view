import _get from "lodash/get";
import _map from "lodash/map";
import _filter from "lodash/filter";

import { fetchJson } from "../utils";

const fetchNewsList = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/news/article/list`
  )
    .then(res => _get(res, "data", []))
    .then(items =>
      _map(items, item => {
        return {
          ...item,
          id: _get(item, "_id")
        };
      })
    )
    .then(items => _filter(items, ["status", "Show"]));
  return result;
};

const fetchNewsById = async id => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/news/article/view/${id}`
  ).then(res => _get(res, "data", []));
  return result;
};

export { fetchNewsList, fetchNewsById };
