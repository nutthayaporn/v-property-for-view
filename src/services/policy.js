import _get from "lodash/get";

import { fetchJson } from "../utils";

const fetchPolicy = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/policy/first-doc`
  ).then(res => _get(res, "data", []));
  return result;
};

export { fetchPolicy };
