import _get from "lodash/get";

import { fetchJson } from "../utils";

const fetchDisclamer = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/disclaimer/first-doc`
  ).then(res => _get(res, "data", []));
  return result;
};

export { fetchDisclamer };
