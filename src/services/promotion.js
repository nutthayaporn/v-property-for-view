import _get from "lodash/get";
import _filter from "lodash/filter";

import { fetchJson } from "../utils";

const fetchPromotionList = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/promotion/article/list`
  ).then(res => _get(res, "data", []));
  return result;
};

const fetchPromotionById = async id => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/promotion/article/view/${id}`
  )
    .then(res => _get(res, "data", []))
    .then(items => _filter(items, ["status", "Show"]));
  return result;
};

export { fetchPromotionList, fetchPromotionById };
