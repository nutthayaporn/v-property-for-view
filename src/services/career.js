import _get from "lodash/get";

import { fetchJson } from "../utils";

const fetchCareer = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/career/first-doc`
  ).then(res => _get(res, "data", []));
  return result;
};

export { fetchCareer };
