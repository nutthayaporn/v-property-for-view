import _get from "lodash/get";

import { fetchJson } from "../utils";

const fetchProjectProgress = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/project-progress/first-doc`
  ).then(res => _get(res, "data", []));
  return result;
};

export { fetchProjectProgress };
