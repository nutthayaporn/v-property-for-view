import _get from "lodash/get";

import { fetchJson } from "../utils";

const fetchContact = async () => {
  const result = await fetchJson(
    `${process.env.serviceApiUrl}/contact-us/first-doc`
  ).then(res => _get(res, "data", []));
  return result;
};

export { fetchContact };
