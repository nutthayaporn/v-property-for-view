import React, { useState, useContext, createContext } from "react";

const ThemeContext = createContext({
  mainColor: null,
  buttonTextColor: null,
  mainColorTheme: () => {},
  buttonTextColorTheme: () => {}
});

export const ThemeProvider = props => {
  const [mainColor, setMainColor] = useState();
  const [buttonTextColor, setButtonTextColor] = useState();

  const theme = {
    mainColor,
    buttonTextColor,
    mainColorTheme: value => setMainColor(value),
    buttonTextColorTheme: value => setButtonTextColor(value)
  };

  return <ThemeContext.Provider value={theme} {...props} />;
};

export const useTheme = () => useContext(ThemeContext);
