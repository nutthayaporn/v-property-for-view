import React from "react";
import _includes from "lodash/includes";

const noImageAvailable = "/static/images/No_Image_Available.jpg";

const Image = (props) => {
  const { alt, className, style, src } = props;

  let newSrc = src;
  if (_includes(newSrc, "/upload/")) {
    newSrc = `${process.env.serviceApiUrl}${src}`;
  }

  return <img className={className} src={newSrc} alt={alt} style={style} />;
};

export default Image;

Image.defaultProps = {
  src: noImageAvailable,
  alt: "V Property",
  className: null,
};
