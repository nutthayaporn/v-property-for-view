import React from "react";
import _map from "lodash/map";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";

import { withMyTranslation } from "../hoc";
import { BackgroundImage, Image, StatusBar } from "../components";

const CardProject = props => {
  const {
    image,
    logo,
    status,
    captions,
    fontColorStatus,
    backgroundColorStatus,
    t
  } = props;
  return (
    <div className="card-project-component">
      <div className="card-project-image-mobile">
        <BackgroundImage image={image} />
      </div>
      <div className="card-project-detail-mobile">
        <Image key={logo} className="image-logo" src={logo} />

        <div className="detail-block">
          <div className="detail-block-background" />
          <div className="status-block">
            <StatusBar
              status={status}
              fontColor={fontColorStatus}
              backgroundColor={backgroundColorStatus}
            />
          </div>
          {!_isEmpty(captions) &&
            _map(captions, (caption, index) => {
              const icon = t(_get(caption, "image"));
              const name = t(_get(caption, "title"));
              return (
                <div className="text-icon-block" key={index}>
                  <div className="row no-gutters align-items-center">
                    <div className="col-1">
                      <Image
                        className="icon"
                        src={icon}
                        alt=""
                        style={{ width: 20 }}
                      />
                    </div>
                    <div className="col">
                      <p>{name}</p>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default withMyTranslation(CardProject);
