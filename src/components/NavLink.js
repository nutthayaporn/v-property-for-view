import React, { Children, useEffect } from "react";
import { withRouter } from "next/router";
import _includes from "lodash/includes";

import { Link, Router, i18n } from "../i18n";

const NavLink = ({ router, children, ...props }) => {
  const { href } = props;
  const child = Children.only(children);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  }, [href]);

  let className = child.props.className || null;

  const match =
    props.exact === true
      ? router.pathname === props.href
      : _includes(router.pathname, props.href);

  if (match && props.activeClassName) {
    className = `${className !== null ? className : ""} ${
      props.activeClassName
    }`.trim();
  }

  delete props.activeClassName;
  delete props.exact;

  return (
    <div>
      {React.cloneElement(child, {
        className,
        onClick: () => {
          if (props.openNewTab) {
            window.open(props.href, "_blank");
          } else {
            props.onClick();
            Router.push(props.href);
          }
        },
      })}
    </div>
  );
};

export default withRouter(NavLink);

NavLink.defaultProps = {
  href: "/",
  exact: true,
  activeClassName: "active",
  onClick: () => null,
  openNewTab: false,
};
