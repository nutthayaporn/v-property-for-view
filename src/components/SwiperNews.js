import React from "react";
import _chunk from "lodash/chunk";
import _get from "lodash/get";
import _padStart from "lodash/padStart";
import _map from "lodash/map";
import _size from "lodash/size";
import _orderBy from "lodash/orderBy";

import { CardArticle, Swiper, Icon } from "../components";
import { withMyTranslation } from "../hoc";

const SwiperNews = (props) => {
  const { t } = props;
  let { items } = props;
  items = _orderBy(items, ["order", "publishedDate"], ["asc", "desc"]);
  const itemPerPage = 4;
  const newItems = _chunk(items, itemPerPage);
  return (
    <div className="swiper-news-component">
      <div className="d-none d-md-block desktop">
        <div className="container-swiper ">
          <Swiper
            items={newItems}
            setting={{
              loop: false,
            }}
            uiRenderer={(items, pageIndex) => {
              return (
                <div className="card-container" key={pageIndex}>
                  {_map(items, (item, itemIndex) => {
                    const id = _get(item, "_id");
                    const image = _get(item, "thumbnail");
                    const date = _get(item, "publishedDate");
                    const title = t(_get(item, "title"));
                    return (
                      <div className="card-block" key={itemIndex}>
                        <CardArticle
                          image={image}
                          date={date}
                          title={title}
                          id={id}
                        />
                      </div>
                    );
                  })}
                </div>
              );
            }}
          />
        </div>
      </div>
      <div className="d-md-none mobile">
        <div className="container-swiper ">
          <Swiper
            setting={{
              pagination: {
                el: ".swiper-pagination",
                type: "fraction",
              },
              navigation: {
                nextEl: ".swiper-news-mobile-button-next",
                prevEl: ".swiper-news-mobile-button-prev",
              },
              renderPrevButton: () => (
                <div className="swiper-news-mobile-button-prev">
                  <Icon type="left" />
                </div>
              ),
              renderNextButton: () => (
                <div className="swiper-news-mobile-button-next">
                  <Icon type="right" />
                </div>
              ),
            }}
            items={items}
            uiRenderer={(item, index) => {
              const id = _get(item, "_id");
              const image = _get(item, "thumbnail");
              const date = _get(item, "publishedDate");
              const title = t(_get(item, "title"));
              return (
                <div className="card-block" key={index}>
                  <CardArticle
                    image={image}
                    date={date}
                    title={title}
                    id={id}
                  />
                </div>
              );
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default withMyTranslation(SwiperNews);
