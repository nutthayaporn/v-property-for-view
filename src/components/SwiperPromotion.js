import React from "react";
import AspectRatio from "react-ratio";
import _get from "lodash/get";

import { Swiper, BackgroundImage, Image } from "../components";

const whiteArrowLeft = "static/images/icon/white-arrow-left.svg";
const whiteArrowRight = "static/images/icon/white-arrow-right.svg";

const SwiperPromotion = (props) => {
  const { advertises } = props;
  return (
    <div className="swiper-promotion-component">
      <div className="container-swiper-desktop">
        <Swiper
          items={advertises}
          setting={{
            navigation: {
              nextEl: ".swiper-promotion-button-next",
              prevEl: ".swiper-promotion-button-prev",
            },
            renderPrevButton: () => (
              <div className="swiper-promotion-button-prev">
                <Image src={whiteArrowLeft} alt="" />
              </div>
            ),
            renderNextButton: () => (
              <div className="swiper-promotion-button-next">
                <Image src={whiteArrowRight} alt="" />
              </div>
            ),
            speed: 500,
            autoplay: {
              delay: 7000,
            },
            slidesPerView: "auto",
            coverflowEffect: {
              rotate: 0,
              stretch: 0,
              depth: 250,
              modifier: 1,
              slideShadows: false,
            },
            effect: "coverflow",
            centeredSlides: true,
            initialSlide: 0,
          }}
          uiRenderer={(advertise, key) => {
            const image = _get(advertise, "bannerImage");
            return (
              <div className="advertise-block" key={key}>
                <AspectRatio
                  ratio={16 / 9}
                  style={{
                    backgroundImage: `url('${process.env.serviceApiUrl}${image}')`,
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                  }}
                />
              </div>
            );
          }}
        />
      </div>
      <div className="container-swiper-mobile">
        <Swiper
          items={advertises}
          uiRenderer={(advertise, key) => {
            const image = _get(advertise, "thumbnail");
            return (
              <div className="advertise-block" key={key}>
                <BackgroundImage image={image} size="contain" />
              </div>
            );
          }}
        />
      </div>
    </div>
  );
};

export default SwiperPromotion;
