import React, { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import _get from "lodash/get";
import _size from "lodash/size";

const Lightbox = dynamic(() => import("react-image-lightbox"), {
  ssr: false
});

const GalleryLightBoxZoom = props => {
  const { active, images, imageCaption } = props;
  const [ready, setReady] = useState(false);
  const [activeIndex, setActiveIndex] = useState(active);

  useEffect(() => {
    setReady(true);
  }, []);

  if (!ready) return null;

  const currentImage = _get(images, activeIndex);
  const mainSrc = _get(currentImage, "image");
  const imageCount = _size(images);

  return (
    <Lightbox
      {...props}
      clickOutsideToClose={true}
      enableZoom={true}
      mainSrc={mainSrc}
      prevSrc={images[(activeIndex + _size(images) - 1) % _size(images)]}
      nextSrc={images[(activeIndex + 1) % _size(images)]}
      onMovePrevRequest={() => {
        const nextIndex = (activeIndex + imageCount - 1) % imageCount;
        setActiveIndex(nextIndex);
      }}
      onMoveNextRequest={() => {
        const nextIndex = (activeIndex + 1) % imageCount;
        setActiveIndex(nextIndex);
      }}
      imageCaption={imageCaption(currentImage)}
    />
  );
};

export default GalleryLightBoxZoom;
