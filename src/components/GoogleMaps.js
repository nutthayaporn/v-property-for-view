import React, { useEffect, useState } from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker
} from "react-google-maps";
import _isEmpty from "lodash/isEmpty";
import _get from "lodash/get";
import { mapStyle } from "../components";

const logoVproperty = "/static/images/pages/contact/logo-maps.svg";
const pin = "/static/images/pages/contact/pin.svg";

const Maps = withScriptjs(
  withGoogleMap(props => {
    const {
      isMarkerShown,
      defaultZoom,
      lat,
      lng,
      directions,
      logo,
      pinSize
    } = props;
    const [displayDirections, setDisplayDirections] = useState(null);

    useEffect(() => {
      if (!_isEmpty(directions)) {
        const directionsService = new google.maps.DirectionsService();

        directionsService.route(
          {
            origin: { lat, lng },
            destination: directions,
            travelMode: google.maps.TravelMode.DRIVING
          },
          (result, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
              setDisplayDirections(result);
            } else {
              console.error(`error fetching DirectionsService ${result}`);
            }
          }
        );
      } else {
        setDisplayDirections(null);
      }
    }, [directions]);

    return (
      <GoogleMap
        defaultZoom={defaultZoom}
        options={{
          styles: mapStyle
        }}
        defaultCenter={new google.maps.LatLng(lat, lng)}
      >
        {isMarkerShown && (
          <>
            <Marker
              icon={{
                url: logo,
                scaledSize: new google.maps.Size(pinSize, pinSize)
              }}
              position={new google.maps.LatLng(lat, lng)}
            />
            <Marker
              icon={{ url: pin, scaledSize: new google.maps.Size(50, 50) }}
              position={
                new google.maps.LatLng(
                  _get(directions, "lat"),
                  _get(directions, "lng")
                )
              }
            />
          </>
        )}
        {displayDirections && (
          <DirectionsRenderer
            directions={displayDirections}
            options={{
              suppressMarkers: true,
              polylineOptions: {
                strokeColor: "#d4bb9d",
                strokeOpacity: 0.8,
                strokeWeight: 4
              }
            }}
          />
        )}
      </GoogleMap>
    );
  })
);

const GoogleMaps = props => {
  return (
    <Maps
      googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${process.env.googleMapsKey}&v=3.exp&libraries=geometry,drawing,places`}
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={
        <div
          style={{
            height: `100%`,
            flex: 1,
            display: "flex",
            flexDirection: "column"
          }}
        />
      }
      mapElement={<div style={{ height: `100%`, flex: 1 }} />}
      {...props}
    />
  );
};

export default GoogleMaps;

GoogleMaps.defaultProps = {
  logo: logoVproperty,
  lat: 13.6992876,
  lng: 100.5544642,
  defaultZoom: 15,
  isMarkerShown: true,
  directions: {},
  pinSize: 90
};
