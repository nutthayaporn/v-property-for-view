import React, { useState } from "react";
import Head from "next/head";
import anime from "animejs";
import _map from "lodash/map";
import _get from "lodash/get";
import _split from "lodash/split";
import _includes from "lodash/includes";
import _find from "lodash/find";
import _filter from "lodash/filter";
import { useRouter } from "next/router";
import { Form, Input, message } from "antd";

import {
  Icon,
  LanguageSwitcher,
  NavLink,
  HamburgerIcon,
  Image,
  MyButton,
  CookiePolicy
} from "../components";
import { withMyTranslation } from "../hoc";
import { fetchFormSubmit } from "../services/formSubmit";
import { fetchEmailReceiver } from "../services/emailReceiver";

import "../assets/css/vendors/antd.less";
import "../assets/css/index.scss";

const logoImage = "/static/images/logo.svg";
const calculator = "/static/images/icon/calculator.svg";
const foreign = "/static/images/icon/foreign.svg";
const payment = "/static/images/icon/payment.svg";

const Layout = (props) => {
  const { projects, logo, t } = props;
  const { getFieldDecorator, validateFieldsAndScroll } = props.form;
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const [openProjects, setOpenProjects] = useState(false);

  const filterProjects = _filter(projects, { status: "Show" });

  const router = useRouter();
  let pathName = router.pathname;
  pathName = _split(pathName, "/");

  const {
    children,
    title = "V PROPERTY DEVELOPMENT ผู้นำด้านการพัฒนาคอนโดมิเนียมติดรถไฟฟ้า สุขุมวิท กรุงเทพมหานคร | บริษัท วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด โทร. 1298",
  } = props;

  const menus = [
    { id: "home", name: t("Home"), link: "/" },
    {
      id: "projects",
      exact: false,
      name: t("Projects"),
      link: "/projects",
      projects: _map(filterProjects, (project) => {
        const name = t(_get(project, "name"));
        const slug = _get(project, "slug");
        return { name: name, link: `/projects/${slug}` };
      }),
    },
    { id: "news", exact: false, name: t("Promotion & News"), link: "/news" },
    // {
    //   id: "project-progress",
    //   name: t("Project Progress"),
    //   link: "/project-progress"
    // },
    { id: "about", name: t("About Us"), link: "/about" },
    { id: "career", name: t("Career"), link: "/career" },
    { id: "contact", name: t("Contact"), link: "/contact" },
    // { id: "investors", name: t("Investors"), link: "" }
  ];

  const socials = [
    { icon: "line", link: process.env.lineLink },
    { icon: "instagram", link: process.env.intragramLink },
    {
      icon: "facebook",
      link: process.env.facebookLink,
    },
  ];



  const renderPhone = () => {
    return (
      <a aria-label="tel:1298" href="tel:1298">
        <div className="call-block">
          <Icon type="phone" color="#d4bb9d" width={30} height={30} />
          <p>1298</p>
        </div>
      </a>
    );
  };

  const renderHeaderDesktop = (menus) => {
    return (
      <div className="container">
        <div className="header-desktop">
          <div className="left">
            <div className="branding">
              <NavLink href="/">
                <a aria-label="logo">
                  <Image
                    className="img-branding"
                    src={logo ? logo : logoImage}
                  />
                </a>
              </NavLink>
            </div>
          </div>
          <div className="center">
            <div className="menu-container">
              <ul>
                {_map(menus, (menu, index) => {
                  const link = _get(menu, "link");
                  const exact = _get(menu, "exact");
                  const name = _get(menu, "name");
                  return (
                    <li
                      key={index}
                      style={{ pointerEvents: link ? "inherit" : "none" }}
                    >
                      <NavLink href={link} exact={exact}>
                        <a aria-label={name}>{name}</a>
                      </NavLink>
                    </li>
                  );
                })}
                <li>
                  <LanguageSwitcher />
                </li>
              </ul>
            </div>
          </div>
          <div className="right">{renderPhone()}</div>
        </div>
      </div>
    );
  };

  const handleToggle = () => {
    const nextShowMobileMenu = !showMobileMenu;
    if (nextShowMobileMenu) {
      anime({
        targets: ".header-mobile .menus li",
        opacity: [0, 1],
        // translateX: [100, 0],
        // easing: 'easeInQuad',
        delay: function (el, i, l) {
          return i * 100 + 400;
        },
        endDelay: function (el, i, l) {
          return (l - i) * 100;
        },
      });
    }
    document.body.style.overflow = nextShowMobileMenu ? "hidden" : "auto";
    setShowMobileMenu(nextShowMobileMenu);
  };

  const renderHeaderMobile = (menus) => {
    return (
      <div
        className={`header-mobile ${
          showMobileMenu ? "show-menu" : "hide-menu"
        }`}
      >
        <div className="left">
          <div className="button-toggle" onClick={() => handleToggle()}>
            <HamburgerIcon
              style={{ width: 55, height: 55 }}
              open={showMobileMenu}
              color={showMobileMenu ? "#d4bb9d" : "#ffffff"}
            />
          </div>
        </div>
        <div className="center">
          <div className="branding">
            <NavLink href="/">
              <a aria-label="logo">
                <Image className="img-branding" src={logo ? logo : logoImage} />
              </a>
            </NavLink>
          </div>
        </div>
        <div className="right">
          {showMobileMenu ? <LanguageSwitcher /> : renderPhone()}
        </div>
        <div className="menu-container">
          <div className="bg"></div>
          <div className="menus">
            <ul>
              {_map(menus, (menu, index) => {
                const link = _get(menu, "link");
                const exact = _get(menu, "exact");
                const name = _get(menu, "name");
                return (
                  <li
                    key={index}
                    style={{ pointerEvents: link ? "inherit" : "none" }}
                  >
                    <NavLink
                      href={link}
                      exact={exact}
                      onClick={() => handleToggle()}
                    >
                      <a aria-label={name}>{name}</a>
                    </NavLink>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
    );
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        let emailReceiver = await fetchEmailReceiver();
        emailReceiver = _find(emailReceiver, ["page", "subscribe"]);

        const data = {
          page: "subscribe",
          email: _get(values, "email"),
          to: _get(emailReceiver, "to"),
          cc: _get(emailReceiver, "cc"),
          bcc: _get(emailReceiver, "bcc"),
        };
        const result = await fetchFormSubmit(data);
        if (_get(result, "status") === "success") {
          message.success("Success");
          props.form.resetFields();
        }
      }
    });
  };

  return (
    <div className="layout">
      <Head>
        <title>{title}</title>
      </Head>
      <header>
        {renderHeaderDesktop(menus)}
        {renderHeaderMobile(menus)}
      </header>
      <div className="main-content">{children}</div>

      <footer>
        <div className="row-1-container">
          <div className="container">
            <div className="row-1">
              <div className="left"></div>
              <div className="center">
                <div className="menu-container">
                  <ul>
                    {_map(menus, (menu, index) => {
                      const link = _get(menu, "link");
                      const exact = _get(menu, "exact");
                      const id = _get(menu, "id");
                      if (id === "projects") {
                        const projects = _get(menu, "projects");
                        return (
                          <li
                            key={index}
                            className="li-project"
                            onClick={() => setOpenProjects(!openProjects)}
                          >
                            <a aria-label={_get(menu, "name")}>
                              {_get(menu, "name")}{" "}
                              {openProjects ? (
                                <Icon type="caret-up" />
                              ) : (
                                <Icon type="caret-down" />
                              )}
                            </a>
                            <ul
                              className="ul-project"
                              style={{
                                display: openProjects ? "flex" : "none",
                              }}
                            >
                              {_map(projects, (project, index) => {
                                const name = _get(project, "name");
                                const projectLink = _get(project, "link");
                                return (
                                  <li key={index}>
                                    <NavLink href={projectLink} exact={exact}>
                                      <a aria-label={name}>{name}</a>
                                    </NavLink>
                                  </li>
                                );
                              })}
                            </ul>
                          </li>
                        );
                      }
                      return (
                        <li
                          key={index}
                          style={{ pointerEvents: link ? "inherit" : "none" }}
                        >
                          <NavLink href={link} exact={exact}>
                            <a aria-label={_get(menu, "name")}>
                              {_get(menu, "name")}
                            </a>
                          </NavLink>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
              <div className="right"></div>
            </div>
          </div>
        </div>
        <div className="row-2-container">
          <div className="container">
            <div className="row align-items-center no-gutters">
              <div className="col-12 col-lg-8 col-xl-6">
                <div className="row align-items-center no-gutters">
                  <div className="col-12 col-md-2">
                    <div className="company-logo">
                      <Image className="logo" src={logoImage} />
                    </div>
                  </div>
                  <div className="col-12 col-md-10">
                    <div className="address-block">
                      <p className="company-name">
                        บริษัท วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด
                      </p>
                      <p className="address">
                        725 อาคารเมโทรโพลิศ ชั้น 21 ห้อง 2105-06
                        แขวงคลองตันเหนือ เขตวัฒนา กทม. 10110
                      </p>
                      <p className="address">
                        Copyright © 2019. V Property Development Co.,Ltd All
                        rights reserved.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-4 col-xl-6">
                <div className="row" style={{ alignItems: "center" }}>
                  <div className="col-12 col-xl-6">
                    <div className="contact-block">
                      <div className="socials-block">
                        <div className="socials">
                          {_map(socials, (social, index) => {
                            const icon = _get(social, "icon");
                            return (
                              <a
                                aria-label={icon}
                                key={index}
                                href={_get(social, "link")}
                              >
                                <Icon type={icon} />
                              </a>
                            );
                          })}
                        </div>
                        {renderPhone()}
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-lg-1 col-xl-6 d-block d-lg-none d-xl-block">
                    <div className="newsletter-block">
                      <p className="newsletter-title">E-Newsletter Subscribe</p>
                      <div className="block2">
                        <Form layout="inline" onSubmit={handleSubmit}>
                          {getFieldDecorator("email", {
                            rules: [
                              {
                                required: true,
                                message: "Please input your email.",
                              },
                            ],
                          })(
                            <Input
                              aria-label="email"
                              className="input-email"
                              placeholder="Your email address"
                            />
                          )}
                          <div className="button-submit-block">
                            <MyButton htmlType="submit" type="white">
                              {t("Submit")}
                            </MyButton>
                          </div>
                        </Form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row-newsletter">
          <div className="newsletter-block">
            <p className="newsletter-title">E-Newsletter Subscribe</p>
            <div className="block2">
              <Form layout="inline" onSubmit={handleSubmit}>
                {getFieldDecorator("email", {
                  rules: [
                    {
                      required: true,
                      message: "Please input your email.",
                    },
                  ],
                })(
                  <Input
                    aria-label="email"
                    className="input-email"
                    placeholder="Your email address"
                  />
                )}
                <div className="button-submit-block">
                  <MyButton htmlType="submit" type="white">
                    {t("Submit")}
                  </MyButton>
                </div>
              </Form>
            </div>
          </div>
        </div>
        <div className="row-3-container">
          <div className="container">
            <div className="row-3">
              <div className="left">
                <ul>
                  <li>
                    <a
                      aria-label={t("คำนวนยอดเงินกู้")}
                      href=" https://www.1213.or.th/th/tools/programs/pages/loans.aspx"
                      target="_blank"
                    >
                      <Image className="icon" src={calculator} />
                      {t("คำนวนยอดเงินกู้")}
                    </a>
                  </li>
                  <li>
                    <a
                      aria-label={t("Online Payment")}
                      href="/"
                      // target="_blank"
                    >
                      <Image className="icon" src={payment} />
                      {t("Online Payment")}
                    </a>
                  </li>
                  <li>
                    <a
                      aria-label={t(`Foreign Buyer's Guide`)}
                      href="/foreign-buyer-guide"
                      target="_blank"
                    >
                      <Image className="icon" src={foreign} />
                      {t(`Foreign Buyer's Guide`)}
                    </a>
                  </li>
                </ul>
              </div>
              <div className="right">
                <ul>
                  <li>
                    <NavLink href="/policy" openNewTab>
                      <a aria-label={t("Privacy Policy")}>
                        {t("Privacy Policy")}
                      </a>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink href="/disclamer" openNewTab>
                      <a aria-label={t("Disclamer")}>{t("Disclamer")}</a>
                    </NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <CookiePolicy t={t} />
    </div>
  );
};

export default withMyTranslation(Form.create()(Layout));
