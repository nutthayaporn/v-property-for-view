import React from "react";
import Swiper from "react-id-swiper";
import _map from "lodash/map";

const whiteArrowLeft = "static/images/icon/white-arrow-left.svg";
const whiteArrowRight = "static/images/icon/white-arrow-left.svg";

const MySwiper = props => {
  const {
    swiperRef,
    items,
    uiRenderer,
    setting,
    showPagination = true
  } = props;

  const pagination = {
    el: ".swiper-pagination",
    type: "bullets",
    clickable: true,
    bulletElement: "div"
  };

  const params = {
    loop: true,
    pagination: showPagination ? pagination : {},
    ...setting
  };
  return (
    <Swiper ref={swiperRef} {...params}>
      {_map(items, (item, index) => {
        return <div key={index}>{uiRenderer(item, index)}</div>;
      })}
      {/* <div className="swiper-button-next">
        <Image src={whiteArrowRight} alt="" />
      </div>
      <div className="swiper-button-prev">
        <Image src={whiteArrowLeft} alt="" />
      </div> */}
    </Swiper>
  );
};

export default MySwiper;
