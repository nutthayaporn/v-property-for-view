import React, { useEffect } from "react";
import anime from "animejs";

const lineLeftClose = "M128,188 256,188 384,188";
const lineLeftOpen = "M158,158 256,256 354,158";

const lineCenterClose = "M128,256 384,256";
const lineCenterOpen = "M256,256 256,256";

const lineRightClose = "M128,324 256,324 384,324";
const lineRightOpen = "M158,354 256,256 354,354";

const animeConfig = {
  easing: "easeOutQuad",
  elasticity: 0,
  duration: 300
};

const HamburgerIcon = props => {
  const { open, color } = props;

  useEffect(() => {
    if (open) {
      anime({
        targets: "#line-left",
        d: lineLeftOpen,
        ...animeConfig
      });
      anime({
        targets: "#line-center",
        d: lineCenterOpen,
        ...animeConfig,
        duration: 200
      });
      anime({
        targets: "#line-right",
        d: lineRightOpen,
        ...animeConfig
      });
    } else {
      anime({
        targets: "#line-left",
        d: lineLeftClose,
        ...animeConfig
      });
      anime({
        targets: "#line-center",
        d: lineCenterClose,
        ...animeConfig
      });
      anime({
        targets: "#line-right",
        d: lineRightClose,
        ...animeConfig
      });
    }
  }, [open]);

  return (
    <svg viewBox="0 0 512 512" {...props}>
      <rect x={0} y={0} width={512} height={512} fill="none" />
      <g>
        <path
          d={lineLeftClose}
          stroke={color}
          fill="none"
          strokeWidth={open ? 10 : 15}
          id="line-left"
        />
        <path
          d={lineCenterClose}
          stroke={color}
          fill="none"
          strokeWidth={open ? 10 : 15}
          id="line-center"
        />
        <path
          d={lineRightClose}
          stroke={color}
          fill="none"
          strokeWidth={open ? 10 : 15}
          id="line-right"
        />
      </g>
    </svg>
  );
};

export default HamburgerIcon;

HamburgerIcon.defaultProps = {
  open: false,
  color: "#ffffff"
};
