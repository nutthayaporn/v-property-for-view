import React from "react";

const StatusBar = props => {
  const { status, fontColor, backgroundColor } = props;

  // let backgroundColor = "#000000";
  // if (status === "New Project") {
  //   backgroundColor = "#cdb659";
  // } else if (status === "Ready to Move In") {
  //   backgroundColor = "#69a699";
  // } else if (status === "Coming Soon") {
  //   backgroundColor = "#b46161";
  // }

  return (
    <div
      className="status-bar-component"
      style={{ backgroundColor: backgroundColor, color: fontColor }}
    >
      {status}
    </div>
  );
};

export default StatusBar;

StatusBar.defaultProps = {
  status: "New Project",
  fontColor: "#ffffff",
  backgroundColor: '#000000'
};
