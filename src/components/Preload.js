import React, { useState, useEffect } from "react";

const logo = "/static/images/logo.svg";

const duration = 1500;

const Preload = () => {
  const [hide, setHide] = useState(false);
  const [finished, setFinished] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      const root = document.querySelector("#root");
      if (!root) {
        return;
      }
      document.querySelector("#root").style.visibility = "visible";
      document.body.style.overflow = "auto";
      setHide(true);
    }, duration);

    setTimeout(() => {
      setFinished(true);
    }, duration + 500);

  }, []);

  if (finished) return null;

  return (
    <div className={`preload ${hide ? "hide" : "show"}`}>
      <div className="logo-block">
        <img className="logo" src={logo} />
        <div className="loading">
          <div className="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Preload;
