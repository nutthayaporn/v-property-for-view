import React, { Component } from "react";
import Slider from "react-slick";
import _map from "lodash/map";
import _get from "lodash/get";

import { Image, Icon, GalleryLightBoxZoom } from "../components";

function NextArrow(props) {
  const { className, style, onClick, color } = props;
  return (
    <div className={className} style={{ ...style }} onClick={onClick}>
      <Icon type="arrowNext" color={color} />
    </div>
  );
}

function PrevArrow(props) {
  const { className, style, onClick, color } = props;
  return (
    <div className={className} style={{ ...style }} onClick={onClick}>
      <Icon type="arrowPrev" color={color} />
    </div>
  );
}

export default class Gallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null,
      activeIndex: null
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }

  render() {
    const { items, t, color } = this.props;
    const { activeIndex } = this.state;
    return (
      <div className="gallery-component">
        <div className="slider1">
          <Slider
            fade={true}
            asNavFor={this.state.nav2}
            ref={slider => (this.slider1 = slider)}
          >
            {_map(items, (item, index) => {
              const image = _get(item, "image");
              const alt = t(_get(item, "alt"));
              return (
                <div
                  className="image-item-block"
                  key={index}
                  onClick={() => this.setState({ activeIndex: index })}
                >
                  <Image src={image} alt={alt} />
                </div>
              );
            })}
          </Slider>
        </div>
        {activeIndex !== null && (
          <GalleryLightBoxZoom
            imageCaption={item => {
              const title = t(_get(item, "title"));
              return (
                <div className="title-block">
                  <p style={{ color: color }}>{title}</p>
                </div>
              );
            }}
            active={activeIndex}
            images={items}
            onCloseRequest={() => this.setState({ activeIndex: null })}
          />
        )}
        <div className="slider2">
          <Slider
            asNavFor={this.state.nav1}
            ref={slider => (this.slider2 = slider)}
            slidesToShow={4}
            swipeToSlide={true}
            focusOnSelect={true}
            responsive={[
              {
                breakpoint: 680,
                settings: {
                  slidesToShow: 2
                }
              }
            ]}
            nextArrow={<NextArrow className="my-arrow" color={color} />}
            prevArrow={<PrevArrow className="my-arrow" color={color} />}
          >
            {_map(items, (item, index) => {
              const image = _get(item, "image");
              const title = t(_get(item, "title"));
              const alt = t(_get(item, "alt"));
              return (
                <div className="image-item-block" key={index}>
                  <Image src={image} alt={alt} />
                  <div className="name">
                    <p>{title}</p>
                  </div>
                </div>
              );
            })}
          </Slider>
        </div>
      </div>
    );
  }
}
