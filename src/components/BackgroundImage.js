import React from "react";
import styled from "styled-components";
import _includes from "lodash/includes";
// import lazyload from "react-lazyLoad";

const noImageAvailable = "/static/images/No_Image_Available.jpg";

const BackgroundImageStyled = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  background-image: url('${(props) => props.image}');
  background-size: ${(props) => props.size};
  background-repeat: ${(props) => props.repeat};
  background-position: ${(props) => props.position};
`;

const BackgroundImage = (props) => {
  const { className, image } = props;
  let newImage = image;
  if (_includes(newImage, "/upload/")) {
    newImage = `${process.env.serviceApiUrl}${image}`;
  }
  return (
    <BackgroundImageStyled
      {...props}
      className={`background-image-component ${className}`}
      image={newImage || noImageAvailable}
    />
  );
};

export default BackgroundImage;

BackgroundImage.defaultProps = {
  width: "100%",
  height: "100%",
  image: noImageAvailable,
  size: "cover",
  repeat: "no-repeat",
  position: "center",
};
