import React from "react";
import _get from "lodash/get";

import { Swiper, BackgroundImage, Image } from "../../components";

const mobileImage = "/static/images/pages/about/Line.png";

const activities = [
  {
    title:
      "วีธารา 36 ปลื้มโกยยอดขายกว่า 100 ลบ. จาก Exclusive Seminar “ลงทุนอสังหากี่ครั้งก็กำไร” โดย 2GURU 2Gen",
    date: "06 july 2019",
    image: "https://www.vproperty.co.th/photo/news/news_4992144362.jpg"
  },
  {
    title: "Review : VTARA36",
    date: "06 july 2019",
    image: "https://www.vproperty.co.th/photo/news/news_7168590078.png"
  },
  {
    title:
      "Review: VTARA36 คอนโดระดับ Luxury พร้อม Courtyard Villa ใกล้ BTSทองหล่อ (PROP2MORROW)",
    date: "06 july 2019",
    image: "http://www.vtara36.com/storage/news/TRUSTWORTHY.png"
  },
  {
    title:
      "V Property ผลงานไตรมาส 1/60 ทะลุเป้า ท้าชนยักษ์ อสังหา - ชิงเค้กคอนโด Luxury (ประชาชาติธุรกิจ)",
    date: "06 july 2019",
    image: "https://www.vproperty.co.th/photo/news/news_1151335044.jpg"
  }
];

const customers = [
  {
    title:
      "ไม่ใช่เรื่องง่ายที่จะให้ใครสักคนตกหลุมรักโดยยังไม่รู้จักกันจริงๆ แต่สำหรับคุณอิศราภรณ์ โชติช่วง นักธุรกิจหญิงมากประสบการณ์",
    description:
      "เหตุผลที่ตัดสินใจจริงๆก็ เป็นคนที่ชอบทำเลโซนสุขุมวิทอยู่แล้ว และบังเอิญก็เห็นเขากำลังพรีเซลกันอยู่ ราคาก็น่าสนใจดี คอนเซปต์ก็ดูสวยอะไรอย่างนี้ เห็นว่าดีไซน์เขาสวย แล้วก็ดูฟั่งชั่นมันครบ เราก็เลยตัดสินใจได้ไม่ยาก",
    date: "06 july 2019",
    mediaType: "Image",
    image: "https://www.vproperty.co.th/photo/news/news_2408816821.jpg"
  },
  {
    title:
      "เหตุผลที่ตัดสินใจเลือกซื้อทุกโครงการในเครือ V Property ของคุณวงศพัทธ์ ปิยเศรษฐ์",
    description:
      "ด้วยความเป็นคนมองการณ์ไกลเรื่องการลงทุนในคอนโด สิ่งแรกที่คุณวงศพัทธ์ ปิยเศรษฐ์ ลูกค้าโครงการ H Condo ให้ความสำคัญคือเรื่อง โลเคชั่น และการเลือกสรรวัสดุทุกชิ้นทั้งในห้องพักและ Facility ส่วนกลาง",
    date: "06 july 2019",
    mediaType: "Image",
    image: "https://www.vproperty.co.th/photo/news/news_7878180285.jpg"
  },
  {
    title: "H Sukhumvit 43 & VTARA36 : Customer Testimonials",
    description:
      "เหตุผลที่ตัดสินใจจริงๆก็ เป็นคนที่ชอบทำเลโซนสุขุมวิทอยู่แล้ว และบังเอิญก็เห็นเขากำลังพรีเซลกันอยู่ ราคาก็น่าสนใจดี คอนเซปต์ก็ดูสวยอะไรอย่างนี้ เห็นว่าดีไซน์เขาสวย แล้วก็ดูฟั่งชั่นมันครบ เราก็เลยตัดสินใจได้ไม่ยาก",
    date: "06 july 2019",
    mediaType: "Video",
    link: "https://www.youtube.com/embed/5OWyihcR1U4",
    image: "https://www.vproperty.co.th/photo/news/news_6081689677.jpg"
  }
];

const VFamily = () => {
  return (
    <div className="v-family-tab">
      <div className="activity-block">
        <h2>กิจกรรม</h2>
        <div className="desktop">
          <Swiper
            items={activities}
            setting={{
              slidesPerView: 5,
              spaceBetween: 40,
              slidesPerView: "auto",
              centeredSlides: true,
              loop: true
            }}
            uiRenderer={(item, index) => {
              const image = _get(item, "image");
              const title = _get(item, "title");
              const date = _get(item, "date");
              return (
                <div className="item-slide" key={index}>
                  <div className="background-block">
                    <BackgroundImage image={image} />
                  </div>
                  <div className="detail">
                    <p className="date">{date}</p>
                    <p className="title">{title}</p>
                  </div>
                </div>
              );
            }}
          />
        </div>
        <div className="mobile">
          <div className="container">
            <Swiper
              items={activities}
              uiRenderer={(item, index) => {
                const image = _get(item, "image");
                const title = _get(item, "title");
                const date = _get(item, "date");
                return (
                  <div className="item-slide" key={index}>
                    <div className="background-block">
                      <BackgroundImage image={image} />
                    </div>
                    <div className="detail">
                      <p className="date">{date}</p>
                      <p className="title">{title}</p>
                    </div>
                  </div>
                );
              }}
            />
          </div>
        </div>
      </div>
      <div className="block2">
        <div className="container">
          <div className="row">
            <div className="col-md-7">
              <div className="left-block">
                <h2>แจ้งซ่อมออนไลน์</h2>
                <p className="title">
                  บริการแจ้งซ่อม Online ให้ลูกบ้าน V Property สะดวก รวดเร็ว
                  ในการแจ้งซ่อมเรื่องบ้านตลอด 24 ชม. สำหรับโครงการบ้านเดี่ยว
                  และทาวน์โฮม ในกรุงเทพฯและปริมณฑล
                  (บริการนี้ยังไม่รวมคอนโดและโครงการ V Property ในจังหวัดอื่นๆ)
                  เมื่อแจ้งซ่อม Online แล้ว ทางเจ้าหน้าที่ Service Center
                  จะติดต่อกลับในช่วงเวลาทำการ 9.00-17.30 น. ใน 24 ชม.
                </p>
                <ul>
                  <li>
                    <p>
                      ลูกบ้าน V Property จะได้รับ Sign-in name และ Password
                      เมื่อโอนบ้าน เพื่อเข้าใช้บริการ
                    </p>
                  </li>
                  <li>
                    <p>
                      สำหรับท่านที่ไม่ได้รับ Sign-in name และ Password
                      สามารถติดต่อได้ที่ Service Center โทร 1198 กด 2 หรือ
                      Email: c_service@vproperty.co.th
                    </p>
                  </li>
                  <li>
                    <p>
                      ขอสงวนสิทธิ์ช่องทางนี้สำหรับการแจ้งซ่อมเท่านั้น
                      หากลูกบ้านต้องการร้องเรียน เรื่องอื่นๆ ที่ไม่ใช่ซ่อมบ้าน
                      กรุณาใช้บริการที่ "ร้องเรียนเรื่องบ้าน"
                    </p>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md-5">
              <div className="right-block">
                <Image src={mobileImage} />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="block3">
        <h2>ลูกค้าสัมพันธ์</h2>
        <div className="desktop">
          <Swiper
            items={customers}
            setting={{
              slidesPerView: 3,
              spaceBetween: 40,
              slidesPerView: "auto",
              centeredSlides: true,
              loop: true
            }}
            uiRenderer={(item, index) => {
              const image = _get(item, "image");
              const title = _get(item, "title");
              const description = _get(item, "description");
              const date = _get(item, "date");
              const mediaType = _get(item, "mediaType");
              const video = _get(item, "link");
              return (
                <div className="item-slide" key={index}>
                  <div className="background-block">
                    {mediaType === "Image" && (
                      <BackgroundImage image={image} />
                    )}
                    {/* {mediaType === "Image" && <Image src={image} />} */}
                    {mediaType === "Video" && (
                      <iframe
                        src={video}
                        allowFullScreen
                        allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                      />
                    )}
                  </div>
                  <div className="detail-block">
                    <div className="detail">
                      <p className="date">{date}</p>
                      <p className="title">{title}</p>
                      <p className="description">{description}</p>
                    </div>
                  </div>
                </div>
              );
            }}
          />
        </div>
        <div className="mobile">
          <Swiper
            items={customers}
            uiRenderer={(item, index) => {
              const image = _get(item, "image");
              const title = _get(item, "title");
              const description = _get(item, "description");
              const date = _get(item, "date");
              return (
                <div className="item-slide" key={index}>
                  <div className="background-block">
                    <BackgroundImage image={image} />
                  </div>
                  <div className="detail-block">
                    <div className="detail">
                      <p className="date">{date}</p>
                      <p className="title">{title}</p>
                      <p className="description">{description}</p>
                    </div>
                  </div>
                </div>
              );
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default VFamily;
