import React from "react";
import _get from "lodash/get";
import _map from "lodash/map";

import { BackgroundImage, Image, Swiper, Icon } from "../../components";
import { withMyTranslation } from "../../hoc";

const person1 = "/static/images/pages/about/vproperty-brand/image-person.jpg";
const personMobile1 =
  "/static/images/pages/about/vproperty-brand/image-person-mobile.jpg";
const block4Bg = "/static/images/pages/about/vproperty-brand/block4-bg.jpg";
const background2 = "/static/images/pages/about/bg-about-2.jpg";

const persons = [
  {
    image: person1,
    imageMobile: personMobile1,
    name: "คุณพรชัย เลิศอนันต์โชค",
    position: "ประธานเจ้าหน้าที่บริหาร",
    description:
      "“จากประสบการณ์อันยาวนาน บริษัทฯ ได้เรียนรู้ เพื่อนำมาพัฒนาทั้งตัวโปรดักส์, บริการ, บุคลากรให้มีคุณภาพและประสิทธิภาพเพิ่มมากขึ้น โดยให้ ลูกค้าเป็นศูนย์กลาง มาปรับใช้ตั้งแต่การเพิ่ม Innovation ซึ่งตอบสนองที่แท้จริงของการใช้ชีวิต ไปจนถึงการออกแบบดีไซน์พัฒนาโครงการที่อยู่อาศัยที่สามารถ ตอบโจทย์ทุกความต้องการและเพื่อมอบความสุขให้ผู้อยู่อาศัยแก่ คนเจเนอเรชั่นใหม่อย่างแท้จริง”"
  }
];

const points = [
  {
    name: "The Finest Living Experience",
    image: <Icon type="h" color="#262262" />
  },
  {
    name: "DISCOVER THE OASIS OF SUKHUMVIT",
    image: <Icon type="vtara" color="#262262" />
  },
  { name: "1 Minute BTS. Onnut", image: <Icon type="ikon" color="#ed5828" /> },
  {
    name:
      " LIFE HAS NO FIXED RULES... YOUR SIGNATURE LIFESTYLE MUST BE FULLYREALIZE",
    image: <Icon type="vertier" color="#9e4a4a" />
  }
];

const aboutUs = {
  title: {
    en: "V Property Development",
    th: "วี พร๊อพเพอร์ตี้ ดีเวลลอปเมนท์"
  },
  block1: {
    en:
      "V Property Development was founded in 2009 by Sapphire Holding Group, an eminent gems exporter in Thailand. Regards to over 40 years of experiences in jewelry export business field, The Sapphire Holding Group has all been recognized in world-class market. And now, the company has expanded the new line of business to real estate development which is known as V Property Development Co., Ltd.",
    th:
      "วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ ก่อตั้งเมื่อปี 2552 โดย แซปไฟร์ โฮลดิ้ง กรุ๊ป กลุ่มบริษัทส่งออกอัญมณีรายใหญ่ในเมืองไทย ด้วยประสบการณ์ กว่า 40 ปี กับธุรกิจส่งออกอัญมณี ทำให้กลุ่มบริษัทฯ เป็นที่รู้จักในตลาดอัญมณีทั่วโลก ปัจจุบันทางกลุ่มบริษัทฯ ได้ขยายธุรกิจด้านอสังหาริมทรัพย์"
  },
  block2: {
    en:
      "V Property Development strives to develop only the real estate projects that can both render the ultimate living comfort which never compromise the exquisite design and tranquility and the supreme convenience of location on prime Sukhumvit area such as Phromphong and Thonglor.",
    th:
      "วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ มุ่งเน้น และเอาใจใส่ทุกรายละเอียดของการพัฒนาโครงการ ที่ต้องการให้เป็นโครงการคอนโดมิเนียมที่เป็นที่สุดของที่พักอาศัยท่ามกลางธรรมชาติและความเงียบสงบสุดยอดทำเลใจกลางสุขุมวิท"
  },
  corruptionPolicy: {
    title: {
      en: "V Family",
      th: "นโยบายต่อต้านการทุจริตและคอร์รัปชั่น"
    },
    block1: {
      en:
        "“The Company” recognizes the important of a transparent business operations and embrace the principle of good corporate governance for effectiveness and equitably with responsibility to society and stakeholders. As well as to prevent corruption that may occur, hence the Company has declared the intention of anti-corruption that do not support the activities of groups or individuals acting in wrongful exploitation. In order to be entrusted in undertaking a no fraud operation, the Company has proclaimed the anti-corruption policy as the guidelines for the Board of Directors, management, and employees to strictly comply to the following;",
      th:
        "“บริษัทฯ” ตระหนักถึงความสำคัญของการดำเนินกิจการอย่างโปร่งใส ยึดหลักการกำกับดูแลกิจการที่ดีเพื่อให้มีการดำเนินกิจการอย่างมีประสิทธิภาพ เป็นธรรม มีความรับผิดชอบต่อสังคมและผู้มีส่วนได้เสีย และป้องกันการทุจริตคอรัปชั่นที่อาจเกิดขึ้นในองค์กรทุกๆ กรณี บริษัทฯ จึงกำหนดหลักการดำเนินกิจการที่ไม่สนับสนุนกิจการของกลุ่มหรือบุคคลที่กระทำการโดยมิชอบในการแสวงหาผลประโยชน์ เพื่อให้บริษัทฯ ได้รับความไว้วางใจในการดำเนินกิจการอันปราศจากการทุจริต บริษัทฯ จึงได้จัดทำนโยบายต่อการคอร์รัปชั่นขึ้น และเพื่อเป็นแนวทางสำหรับคณะกรรมการ ผู้บริหาร และพนักงานปฏิบัติตามอย่างเคร่งครัด โดยมีรายละเอียดนโยบาย ดังนี้"
    },
    definition: {
      title: {
        en: "Definition",
        th: "คำนิยาม"
      },
      detail: {
        en:
          "“Corruption” defined as the abuse of power to exploitation, whether in bribery or in other forms by offering or receiving from public sectors and private sectors including favor oneself or ally. However, there are exceptional cases for the activities that legal, tradition, and commercial conservative which can be performed.",
        th:
          "“การคอร์รัปชั่น” หมายถึง การใช้อำนาจที่ได้มาโดยหน้าที่ในการหาประโยชน์ส่วนตัว การให้สินบนไม่ว่าจะเป็นรูปแบบใดก็ตาม โดยการเสนอให้ การสัญญา การให้คำมั่น การเรียกร้อง หรือการรับซึ่งเงิน หรือทรัพย์สินและผลประโยชน์อื่นๆ ที่ไม่เหมาะสม ทั้งจากเจ้าหน้าที่ของรัฐ หน่วยงานภาครัฐ หน่วยงานเอกชน รวมถึงการเอื้อประโยชน์ในทางธุรกิจให้กับตนเองหรือพวกพ้อง ในการปฏิบัติหน้าที่อันนำมาซึ่งผลประโยชน์ในทางมิชอบ เว้นแต่เป็นการกระทำในกรณีที่กฏหมาย ระเบียบปฏิบัติ ประกาศ ข้อบังคับ ขนบธรรมเนียม ประเพณีท้องถิ่น หรือจารีตทางการค้าให้กระทำได้"
      }
    },
    policy: {
      title: {
        en: "Anti-Corruption Policy",
        th: "นโยบายต่อต้านการคอร์รัปชั่น"
      },
      detail: {
        en:
          "Restricted the Board of Directors, management, and employees of the Company claim, perform, or accept corruption in all forms; both directly and indirectly. The Company has determined the reviewing of the anti-corruption policy’s implementation regularly. Moreover, the roles and responsibilities of the parties involved to comply with the policies and operations of the Company.",
        th:
          "ห้ามให้คณะกรรมการ ผู้บริหาร และพนักงานของบริษัทฯ เรียกร้อง ดำเนินการหรือยอมรับการทุจริตในทุกรูปแบบทั้งทางตรงและทางอ้อม เพื่อให้เกิดประโยชน์กับองค์กรและการดำเนินธุรกิจขององค์กร และกำหนดให้มีการสอบทานการปฏิบัติตามนโยบายการต่อต้านคอรัปชั่นอย่างสม่ำเสมอ พร้อมทั้งได้มีการกำหนดบทบาทหน้าที่และความรับผิดชอบของฝ่ายที่เกี่ยวข้อง เพื่อให้สอดคล้องกับนโยบายและการดำเนินกิจการของบริษัทฯ"
      }
    }
  }
};

const VpropertyBrand = props => {
  const { t } = props;
  return (
    <div className="vproperty-brand-tab">
      <div className="block1">
        <div className="container">
          <div className="block1-1" style={{ paddingBottom: 200 }}>
            <h2>{t(_get(aboutUs, "title"))}</h2>
            <p>{t(_get(aboutUs, "block1"))}</p>
            <p>{t(_get(aboutUs, "block2"))}</p>
          </div>
          {/* <div className="block1-2">
            <h2>{t(_get(aboutUs, "corruptionPolicy.title"))}</h2>
            <p>{t(_get(aboutUs, "corruptionPolicy.block1"))}</p>
            <div className="policy-block">
              <div className="row">
                <div className="col-md-6">
                  <div className="block">
                    <p className="bold">
                      {t(_get(aboutUs, "corruptionPolicy.definition.title"))}
                    </p>
                    <p>
                      {t(_get(aboutUs, "corruptionPolicy.definition.detail"))}
                    </p>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="block">
                    <p className="bold">
                      {t(_get(aboutUs, "corruptionPolicy.policy.title"))}
                    </p>
                    <p>{t(_get(aboutUs, "corruptionPolicy.policy.detail"))}</p>
                  </div>
                </div>
              </div>
            </div>
          </div> */}
        </div>
      </div>

      <div className="block3">
        <Swiper
          items={persons}
          uiRenderer={(item, index) => {
            const image = _get(item, "image");
            const imageMobile = _get(item, "imageMobile");
            const name = _get(item, "name");
            const position = _get(item, "position");
            const description = _get(item, "description");
            return (
              <div className="item-slide" key={index}>
                <div className="item-slide-image-block">
                  <Image src={image} />
                </div>
                <div className="detail">
                  <p className="name">{name}</p>
                  <p className="position">{position}</p>
                  <p className="description">{description}</p>
                </div>
                {/* <div className="row">
                  <div className="col-md-6">
                    <div className="item-slide-image-block">
                      <Image src={image} />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="detail">
                      <p className="name">{name}</p>
                      <p className="position">{position}</p>
                      <p className="description">{description}</p>
                    </div>
                  </div>
                </div> */}
              </div>
            );
          }}
        />
      </div>
      <div className="block4">
        <div className="background-block">
          <BackgroundImage image={block4Bg} />
        </div>
        <div className="container">
          <div className="title">
            จุดเด่นของ V Property คือ สามารถดูแลลูกค้าได้อย่างทั่วถึง
          </div>
          <p className="description">
            เนื่องจากจะบริการแค่ลูกค้าภายใน (Exclusive Service)
            ไม่ได้รับบริการคนภายนอก เหมือน Agent หรือบริษัทอื่นๆ
            ซึ่งจะทำให้การดูแลไม่ทั่วถึง
            โดยทั้งหมดนี้เริ่มต้นมาจากแนวคิดที่ต้องการให้ลูกค้าทุกคนเป็น V-VIP
            หรือ V Property – Very Important Person ซึ่งหมายถึง V Care 360 องศา
            ถือเป็นข้อได้เปรียบของ V Property ที่ใช้แข่งกับยักษ์อสังหาฯรายอื่น
          </p>
          <div className="content">
            {_map(points, (point, index) => {
              const image = _get(point, "image");
              const name = _get(point, "name");
              return (
                <div className="logo-block" key={index}>
                  {image}
                  <p className="name">{name}</p>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className="block5">
        <Image className="bg-image" src={background2} />
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="block">
                <h2>วิสัยทัศน์</h2>
                <p className="description">
                  วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์
                  มุ่งหวังเป็นส่วนหนึ่งในการสร้างสรรค์ปัจจัยพื้นฐานที่สมบูรณ์
                  เพื่อเติมเต็มสังคมให้น่าอยู่ขึ้น
                  เพื่อมอบความสุขที่ทุกคนสามารถสัมผัสได้
                  ด้วยอสังหาริมทรัพท์ย่านใจกลางเมือง
                  ที่มาพร้อมกับความทันสมัยและเสริมสร้างมูลค่าให้กับผู้ซื้อได้มากที่สุด
                </p>
              </div>
            </div>
            <div className="col-md-6">
              <div className="block block-2">
                <h2>พันธกิจ</h2>
                <p className="description">
                  เรามุ่งสร้างสรรค์อาคารที่พักอาศัยในทำเลใจกลางเมือง
                  ในราคาจับต้องได้
                  ด้วยการดีไซน์และฟังก์ชั่นที่ตอบทุกโจทย์ของผู้อยู่อาศัย
                  เสมือนเป็นมูลค่าทางหลักทรัพย์ที่ให้ผลตอบแทนกับผู้ถือครองได้มากที่สุด
                  พร้อมให้บริการครบวงจรตั้งแต่ขาย ให้บริการหาผู้เช่า รับฝากขาย
                  พร้อมบริหารอาคารเพื่อสร้างมูลค่าให้งอกเงยสูงสุด
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withMyTranslation(VpropertyBrand);
