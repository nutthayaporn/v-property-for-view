import React, { useState } from "react";
import _get from "lodash/get";
import _map from "lodash/map";

import { withMyTranslation } from "../../hoc";
import { Image, Swiper } from "../../components";

const awards = [
  {
    id: 1,
    image: "https://www.pf.co.th/images/about-us/awards/award-2551.png",
    content: (
      <div>
        <p className="title">
          บริษัทได้รับรางวัลจากการประกวด
          “โครงการบ้านจัดสรรอนุรักษ์พลังงานดีเด่น”
        </p>
        <p className="description">
          ยกรมพัฒนาพลังงานทดแทน และอนุรักษ์พลังงาน กระทรวงพลังงาน ซึ่งมอบรางวัล
          ให้แก่แบบบ้าน และโครงการจัดสรร ที่มีความใส่ใจในเรื่องของการ
          อนุรักษ์พลังงาน ทั้งการออกแบบ การเลือกใช้วัสดุอุปกรณ์ การวางผัง
          ตลอดจนมีการบริหารจัดการที่ดี โดยบริษัทได้รับ รางวัลในประเภทต่างๆ รวม
          10 รางวัล ได้แก่
        </p>
        <ul>
          <li>
            <p>
              รางวัลประเภท บ้านเดี่ยวขนาดเล็ก
              <span>
                พื้นที่ใช้สอยไม่เกิน 200 ตารางเมตร จำนวน 3 รางวัล จาก โครงการ
                เพอร์เฟค เพลส รัตนาธิเบศร์ และ โครงการ เพอร์เฟค เพลส สุขุมวิท
                77-สุวรรณภูมิ
              </span>
            </p>
          </li>
          <li>
            <p>
              รางวัลประเภท บ้านเดี่ยวขนาดกลาง
              <span>
                พื้นที่ใช้สอย 200-300 ตารางเมตร จำนวน 2 รางวัล จาก โครงการ
                เพอร์เฟค มาสเตอร์พีซ รัตนาธิเบศร์
              </span>
            </p>
          </li>
          <li>
            <p>
              รางวัลประเภท บ้านแถว (ทาวน์เฮ้าส์)
              <span>
                พื้นที่ใช้สอยไม่เกิน 120 ตารางเมตร จำนวน 1 รางวัล จาก โครงการ
                เดอะ วิลล่า รัตนาธิเบศร์ รางวัลประเภท บ้านแถว (ทาวน์เฮ้าส์)
                พื้นที่ใช้สอยมากกว่า 120 ตารางเมตร จำนวน 1 รางวัล จาก โครงการ
                เดอะ เมทโทร พระราม 9
              </span>
            </p>
          </li>
          <li>
            <p>
              รางวัลประเภท โครงการจัดสรรขนาดเล็ก
              <span>
                จำนวนแปลงย่อยไม่เกิน 99 แปลง จาก โครงการ มณีรินทร์ พาร์ค 2
                รังสิต
              </span>
            </p>
          </li>
          <li>
            <p>
              รางวัลประเภท โครงการจัดสรรขนาดกลาง
              <span>
                จำนวนแปลงย่อยตั้งแต่ 100-299 แปลง จาก โครงการ เพอร์เฟค
                มาสเตอร์พีซ เอกมัย- รามอินทรา
              </span>
            </p>
          </li>
          <li>
            <p>
              รางวัลประเภท โครงการจัดสรรขนาดใหญ่
              <span>
                จำนวนแปลงย่อยตั้งแต่ 300 แปลงขึ้นไป จาก โครงการ เพอร์เฟค เพลส
                สุขุมวิท 77- สุวรรณภูมิ
              </span>
            </p>
          </li>
        </ul>
      </div>
    )
  },
  {
    id: 2,
    image: "https://www.pf.co.th/images/about-us/awards/award-2557.png",
    content: (
      <div>
        <p className="title">
          บริษัทได้รับรางวัลจากการประกวด
          “โครงการบ้านจัดสรรอนุรักษ์พลังงานดีเด่น”
        </p>
        <p className="description">
          ยกรมพัฒนาพลังงานทดแทน และอนุรักษ์พลังงาน กระทรวงพลังงาน ซึ่งมอบรางวัล
          ให้แก่แบบบ้าน และโครงการจัดสรร ที่มีความใส่ใจในเรื่องของการ
          อนุรักษ์พลังงาน ทั้งการออกแบบ การเลือกใช้วัสดุอุปกรณ์ การวางผัง
          ตลอดจนมีการบริหารจัดการที่ดี โดยบริษัทได้รับ รางวัลในประเภทต่างๆ รวม
          10 รางวัล
        </p>
      </div>
    )
  },
  {
    id: 3,
    image: "https://www.pf.co.th/images/about-us/awards/award-2551.png",
    content: (
      <div>
        <p className="title">
          บริษัทได้รับรางวัลจากการประกวด
          “โครงการบ้านจัดสรรอนุรักษ์พลังงานดีเด่น”
        </p>
        <p className="description">
          ยกรมพัฒนาพลังงานทดแทน และอนุรักษ์พลังงาน กระทรวงพลังงาน ซึ่งมอบรางวัล
          ให้แก่แบบบ้าน และโครงการจัดสรร ที่มีความใส่ใจในเรื่องของการ
          อนุรักษ์พลังงาน ทั้งการออกแบบ การเลือกใช้วัสดุอุปกรณ์ การวางผัง
          ตลอดจนมีการบริหารจัดการที่ดี โดยบริษัทได้รับ รางวัลในประเภทต่างๆ รวม
          10 รางวัล
        </p>
      </div>
    )
  }
];

const data = {
  title: {
    en: "History",
    th: "ประวัติความเป็นมา"
  },
  detail: {
    en: `    <div>
        <p>
          V Property Development was founded in 2009 by Sapphire Holding Group,
          an eminent gems exporter in Thailand. Regards to over 40 years of
          experiences in jewelry export business field, The Sapphire Holding
          Group has all been recognized in world-class market. And now, the
          company has expanded the new line of business to real estate
          development which is known as V Property Development Co., Ltd.
        </p>
        <br />
        <p>
          V Property Development strives to develop only the real estate
          projects that can both render the ultimate living comfort which never
          compromise the exquisite design and tranquility and the supreme
          convenience of location on prime Sukhumvit area such as Phromphong and
          Thonglor.
        </p>
      </div>`,
    th: `<div>
        <p>
          วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ ก่อตั้งเมื่อปี 2552 โดย แซปไฟร์ โฮลดิ้ง
          กรุ๊ป กลุ่มบริษัทส่งออกอัญมณีรายใหญ่ในเมืองไทย ด้วยประสบการณ์ กว่า 40
          ปี กับธุรกิจส่งออกอัญมณี ทำให้กลุ่มบริษัทฯ
          เป็นที่รู้จักในตลาดอัญมณีทั่วโลก ปัจจุบันทางกลุ่มบริษัทฯ
          ได้ขยายธุรกิจด้านอสังหาริมทรัพย์
        </p>
        <br />
        <p>
          วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ มุ่งเน้น
          และเอาใจใส่ทุกรายละเอียดของการพัฒนาโครงการ
          ที่ต้องการให้เป็นโครงการคอนโดมิเนียมที่เป็นที่สุดของที่พักอาศัยท่ามกลางธรรมชาติและความเงียบสงบสุดยอดทำเลใจกลางสุขุมวิท
        </p>
      </div>`
  }
};

const AboutVproperty = props => {
  const { t } = props;
  const [activeIndex, setActive] = useState(0);

  const content = _get(awards, `${activeIndex}.content`);
  const imageAward = _get(awards, `${activeIndex}.image`);
  return (
    <div className="about-vproperty-tab">
      <div className="container">
        <div
          className="header-title"
          dangerouslySetInnerHTML={{ __html: t(_get(data, "title")) }}
        />
        <div
          className="header"
          dangerouslySetInnerHTML={{ __html: t(_get(data, "detail")) }}
        />
        <div className="video-block">
          <iframe
            width="100%"
            height="100%"
            src="https://www.youtube.com/embed/sYMotdiiU3M"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </div>
        {/* <div className="container-award">
          <h2>ความภูมิใจของเรา</h2>
          <div className="desktop">
            <div className="block1">
              <div className="row">
                <div className="col-md-4">
                  <div className="left-block">
                    <Image key={activeIndex} src={imageAward} />
                  </div>
                </div>
                <div className="col-md-8">
                  <div className="description-block">{content}</div>
                </div>
              </div>
            </div>
            <div className="block2">
              {_map(awards, (award, index) => {
                const image = _get(award, "image");
                return (
                  <div
                    className={`image-award ${
                      activeIndex === index ? "active" : ""
                    }`}
                    key={index}
                    onClick={() => setActive(index)}
                  >
                    <Image src={image} />
                  </div>
                );
              })}
            </div>
          </div>
          <div className="mobile">
            <Swiper
              items={awards}
              setting={{
                navigation: {
                  nextEl: ".next-button",
                  prevEl: ".prev-button",
                  renderPrevButton: () => <div className="prev-button" />,
                  renderNextButton: () => <div className="next-button" />
                }
              }}
              uiRenderer={(item, index) => {
                const image = _get(item, "image");
                const content = _get(item, "content");
                return (
                  <div className="award-mobile-block" key={index}>
                    <div className="image-block">
                      <Image src={image} />
                    </div>
                    <div className="description-block">{content}</div>
                  </div>
                );
              }}
            />
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default withMyTranslation(AboutVproperty);
