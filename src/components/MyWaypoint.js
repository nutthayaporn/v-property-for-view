import React, { useState } from "react";
import { Waypoint } from "react-waypoint";
import _map from "lodash/map";
import styled, { css } from "styled-components";

const ChildrenStyled = styled.div`
  width: 100%;
  height: 100%;
  transition: all 1s;
  ${props =>
    props.isDefaultOpacity
      ? css`
          opacity: ${props => (props.isAppear ? 1 : 0)};
        `
      : css`
          opacity: 1;
        `}
`;

const MyWaypoint = props => {
  const { reappear, isDefaultOpacity } = props;
  const [isAppear, setIsAppear] = useState(false);

  const onEnter = () => {
    if (isAppear) return;
    setIsAppear(true);
  };

  const onLeave = () => {
    if (!isAppear) return;
    if (!reappear) return;
    setIsAppear(false);
  };

  const doAnime = anime => {
    _map(anime, value => {
      value();
    });
  };

  if (isAppear) {
    if (props.anime) {
      setTimeout(() => {
        doAnime(props.anime);
      }, 1);
    }
  }
  return (
    <Waypoint {...props} onEnter={onEnter} onLeave={onLeave}>
      <ChildrenStyled isAppear={isAppear} isDefaultOpacity={isDefaultOpacity}>
        {isAppear ? props.children : <span>{props.children}</span>}
      </ChildrenStyled>
    </Waypoint>
  );
};

export default MyWaypoint;

MyWaypoint.defaultProps = {
  reappear: false,
  isDefaultOpacity: true
};
