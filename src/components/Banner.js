import React from "react";

import { MouseScrollWheel, BackgroundImage } from "../components";

const Banner = props => {
  const {
    imageDesktopBackground,
    imageMobileBackground,
    title,
    subTitle,
    titlePosition
  } = props;
  return (
    <div className="banner-component">
      <div className="banner">
        <BackgroundImage
          className="banner-desktop"
          image={imageDesktopBackground}
        />
        <BackgroundImage
          className="banner-mobile"
          image={imageMobileBackground}
        />
      </div>
      <div
        className={`banner-title-block ${
          titlePosition === "right" ? "right" : "left"
        }`}
      >
        {title && <p className="banner-title-bold">{title}</p>}
        {subTitle && <p className="banner-title">{subTitle}</p>}

        <div className="mouse-block">
          <MouseScrollWheel />
        </div>
      </div>
    </div>
  );
};

export default Banner;

Banner.defaultProps = {
  imageDesktopBackground: "",
  imageMobileBackground: "",
  title: "",
  subTitle: "",
  titlePosition: "right"
};
