import React from "react";
import { Button, Icon } from "antd";

const MyButton = props => {
  const { children, type } = props;
  if (type === "arrow-right") {
    return (
      <Button className="mybutton mybutton-arrow" {...props}>
        <Icon type="arrow-right" />
      </Button>
    );
  } else if (type === "arrow-left") {
    return (
      <Button className="mybutton mybutton-arrow" {...props}>
        <Icon type="arrow-left" />
      </Button>
    );
  } else if (type === "active") {
    return (
      <Button className="mybutton mybutton-active" {...props}>
        {children}
      </Button>
    );
  } else if (type === "white") {
    return (
      <Button className="mybutton mybutton-text-white" {...props}>
        {children}
      </Button>
    );
  } else if (type === "black") {
    return (
      <Button className="mybutton mybutton-text-black" {...props}>
        {children}
      </Button>
    );
  } else if (type === "plus") {
    return (
      <Button className="mybutton mybutton-plus" {...props}>
        <Icon type="plus" />
      </Button>
    );
  } 
  return (
    <Button className="mybutton" {...props}>
      {children}
    </Button>
  );
};

export default MyButton;
