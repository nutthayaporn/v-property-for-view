import React, { Component } from "react";
import Script from "react-load-script";

class YouTube extends Component {
  constructor(props) {
    super(props);
    this.player = null;
    this.state = {
      scriptLoaded: false,
      scriptError: false
    };
  }
  onYouTubeIframeAPIReady = () => {
    const { videoId } = this.props;
    this.player = new YT.Player(videoId, {
      width: "100%",
      height: "100%",
      videoId
    });
  };
  handleScriptCreate = () => {
    this.setState({ scriptLoaded: false });
  };

  handleScriptError = () => {
    this.setState({ scriptError: true });
  };

  handleScriptLoad = () => {
    this.setState({ scriptLoaded: true });
    setTimeout(() => {
      this.onYouTubeIframeAPIReady();
    }, 1000);
  };

  render() {
    const { videoId } = this.props;
    const { scriptError } = this.state;
    if (scriptError) return;
    return (
      <>
        <Script
          url="https://www.youtube.com/iframe_api"
          onCreate={this.handleScriptCreate}
          onError={this.handleScriptError}
          onLoad={this.handleScriptLoad}
        />
        <div id={videoId} className="youtube-player" />
      </>
    );
  }
}

export default YouTube;
