import React from "react";

const MetaSocialShare = props => {
  const { title, description, image, path, date } = props;
  
  return (
    <>
      <meta property="og:url" content={`${process.env.baseUrl}${path}`} />
      <meta property="og:type" content="article" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
      <meta
        property="og:site_name"
        key="og:site_name"
        content="V PROPERTY DEVELOPMENT"
      />
      <meta
        property="article:published_time"
        key="article:published_time"
        content={date}
      />
      <meta
        property="article:modified_time"
        key="article:modified_time"
        content={date}
      />
      <meta
        property="article:author"
        key="article:author"
        content="V PROPERTY"
      />

      {/* twitter */}
      <meta
        name="twitter:card"
        key="twitter:card"
        content="summary_large_image"
      />
      <meta name="twitter:title" key="twitter:title" content={title} />
      <meta
        name="twitter:description"
        key="twitter:description"
        content={description}
      />
      <meta name="twitter:url" key="twitter:url" content={`${process.env.baseUrl}${path}`} />
      <meta name="twitter:image" key="twitter:image" content={image} />
    </>
  );
};

export default MetaSocialShare;

MetaSocialShare.defaultProps = {
  title: "",
  description: "",
  image: "",
  path: "",
  date: ""
};
