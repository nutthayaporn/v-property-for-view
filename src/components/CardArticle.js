import React from "react";
import dayjs from "dayjs";
import AspectRatio from "react-ratio";

import { Router } from "../i18n";
import { Icon, MyButton } from "../components";
import { withMyTranslation } from "../hoc";

const CardArticle = (props) => {
  const { image, date, title, id, t, isHighlight } = props;

  const path = `${process.env.baseUrl}/news/${id}`;
  const showDate = dayjs(date).format("D MMMM YYYY");

  return (
    <div className="card-article-component">
      <div className="image-article-block">
        <AspectRatio
          ratio={16 / 9}
          style={{
            backgroundImage: `url('${process.env.serviceApiUrl}${image}')`,
            backgroundSize: "cover",
            backgroundPosition: "center",
            height: "100%",
          }}
        />
      </div>
      <div className="content-article-block">
        <div className="content-article-wrapper">
          <div className="container-content">
            <div className="date-block">{showDate}</div>
            <div className="title-block">{title}</div>
          </div>
          <div className="container-footer">
            <div className="container-social">
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://social-plugins.line.me/lineit/share?url=" +
                      encodeURIComponent(path),
                    "facebook-share-dialog",
                    "width=600,height=500"
                  );
                  return false;
                }}
              >
                <Icon type="line" color="black" />
              </div>
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://www.facebook.com/sharer/sharer.php?u=" +
                      encodeURIComponent(path),
                    "facebook-share-dialog",
                    "width=626,height=436"
                  );
                  return false;
                }}
              >
                <Icon type="facebook" color="black" />
              </div>
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://twitter.com/intent/tweet?text=" +
                      encodeURIComponent(title) +
                      "&amp;url=" +
                      encodeURIComponent(path),
                    "Twitter-dialog",
                    "width=626,height=436"
                  );
                  return false;
                }}
              >
                <Icon type="twitter" color="black" />
              </div>
            </div>
            <a
              className="button-article-block"
              onClick={() => Router.push(`/news/${id}`)}
            >
              <MyButton type="black">{t("View More")}</MyButton>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withMyTranslation(CardArticle);
