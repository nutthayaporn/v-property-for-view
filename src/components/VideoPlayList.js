import React, { Component } from "react";
import Slider from "react-slick";
import _map from "lodash/map";
import _get from "lodash/get";
import _size from "lodash/size";

import { Image, YouTube } from "../components";

export default class VideoPlayList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null
    };
    this.videoRef = [];
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }
  render() {
    const { items } = this.props;
    return (
      <div className="gallery-component">
        <div className="slider1">
          <Slider
            fade={true}
            asNavFor={this.state.nav2}
            ref={slider => (this.slider1 = slider)}
            beforeChange={oldIndex => {
              if (this.videoRef[oldIndex]) {
                this.videoRef[oldIndex].player.pauseVideo();
              }
            }}
          >
            {_map(items, (item, index) => {
              const videoId = _get(item, "resourceId");
              return (
                <div className="image-item-block" key={index}>
                  <YouTube
                    ref={ref => (this.videoRef[index] = ref)}
                    videoId={videoId}
                  />
                </div>
              );
            })}
          </Slider>
        </div>
        {_size(items) > 1 && (
          <div
            className="slider2 slider2-video"
            style={{
              marginLeft: _size(items) < 4 ? 200 / _size(items) : 0,
              marginRight: _size(items) < 4 ? 200 / _size(items) : 0
            }}
          >
            <Slider
              asNavFor={this.state.nav1}
              ref={slider => (this.slider2 = slider)}
              slidesToShow={_size(items) >= 4 ? 4 : _size(items)}
              focusOnSelect={true}
              responsive={[
                {
                  breakpoint: 680,
                  settings: {
                    slidesToShow: 2
                  }
                }
              ]}
            >
              {_map(items, (item, index) => {
                const image = _get(item, "thumbnail");
                return (
                  <div className="image-item-block" key={index}>
                    <Image src={image} />
                  </div>
                );
              })}
            </Slider>
          </div>
        )}
      </div>
    );
  }
}