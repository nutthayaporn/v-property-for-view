import React, { useState, useEffect } from "react";

import { MyButton } from "../components";

const cookiePolicy = {
  en:
    "We use cookies to ensure that we give you the best experience on our website. By using our website you agree to our use of cookies in accordance with our privacy policy.",
  th:
    "เราใช้คุกกี้เพื่อให้แน่ใจว่าเราจะได้รับประสบการณ์ที่ดีที่สุดในเว็บไซต์ของเรา โดยคุณคลิกยินยอม เพื่อให้เราใช้คุกกี้ตามนโยบายความเป็นส่วนตัวของเรา",
};

const CookiePolicy = (props) => {
  const [isAgree, setIsAgree] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const { t } = props;

  useEffect(() => {
    const isAgreelocalStorage = localStorage.getItem("isAgree");
    if (isAgreelocalStorage === "true") {
      setIsAgree(true);
    }
    setIsChecked(true);
  }, [isAgree]);

  if (!isChecked) return <div />;

  return (
    <div
      className="cookie-policy-block"
      style={{ display: isAgree ? "none" : "block" }}
    >
      <span>{t(cookiePolicy)}</span>
      <a href="/policy">{t("Read more")}</a>
      <MyButton
        onClick={() => {
          localStorage.setItem("isAgree", true);
          setIsAgree(true);
        }}
      >
        {t("I Agree")}
      </MyButton>
    </div>
  );
};

export default CookiePolicy;
