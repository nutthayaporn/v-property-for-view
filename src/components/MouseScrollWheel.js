import React from "react";

import { scrollToElement } from "../utils";

const MouseScrollWheel = props => {
  const { toElement } = props;
  return (
    <div
      className="mouse-scroll-wheel"
      onClick={() => scrollToElement(toElement)}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="48"
        height="89"
        viewBox="0 0 48 89"
      >
        <defs>
          <filter
            id="a"
            width="160%"
            height="131.2%"
            x="-30%"
            y="-15%"
            filterUnits="objectBoundingBox"
          >
            <feOffset dy="2" in="SourceAlpha" result="shadowOffsetOuter1" />
            <feGaussianBlur
              in="shadowOffsetOuter1"
              result="shadowBlurOuter1"
              stdDeviation="2"
            />
            <feColorMatrix
              in="shadowBlurOuter1"
              result="shadowMatrixOuter1"
              values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.5 0"
            />
            <feMerge>
              <feMergeNode in="shadowMatrixOuter1" />
              <feMergeNode in="SourceGraphic" />
            </feMerge>
          </filter>
        </defs>
        <g
          fill="none"
          fillRule="evenodd"
          stroke="#FFF"
          strokeWidth="2"
          transform="translate(4 2)"
        >
          <g>
            <rect width="38" height="58" x="1" y="1" rx="19" />
          </g>
          <g className="mouse-wheel">
            <path strokeLinecap="round" d="M20 12.5v8" />
          </g>
          <g className="arrow-down" strokeLinecap="round">
            <path d="M20 70v8M14.354 73.672l5.656 5.656M26.01 73.672l-5.656 5.656" />
          </g>
        </g>
      </svg>
    </div>
  );
};

export default MouseScrollWheel;

MouseScrollWheel.defaultProps = {
  toElement: ""
};
