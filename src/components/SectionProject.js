import React, { useState, useEffect } from "react";
import { Tabs, Radio } from "antd";
import _filter from "lodash/filter";
import _get from "lodash/get";
import _map from "lodash/map";
import _size from "lodash/size";
import _find from "lodash/find";
import _sortBy from "lodash/sortBy";
import { useRouter } from "next/router";
import styled from "styled-components";

import { Icon, Image } from "../components";
import { withMyTranslation } from "../hoc";

const { TabPane } = Tabs;
const bg = "/static/images/bg-section2.png";

const SectionProject = (props) => {
  const { priceRanges, projectTypes, locationNearBy, projectStatus, t } = props;

  const [filterGroup, setFilterGroup] = useState("location"); // location, price, status
  const [filterLocationValue, setFilterLocationValue] = useState(0);
  const [filterPriceValue, setFilterPriceValue] = useState(0);
  const [filterStatusValue, setFilterStatusValue] = useState(0);

  let { projects } = props;
  projects = _filter(projects, { status: "Show" });

  const router = useRouter();
  let { language } = router.query;

  if (!language) {
    language = "en";
  }

  useEffect(() => {
    const element = document.querySelector(".container-filter-block");
    if (element) {
      element.scrollLeft = 0;
    }
  }, [filterGroup]);

  const handleChangeFilterValue = (id, filterGroup) => {
    if (filterGroup === "location") {
      setFilterLocationValue(id);
      setFilterPriceValue(0);
      setFilterStatusValue(0);
    }
    if (filterGroup === "price") {
      setFilterLocationValue(0);
      setFilterPriceValue(id);
      setFilterStatusValue(0);
    }
    if (filterGroup === "status") {
      setFilterLocationValue(0);
      setFilterPriceValue(0);
      setFilterStatusValue(id);
    }
  };

  const renderFilter = (items) => {
    let filterValue = null;
    if (filterGroup === "location") filterValue = filterLocationValue;
    if (filterGroup === "price") filterValue = filterPriceValue;
    if (filterGroup === "status") filterValue = filterStatusValue;

    return (
      <div className="container-filter-block">
        {_map(items, (item, index) => {
          const name = t(_get(item, "name"));
          const id = _get(item, "id");
          const selected = filterValue === id;
          return (
            <div key={index} className="filter-block">
              {filterGroup === "location" && id !== 0 && (
                <div className="icon-bts">
                  <Icon
                    type="bts"
                    width="40"
                    height="35"
                    color={selected ? "#d4bb9d" : "#ffffff"}
                  />
                </div>
              )}
              <p
                className={selected ? "selected" : ""}
                onClick={() => handleChangeFilterValue(id, filterGroup)}
              >
                {name}
              </p>
            </div>
          );
        })}
      </div>
    );
  };

  const getFillterList = () => {
    let items = [];
    if (filterGroup === "location") {
      items = locationNearBy;
      items = _map(items, (item) => {
        return { id: _get(item, "id"), name: _get(item, "name") };
      });
      items = [{ id: 0, name: "All Location" }, ...items];
    } else if (filterGroup === "price") {
      items = priceRanges;
      items = _map(items, (item) => {
        return { id: _get(item, "id"), name: _get(item, "price"), ...item };
      });
      items = [{ id: 0, name: "All Price" }, ...items];
    } else if (filterGroup === "status") {
      items = projectStatus;
      items = _map(items, (item) => {
        return { id: _get(item, "id"), name: _get(item, "name") };
      });
      items = [{ id: 0, name: "All Status" }, ...items];
    }
    return <React.Fragment>{renderFilter(items)}</React.Fragment>;
  };

  return (
    <Section isHideTab={_size(projectTypes) === 1}>
      <div className="section-project-component">
        <Image className="bg-image" src={bg} />
        <div className="menu-block">
          <Tabs animated={false}>
            {_map(projectTypes, (type) => {
              const typeName = t(_get(type, "name"));
              const typeId = _get(type, "id");

              let filterProject = _filter(projects, ["projectTypeId", typeId]);
              if (filterLocationValue !== 0) {
                filterProject = _filter(filterProject, [
                  "projectLocationId",
                  filterLocationValue,
                ]);
              }
              if (filterPriceValue !== 0) {
                const priceDetail = _find(priceRanges, [
                  "id",
                  filterPriceValue,
                ]);
                const minPrice = _get(priceDetail, "minPrice");
                const maxPrice = _get(priceDetail, "maxPrice");
                filterProject = _filter(filterProject, (project) => {
                  const projectPrice = _get(project, "projectPrice");
                  return projectPrice >= minPrice && projectPrice <= maxPrice;
                });
              }

              if (filterStatusValue !== 0) {
                filterProject = _filter(filterProject, [
                  "projectStatusId",
                  filterStatusValue,
                ]);
                filterProject = _sortBy(filterProject, ["order"]);
              }
              return (
                <TabPane tab={typeName} key={typeName} forceRender={false}>
                  <div className="button-group-block">
                    <Radio.Group
                      value={filterGroup}
                      onChange={(e) => {
                        setFilterGroup(e.target.value);
                        setFilterLocationValue(0);
                        setFilterPriceValue(0);
                        setFilterStatusValue(0);
                      }}
                    >
                      <Radio.Button value="location">
                        {t("Location")}
                      </Radio.Button>
                      <Radio.Button value="price">
                        {t("Price Range")}
                      </Radio.Button>
                      <Radio.Button value="status">{t("Status")}</Radio.Button>
                    </Radio.Group>
                  </div>

                  <div className="filter-list-block">{getFillterList()}</div>
                  <div key={_size(filterProject)}>
                    {props.render(filterProject)}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </div>
      </div>
    </Section>
  );
};

SectionProject.defaultProps = {
  isFullSize: true,
};

export default withMyTranslation(SectionProject);

const Section = styled.div`
  .section-project-component {
    position: relative;
    background-color: #2c2f3c;
    padding-bottom: 30px;
    .bg-image {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      @media (max-width: 820px) {
        height: 260px;
        object-fit: cover;
      }
    }
    .ant-tabs {
      padding-top: ${(props) => (props.isHideTab ? "80px" : "60px")};
      .ant-tabs-bar {
        display: ${(props) => (props.isHideTab ? "none" : "block")};
      }
      .ant-tabs-tab {
        color: white;
        font-family: DBHelvethaicaX-55Regular;
        font-size: 26px;
        letter-spacing: 0.5px;
        margin-left: 40px;
        margin-right: 40px;
        padding-top: 5px;
      }
      .ant-tabs-tab-active {
        color: #d4bb9d;
      }
      .ant-tabs-ink-bar {
        height: 3px;
        background-color: #c2ab92;
      }
    }
    .button-group-block {
      display: flex;
      justify-content: center;
      @media (max-width: 680px) {
        .ant-radio-button-wrapper {
          padding: 5px 20px;
          span {
            font-family: DBHelvethaicaX-Li;
            font-size: 20px;
          }
        }
      }
    }
    .filter-list-block {
      .container-filter-block {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 40px;
        min-height: 53px;
        @media (max-width: 1200px) {
          overflow: scroll;
          padding-left: 30px;

          &::-webkit-scrollbar {
            width: 0px;
            background: transparent;
          }
          .filter-block {
            min-height: 53px;
            &:not(:first-child) {
              border-left: 1px solid #c2ab92;
              padding-left: 15px;
            }
            &:not(:first-child) {
              margin-left: 15px;
            }
            &:last-child {
              padding-right: 20px;
            }
          }
        }
        @media (max-width: 680px) {
          justify-content: flex-start;
          margin-top: 20px;
        }
      }
      .filter-block {
        display: flex;
        align-items: center;
        p {
          color: white;
          font-family: DBHelvethaicaX-55Regular;
          font-size: 23px;
          letter-spacing: 0.5px;
          width: max-content;
          cursor: pointer;
          &:hover,
          &.selected {
            color: #d4bb9d;
          }
        }
        .icon-bts {
          display: flex;
          justify-content: center;
          align-items: center;
          padding-right: 10px;
        }
        &:not(:first-child) {
          border-left: 1px solid #c2ab92;
          padding-left: 30px;
        }
        &:not(:first-child) {
          margin-left: 30px;
        }
      }
    }
  }
`;
