import React from "react";
import { Form, Input, message } from "antd";
import _get from "lodash/get";
import _find from "lodash/find";

import { MyButton, Image, MyWaypoint } from "..";
import { fetchFormSubmit } from "../../services/formSubmit";
import { fetchEmailReceiver } from "../../services/emailReceiver";

const SectionContact = (props) => {
  const { data, t } = props;
  const { getFieldDecorator, validateFieldsAndScroll } = props.form;

  let backgroundImage = _get(data, "backgroundImage");
  if (backgroundImage) {
    backgroundImage = `${process.env.serviceApiUrl}${backgroundImage}`;
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        let emailReceiver = await fetchEmailReceiver();
        emailReceiver = _find(emailReceiver, ["page", "subscribe"]);

        const data = {
          page: "subscribe",
          email: _get(values, "email"),
          to: _get(emailReceiver, "to"),
          cc: _get(emailReceiver, "cc"),
          bcc: _get(emailReceiver, "bcc"),
        };
        const result = await fetchFormSubmit(data);
        if (_get(result, "status") === "success") {
          message.success("Success");
          props.form.resetFields();
        }
      }
    });
  };

  return (
    <MyWaypoint>
      <div
        className="section6-home-page"
        style={{ backgroundImage: `url('${backgroundImage}')` }}
      >
        <div className="container-content">
          <div className="row no-gutters" style={{ width: "100%" }}>
            <div className="col-lg-4">
              <div
                className="block-left"
                style={{ backgroundImage: `url('${backgroundImage}')` }}
              >
                <div className="bg-block-left" />
                <div className="block-content-wrapper">
                  <div className="line" />
                  <h2>{t("Investor")}</h2>
                  <p>{t("Relation")}</p>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div
                className="block-center"
                style={{ backgroundImage: `url('${backgroundImage}')` }}
              >
                <div className="qr-block">
                  <Image
                    className="img-qr-code"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6AQAAAACgl2eQAAABpklEQVR42u2ZTW7EMAiFkXyAHMlX95F8AEsMPHB+2rSLqgs/yZE1cuJvg4AHeER/f5psYAMb+BvQxJ6jFdVxNDl0iAz/JFyA+pEvO9VeRart4zsR4HbhFKtpF9jLCLTS/WVI1fQaI3AaGI7jAyLkipH4RQa9xOTiQKT/uK83fVgbyKf0alFnuV9g4HetXhyIABtHu0jVcjmLA7BXU2DLGjuasN6dxQF0Qe7jVJKhA6BXLU7nq7uMC4ia6AqWUty8rLABiLpWEHWRQQKYDDDrpKJjdAOR+940cgGuVy7CEtXENx57jQqAapmZ6NhB1uh+uQB3kNfEepaV2HMB0aLgNzouheFCBcyQ8zHQ+xPk/ltMrg349IQBMObZG8MERLLHUSTOlxaFAUgFw6aegccGoAKiCEoWRHTvN2cxAHmfAPlqaH1hdRcqIAembK4y8R8ixgCkCGMMwTDYrzsTIiDmQUVlDJPnYgLiwsrHKPSKYXh5tCgsgAvyiDoyayIhkDEmc//T7eKyQF679dNl8hwGOYBMfzfTR8LQ4Rd9WBrY/0ZtYAP/DXwA8DbAvvEXV4EAAAAASUVORK5CYII="
                  />
                </div>
                <p className="p-text">{t("Add Line ID")} :</p>
                <a
                  aria-label="line-id"
                  href={process.env.lineLink}
                  target="_blank"
                  rel="noopener"
                >
                  <p className="p-line-id">@vproperty</p>
                </a>
              </div>
            </div>
            <div className="col-lg-4">
              <div
                className="block-right"
                style={{ backgroundImage: `url('${backgroundImage}')` }}
              >
                <div className="bg-block-right" />
                <div className="block-content-wrapper">
                  <div className="block1">
                    <p>E-Newsletter</p>
                    <p>{t("Subscribe")}</p>
                  </div>
                  <div className="block2">
                    <Form layout="inline" onSubmit={handleSubmit}>
                      <Form.Item>
                        {getFieldDecorator("email", {
                          rules: [
                            {
                              required: true,
                              message: "Please input your email.",
                            },
                          ],
                        })(
                          <Input
                            aria-label="email"
                            className="input-email"
                            placeholder="Your email address"
                          />
                        )}
                      </Form.Item>
                      <div className="button-submit-block">
                        <MyButton htmlType="submit" type="white">
                          {t("Submit")}
                        </MyButton>
                      </div>
                    </Form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </MyWaypoint>
  );
};

export default Form.create()(SectionContact);
