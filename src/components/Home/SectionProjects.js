import React from "react";
import _map from "lodash/map";
import _get from "lodash/get";
import _filter from "lodash/filter";
import _size from "lodash/size";
import _isEmpty from "lodash/isEmpty";
import _find from "lodash/find";
import _sortBy from "lodash/sortBy";

import { Link, Router } from "../../i18n";
import {
  BackgroundImage,
  Swiper,
  MyButton,
  Image,
  StatusBar,
  SectionProject,
  CardProject
} from "..";
import { toCodeColor } from "../../utils";

const SectionProjects = props => {
  const {
    projects,
    priceRanges,
    locationNearBy,
    projectStatus,
    projectTypes,
    t
  } = props;

  return (
    <div className="section2-home-page">
      <SectionProject
        projects={projects}
        priceRanges={priceRanges}
        locationNearBy={locationNearBy}
        projectStatus={projectStatus}
        projectTypes={projectTypes}
        render={filterProject => {
          return (
            <div>
              <div className="container-project-silder">
                <div className="container">
                  <Swiper
                    key={_size(filterProject)}
                    items={filterProject}
                    setting={{
                      slidesPerView: "auto",
                      // speed: 500,
                      // autoplay: {
                      //   delay: 7000
                      // },
                      coverflowEffect: {
                        rotate: 0,
                        stretch: 0,
                        depth: 150,
                        modifier: 1,
                        slideShadows: false
                      },
                      effect: "coverflow",
                      loop: false,
                      centeredSlides: true
                    }}
                    uiRenderer={(project, index) => {
                      const image = _get(project, "wideThumbnail");
                      const logo = _get(project, "logo");
                      const tag = t(_get(project, "tag"));
                      const name = t(_get(project, "name"));
                      const title = t(_get(project, "tagLine"));
                      const description = t(_get(project, "description"));
                      const captions = _get(project, "captions");
                      const slug = _get(project, "slug");
                      const projectStatusId = _get(
                        project,
                        "projectStatus._id"
                      );
                      const currentStatus = _find(projectStatus, [
                        "_id",
                        projectStatusId
                      ]);
                      const fontColorStatus = toCodeColor(
                        _get(currentStatus, "fontColor")
                      );
                      const backgroundColorStatus = toCodeColor(
                        _get(currentStatus, "backgroundColor")
                      );
                      return (
                        <div className="project-block" key={index}>
                          <div className="project-image">
                            <BackgroundImage image={image} />
                          </div>
                          <div className="logo-block">
                            <Image src={logo} />
                          </div>
                          <div className="project-detail">
                            <div className="block-detail-1">
                              <p className="name">{name}</p>
                              <p className="title">{title}</p>
                              <div
                                className="description"
                                dangerouslySetInnerHTML={{
                                  __html: description
                                }}
                              />
                            </div>
                            <div className="block-detail-2">
                              <div className="status">
                                <StatusBar
                                  status={tag}
                                  fontColor={fontColorStatus}
                                  backgroundColor={backgroundColorStatus}
                                />
                              </div>
                              <div className="container-text-icon">
                                {!_isEmpty(captions) &&
                                  _map(captions, (caption, index) => {
                                    const icon = t(_get(caption, "image"));
                                    const name = t(_get(caption, "title"));
                                    return (
                                      <div
                                        className="text-icon-block"
                                        key={index}
                                      >
                                        <div className="row no-gutters align-items-center">
                                          <div className="col-1">
                                            <Image
                                              src={icon}
                                              alt=""
                                              style={{
                                                width: 23
                                              }}
                                            />
                                          </div>
                                          <div className="col">
                                            <p>{name}</p>
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  })}
                                <div className="contain-button-more">
                                  {/* <Link
                                    href={`/projects/[id]`}
                                    as={`/projects/${slug}`}
                                  > */}
                                  <a
                                    onClick={() =>
                                      Router.push(`/projects/${slug}`)
                                    }
                                  >
                                    <div className="button-more">
                                      <MyButton type="white">
                                        {t("View More")}
                                      </MyButton>
                                    </div>
                                  </a>
                                  {/* </Link> */}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    }}
                  />
                </div>
              </div>

              <div className="container-project-silder container-project-silder-mobile">
                <div className="container">
                  <Swiper
                    items={filterProject}
                    uiRenderer={(project, key) => {
                      const image = _get(project, "thumbnail");
                      const tag = t(_get(project, "tag"));
                      const logo = _get(project, "logo");
                      const captions = _get(project, "captions");
                      const slug = _get(project, "slug");
                      const projectStatusId = _get(
                        project,
                        "projectStatus._id"
                      );
                      const currentStatus = _find(projectStatus, [
                        "_id",
                        projectStatusId
                      ]);
                      const fontColorStatus = toCodeColor(
                        _get(currentStatus, "fontColor")
                      );
                      const backgroundColorStatus = toCodeColor(
                        _get(currentStatus, "backgroundColor")
                      );
                      return (
                        <div className="project-block-mobile" key={key}>
                          {/* <Link
                            href={`/projects/[id]`}
                            as={`/projects/${slug}`}
                          > */}
                          <a onClick={() => Router.push(`/projects/${slug}`)}>
                            <CardProject
                              image={image}
                              logo={logo}
                              status={tag}
                              captions={captions}
                              fontColorStatus={fontColorStatus}
                              backgroundColorStatus={backgroundColorStatus}
                            />
                          </a>
                          {/* </Link> */}
                        </div>
                      );
                    }}
                  />
                </div>
              </div>
            </div>
          );
        }}
      />
    </div>
  );
};

export default SectionProjects;
