import React from "react";
import anime from "animejs";
import _get from "lodash/get";
import _find from "lodash/find";
import _filter from "lodash/filter";
import _size from "lodash/size";
import _sortBy from "lodash/sortBy";
import styled from "styled-components";

import { Link } from "../../i18n";
import { MyButton, MyWaypoint, SwiperNews, BackgroundImage } from "..";

const SectionNews = (props) => {
  const { data, news, t } = props;
  const backgroundImage = _get(data, "backgroundImage");
  const highlightNews = _find(news, "isHighlight");
  let otherNews = _filter(news, { isHighlight: false });
  otherNews = _sortBy(otherNews, (item) => {
    return _get(item, "publishedDate");
  });
  let orderedNews = [...otherNews];
  if (highlightNews) {
    orderedNews = [highlightNews, ...otherNews];
  }
  const lengthItem = _size(news);
  return (
    <MyWaypoint>
      <Section lengthItem={lengthItem}>
        <div className="section5-home-page">
          {backgroundImage && (
            <div className="container-image-background">
              <BackgroundImage image={backgroundImage} />
            </div>
          )}

          <div className="container">
            <SwiperNews items={orderedNews} />
          </div>
          <MyWaypoint
            anime={[
              () => {
                anime({
                  targets: ".section5-home-page .button-block p",
                  opacity: [0, 1],
                  translateX: [50, 0],
                  duration: 1700,
                  easing: "easeInOutCubic",
                });
              },
            ]}
          >
            <Link href="/news#all-news" as="/news#all-news">
              <a className="button-block">
                <p>{t("View More")}</p>
                <MyButton type="plus" />
              </a>
            </Link>
          </MyWaypoint>
        </div>
      </Section>
    </MyWaypoint>
  );
};

export default SectionNews;

const Section = styled.div`
  position: relative;
  .section5-home-page {
    padding-top: 100px;
    padding-bottom: 50px;
    position: relative;
    overflow: hidden;

    .container-image-background {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      img {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
    }

    .button-block {
      position: absolute;
      bottom: 0;
      right: 0;
      display: flex;
      align-items: center;
      overflow: hidden;
      p {
        font-family: "DBHelvethaicaX-55Regular";
        font-size: 22px;
        color: #2c2f3c;
        margin-right: 10px;
        overflow: hidden;
      }
    }

    .swiper-pagination {
      bottom: 0;
      display: ${(props) => (props.lengthItem < 5 ? "none" : "block")};
      @media (max-width: 540px) {
        display: block;
      }
    }
    .swiper-container {
      padding-bottom: 50px;
    }
    @media (max-width: 540px) {
      padding-top: 30px;
      padding-bottom: 50px;
      .swiper-container {
        padding-bottom: 20px;
      }
    }
  }
`;
