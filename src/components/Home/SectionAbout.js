import React, { useState } from "react";
import _find from "lodash/find";
import _get from "lodash/get";
import _map from "lodash/map";
import _replace from "lodash/replace";
import posed, { PoseGroup } from "react-pose";
import anime from "animejs";

import { Link } from "../../i18n";
import { BackgroundImage, MyButton, Icon, MyWaypoint, Swiper } from "..";

const testImage = "/static/images/pages/home/img-about.jpg";
const imageBanner1 = "/static/images/pages/home/section1-image.jpg";

const projects = [
  {
    id: 1,
    name: "V Property Brand",
    image: testImage,
    description:
      "วี พร๊อพเพอร์ตี้ ดีเวลลอปเมนท์ ก่อตั้งเมื่อปี 2552 โดย แซปไฟร์ โฮลดิ้ง กรุ๊ป กลุ่มบริษัทส่งออกอัญมณีรายใหญ่ในเมืองไทย ด้วยประสบการณ์ กว่า 40 ปี กับธุรกิจส่งออกอัญมณี ทำให้กลุ่มบริษัทฯ เป็นที่รู้จักในตลาดอัญมณีทั่วโลก"
  },
  {
    id: 2,
    name: "About V Property",
    image:
      "https://cdn-images.prod.thinkofliving.com/wp-content/uploads/1/2018/01/5a21083db379f47e6a7c484d_5a21224015f0201f15b327c4.jpg",
    description:
      "วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ ก่อตั้งเมื่อปี 2552 โดย แซปไฟร์ โฮลดิ้ง กรุ๊ป กลุ่มบริษัทส่งออกอัญมณีรายใหญ่ในเมืองไทย ด้วยประสบการณ์ กว่า 40 ปี กับธุรกิจส่งออกอัญมณี ทำให้กลุ่มบริษัทฯ เป็นที่รู้จักในตลาดอัญมณีทั่วโลก ปัจจุบันทางกลุ่มบริษัทฯ ได้ขยายธุรกิจด้านอสังหาริมทรัพย์"
  },
  {
    id: 3,
    name: "V Family",
    image: imageBanner1,
    description:
      "“บริษัทฯ” ตระหนักถึงความสำคัญของการดำเนินกิจการอย่างโปร่งใส ยึดหลักการกำกับดูแลกิจการที่ดีเพื่อให้มีการดำเนินกิจการอย่างมีประสิทธิภาพ เป็นธรรม มีความรับผิดชอบต่อสังคมและผู้มีส่วนได้เสีย และป้องกันการทุจริตคอรัปชั่นที่อาจเกิดขึ้นในองค์กรทุกๆ กรณี"
  }
];

const Shade = posed.div({
  enter: { opacity: 1 },
  exit: { opacity: 0 }
});

const SectionAbout = props => {
  const { data, t } = props;
  const [selectedId, setSelectedId] = useState(1);
  const selectedItem = _find(projects, ["id", selectedId]);
  const selectedItemImage = _get(selectedItem, "image");
  const selectedItemName = t(_get(selectedItem, "name"));
  const selectedItemDescription = t(_get(selectedItem, "description"));

  const isShow = selectedId !== 2 && selectedId !== 3; // ปิด about ไว้ก่อน

  const backgroundImage = _get(data, "backgroundImage");

  let selectedQueryName = _get(selectedItem, "name");
  selectedQueryName = _replace(selectedQueryName, /\s/g, "");

  const path = process.env.baseUrl;
  const title =
    "V PROPERTY DEVELOPMENT ผู้นำด้านการพัฒนาคอนโดมิเนียมติดรถไฟฟ้า สุขุมวิท กรุงเทพมหานคร | บริษัท วี พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด โทร. 1298";

  return (
    <MyWaypoint
      anime={[
        () => {
          anime({
            targets: ".section4-home-page .content-block h2",
            opacity: [0, 1],
            translateX: [50, 0],
            duration: 700,
            easing: "easeInOutCubic"
          });
        },
        () => {
          anime({
            targets: ".section4-home-page .description-block p",
            opacity: [0, 1],
            translateX: [20, 0],
            duration: 800,
            easing: "easeInOutCubic",
            delay: 200
          });
        },
        () => {
          anime({
            targets: ".section4-home-page .desktop .container-social",
            opacity: [0, 1],
            duration: 600,
            easing: "easeInOutCubic",
            delay: 500
          });
        },
        () => {
          anime({
            targets: ".section4-home-page .desktop .button-block",
            opacity: [0, 1],
            duration: 600,
            easing: "easeInOutCubic",
            delay: 700
          });
        }
      ]}
    >
      <div className="section4-home-page">
        <div className="d-none d-md-block desktop">
          <div className="image-block">
            <PoseGroup>
              <Shade key={selectedId}>
                <BackgroundImage image={selectedItemImage} />
              </Shade>
            </PoseGroup>
          </div>
          <div className="content-block">
            <div className="content-block-background">
              {/* <BackgroundImage image={backgroundImage} /> */}
            </div>
            <PoseGroup>
              <Shade key={selectedId}>
                <h2>{selectedItemName}</h2>
              </Shade>
            </PoseGroup>
            <div className="description-block">
              <PoseGroup>
                <Shade key={selectedId}>
                  <p>{selectedItemDescription}</p>
                </Shade>
              </PoseGroup>
            </div>
            <div className="container-social">
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://social-plugins.line.me/lineit/share?url=" +
                      encodeURIComponent(path),
                    "facebook-share-dialog",
                    "width=600,height=500"
                  );
                  return false;
                }}
              >
                <Icon type="line" color="white" />
              </div>
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://www.facebook.com/sharer/sharer.php?u=" +
                      encodeURIComponent(path),
                    "facebook-share-dialog",
                    "width=626,height=436"
                  );
                  return false;
                }}
              >
                <Icon type="facebook" color="white" />
              </div>
              <div
                className="social-block"
                onClick={() => {
                  window.open(
                    "https://twitter.com/intent/tweet?text=" +
                      encodeURIComponent(title) +
                      "&amp;url=" +
                      encodeURIComponent(path),
                    "Twitter-dialog",
                    "width=626,height=436"
                  );
                  return false;
                }}
              >
                <Icon type="twitter" color="white" />
              </div>
              {/* <div className="social-block">
                <Icon type="facebook" color="white" />
              </div>
              <div className="social-block">
                <Icon type="twitter" color="white" />
              </div> */}
            </div>
            {isShow && (
              <Link
                href={{
                  pathname: "/about",
                  query: { name: selectedQueryName }
                }}
              >
                <a className="button-block">
                  <MyButton type="white">{t("View More")}</MyButton>
                </a>
              </Link>
            )}
          </div>
          <div className="container-projects">
            {_map(projects, project => {
              const id = _get(project, "id");
              const image = _get(project, "image");
              const name = t(_get(project, "name"));
              const isSelected = selectedId === id;
              return (
                <div
                  key={id}
                  className={`projects-warpper ${
                    isSelected ? "projects-warpper-selected" : ""
                  }`}
                >
                  <div className="image-project-block-background">
                    <div
                      className="image-project-block"
                      onClick={() => setSelectedId(id)}
                    >
                      <BackgroundImage className="bg-image" image={image} />
                    </div>
                  </div>
                  <div className="name-block">{name}</div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="d-md-none mobile">
          <Swiper
            items={projects}
            setting={{
              speed: 800
            }}
            uiRenderer={(item, index) => {
              const image = _get(item, "image");
              const name = t(_get(item, "name"));
              const description = t(_get(item, "description"));
              return (
                <div className="container-projects-mobile" key={index}>
                  <div className="image-block">
                    <BackgroundImage image={image} />
                  </div>
                  <div className="container-projects">
                    <div className="container">
                      <div className="title">{name}</div>
                      <div className="description">{description}</div>
                      <div className="content-footer">
                        <div className="container-social">
                          <div className="social-block">
                            <Icon type="facebook" color="white" />
                          </div>
                          <div className="social-block">
                            <Icon type="instagram" color="white" />
                          </div>
                          <div className="social-block">
                            <Icon type="twitter" color="white" />
                          </div>
                        </div>
                        {isShow && (
                          <Link
                            href={{
                              pathname: "/about",
                              query: { name: selectedQueryName }
                            }}
                          >
                            <a className="button-block">
                              <MyButton type="white">{t("View More")}</MyButton>
                            </a>
                          </Link>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              );
            }}
          />
        </div>
      </div>
    </MyWaypoint>
  );
};

export default SectionAbout;
