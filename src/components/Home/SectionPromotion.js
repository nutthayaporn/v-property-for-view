import React from "react";
import anime from "animejs";
import _get from "lodash/get";

import { Link } from "../../i18n";
import { BackgroundImage, MyButton, MyWaypoint, SwiperPromotion } from "..";

const SectionPromotion = props => {
  const { data, promotions, t } = props;
  const backgroundImage = _get(data, "backgroundImage");
  return (
    <MyWaypoint>
      <div className="section3-home-page">
        {backgroundImage && (
          <div className="bg-block">
            <BackgroundImage image={backgroundImage} />
          </div>
        )}

        <div className="container">
          <SwiperPromotion advertises={promotions} />
        </div>
        <MyWaypoint
          anime={[
            () => {
              anime({
                targets: ".section3-home-page .button-block p",
                opacity: [0, 1],
                translateX: [50, 0],
                duration: 1700,
                easing: "easeInOutCubic"
              });
            }
          ]}
        >
          <Link href="/news" as="/news">
            <a className="button-block">
              <p>{t("View More")}</p>
              <MyButton type="plus" />
            </a>
          </Link>
        </MyWaypoint>
      </div>
    </MyWaypoint>
  );
};

export default SectionPromotion;
