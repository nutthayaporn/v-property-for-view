import React, { useState, useEffect } from "react";
import _get from "lodash/get";
import _size from "lodash/size";
import _filter from "lodash/filter";

import {
  BackgroundImage,
  Swiper,
  MouseScrollWheel,
  MyWaypoint,
  Image
} from "..";

import { toCodeColor } from "../../utils";
import { Router } from "../../i18n";

const SectionBanner = props => {
  const { data, t } = props;
  const [height, setHeight] = useState(0);

  useEffect(() => {
    setHeight(document.body.height || window.innerHeight);
  }, [height]);

  let banners = _get(data, "banners");
  banners = _filter(banners, ["status", "Show"]);
  return (
    <div className="section1-home-page" style={{ height: height }}>
      <div className="section1-home-page-desktop">
        <Swiper
          items={banners}
          setting={{
            speed: 800,
            // autoplay: {
            //   delay: 7000
            // },
            navigation: {
              nextEl: ".swiper-button-next",
              prevEl: ".swiper-button-prev"
            }
          }}
          key={_size(banners)}
          uiRenderer={(item, key) => {
            const pcResource = _get(item, "pcResource");
            const pcOverlayImage = _get(item, "pcOverlayImage");
            const textColor = toCodeColor(_get(item, "overlayTextColor"));
            const backgroundColor = toCodeColor(
              _get(item, "overlayBackgroundColor")
            );
            const buttonTextColor = toCodeColor(_get(item, "buttonTextColor"));
            const buttonBackgroundColor = toCodeColor(
              _get(item, "buttonBackgroundColor")
            );

            const buttonStatus = _get(item, "buttonStatus");

            const overlayStatus = _get(item, "overlayStatus");
            const overlayPosition = _get(item, "overlayPosition");

            let styleOverlayBlock = { left: 100, right: "auto" };
            if (overlayPosition === "Mid") {
              styleOverlayBlock = {
                left: 0,
                right: 0,
                margin: "auto"
              };
            } else if (overlayPosition === "Right") {
              styleOverlayBlock = { left: "auto", right: 100 };
            }

            styleOverlayBlock = {
              ...styleOverlayBlock,
              backgroundColor: backgroundColor
            };

            const resourceType = _get(item, "resourceType");
            const logo = _get(item, "logo");
            const description1 = t(_get(item, "description1"));
            const description2 = t(_get(item, "description2"));
            const description3 = t(_get(item, "description3"));
            const description4 = t(_get(item, "description4"));

            const linkIn = _get(item, "linkIn");
            const linkOut = _get(item, "linkOut");

            let herf = linkOut;
            if (linkIn) {
              herf = linkIn;
            }
            
            return (
              <div
                className="image-banner-block"
                key={key}
                onClick={() => {
                  console.log('herf', herf)
                  if (linkIn) {
                    Router.push(herf);
                  } else {
                    window.open(herf, '_blank');
                  }
                }}
              >
                {resourceType === "Image" ? (
                  <BackgroundImage image={pcResource} />
                ) : (
                  <div />
                )}
                {overlayStatus === "Show" && (
                  <div
                    className="banner-detail-block"
                    style={styleOverlayBlock}
                  >
                    <div className="logo-block">
                      <Image src={logo} alt="V Property Logo" />
                    </div>
                    <div className="description-block">
                      <div className="description" style={{ color: textColor }}>
                        {description1}
                      </div>
                      <div
                        className="description description-highlight"
                        style={{ color: textColor }}
                      >
                        {description2}
                      </div>
                      <div className="description" style={{ color: textColor }}>
                        {description3}
                      </div>
                    </div>
                    <div className="price" style={{ color: textColor }}>
                      {description4}
                    </div>
                    {buttonStatus !== 'Hide' && (
                      <div className="button-block">
                        <div
                          className="button-primary"
                          style={{
                            backgroundColor: buttonBackgroundColor
                          }}
                        >
                          <div
                            className="background"
                            style={{
                              backgroundColor: buttonBackgroundColor
                            }}
                          />
                          <p style={{ color: buttonTextColor }}>Register</p>
                        </div>
                      </div>
                    )}
                  </div>
                )}
                {pcOverlayImage && (
                  <div className="banner-detail-block">
                    <Image
                      className="pc-overlay-image"
                      src={pcOverlayImage}
                    />
                  </div>
                )}
              </div>
            );
          }}
        />
        <div className="mouse-scroll-block">
          <MouseScrollWheel toElement=".section2-home-page" />
        </div>
      </div>
      <div className="section1-home-page-mobile">
        <Swiper
          items={banners}
          setting={{
            speed: 800
            // autoplay: {
            //   delay: 7000
            // }
          }}
          key={_size(banners)}
          uiRenderer={(item, key) => {
            const mobileResource = _get(item, "mobileResource");
            const mobileOverlayImage = _get(item, "mobileOverlayImage");
            const textColor = toCodeColor(_get(item, "overlayTextColor"));
            const backgroundColor = toCodeColor(
              _get(item, "overlayBackgroundColor")
            );
            const buttonTextColor = toCodeColor(_get(item, "buttonTextColor"));
            const buttonBackgroundColor = toCodeColor(
              _get(item, "buttonBackgroundColor")
            );

            const buttonStatus = _get(item, "buttonStatus");
            const overlayStatus = _get(item, "overlayStatus");
            const resourceType = _get(item, "resourceType");
            const logo = _get(item, "logo");
            const description1 = t(_get(item, "description1"));
            const description2 = t(_get(item, "description2"));
            const description3 = t(_get(item, "description3"));
            const description4 = t(_get(item, "description4"));

            const linkIn = _get(item, "linkIn");
            const linkOut = _get(item, "linkOut");

            let herf = linkIn;
            if (linkOut) {
              herf = linkOut;
            }
            return (
              <div
                className="image-banner-block"
                key={key}
                onClick={() => {
                  if (linkIn) {
                    Router.push(herf);
                  } else {
                    window.open(herf, '_blank');
                  }
                }}
              >
                {overlayStatus === "Show" ? (
                  <div className="container-mobile-detail">
                    <div className="mobile-image-block">
                      {resourceType === "Image" ? (
                        <BackgroundImage image={mobileResource} />
                      ) : (
                        <div />
                      )}
                    </div>
                    <div
                      className="mobile-detail"
                      style={{ backgroundColor: backgroundColor }}
                    >
                      {/* <div className="container"> */}
                      <div className="project-banner-detail-block">
                        <div className="logo-block">
                          <Image src={logo} alt="V Property Logo" />
                        </div>
                        <div className="description-block">
                          <div
                            className="description"
                            style={{ color: textColor }}
                          >
                            {description1}
                          </div>
                          <div
                            className="description"
                            style={{ color: textColor }}
                          >
                            {description2}
                          </div>
                          <div
                            className="description"
                            style={{ color: textColor }}
                          >
                            {description3}
                          </div>
                          <div
                            className="description"
                            style={{ color: textColor }}
                          >
                            {description4}
                          </div>
                          {buttonStatus !== 'Hide' && (
                            <div className="button-block">
                              {/* <a
                                {...herfOption}
                                onClick={() => {
                                  if (linkIn) {
                                    Router.push(linkIn);
                                  }
                                }}
                              > */}
                              <div
                                className="button-primary"
                                onClick={() => {}}
                                style={{
                                  backgroundColor: buttonBackgroundColor
                                }}
                              >
                                <div
                                  className="background"
                                  style={{
                                    backgroundColor: buttonBackgroundColor
                                  }}
                                />
                                <p style={{ color: buttonTextColor }}>
                                  Register
                                </p>
                              </div>
                              {/* </a> */}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    {/* </div> */}
                  </div>
                ) : (
                  <div className="image-mobile-banner">
                    <BackgroundImage image={mobileOverlayImage} />
                  </div>
                )}
              </div>
            );
          }}
        />
      </div>
    </div>
  );
};

export default SectionBanner;
