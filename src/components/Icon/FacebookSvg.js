import React from "react";

const FacebookSvg = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" {...props}>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h24v24H0z" />
      <path
        fill={props.color || "#ffffff"}
        d="M10.61 19.945h2.518v-6.792c0-.322.262-.583.583-.583h2.346l.001-2.422h-2.347a.584.584 0 0 1-.583-.583V7.991c0-.413.042-.882.354-1.234.376-.426.968-.475 1.381-.475h1.298V4.058l-2.015-.004c-2.18 0-3.536 1.396-3.536 3.643v1.867a.584.584 0 0 1-.583.583H8.055v2.422h1.972c.322 0 .583.261.583.583v6.792zM13.31 21h-2.882a.874.874 0 0 1-.873-.873v-6.502H7.873A.874.874 0 0 1 7 12.752V9.966c0-.481.392-.873.873-.873h1.682V7.698c0-1.384.434-2.56 1.256-3.404C11.636 3.448 12.79 3 14.146 3l2.199.004c.48 0 .871.392.871.873v2.586a.874.874 0 0 1-.872.874h-1.48c-.452 0-.567.09-.591.118-.041.046-.09.176-.09.536v1.102h2.05a.876.876 0 0 1 .881.873l-.001 2.786a.874.874 0 0 1-.874.873h-2.055v6.502a.874.874 0 0 1-.873.873z"
      />
    </g>
  </svg>
);

export default props => {
  return <FacebookSvg {...props} />;
};
