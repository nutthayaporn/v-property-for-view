import React from "react";
import { Icon } from "antd";
import LineSvg from "./LineSvg";
import FacebookSvg from "./FacebookSvg";
import InstagramSvg from "./InstagramSvg";
import TwitterSvg from "./TwitterSvg";
import BtsSvg from "./BtsSvg";
import LogoVertier from "./LogoVertier";
import LogoIkon from "./LogoIkon";
import LogoVtara from "./LogoVtara";
import LogoH from "./LogoH";
import PhoneSvg from "./PhoneSvg";
import LogoMapsSvg from "./LogoMapsSvg";
import ArrowPrev from "./ArrowPrev";
import ArrowNext from "./ArrowNext";
import ArrowDown from "./ArrowDown";

const MyIcon = props => {
  const { type } = props;
  if (type === "line") return <LineSvg {...props} />;
  if (type === "facebook") return <FacebookSvg {...props} />;
  if (type === "instagram") return <InstagramSvg {...props} />;
  if (type === "twitter") return <TwitterSvg {...props} />;
  if (type === "bts") return <BtsSvg {...props} />;
  if (type === "vertier") return <LogoVertier {...props} />;
  if (type === "ikon") return <LogoIkon {...props} />;
  if (type === "vtara") return <LogoVtara {...props} />;
  if (type === "h") return <LogoH {...props} />;
  if (type === "phone") return <PhoneSvg {...props} />;
  if (type === "logoMaps") return <LogoMapsSvg {...props} />;
  if (type === "arrowPrev") return <ArrowPrev {...props} />;
  if (type === "arrowNext") return <ArrowNext {...props} />;
  if (type === "arrowDown") return <ArrowDown {...props} />;
  return <Icon {...props} />;
};

export default MyIcon;
