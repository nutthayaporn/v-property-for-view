import React, { useState } from "react";
import _map from "lodash/map";
import _get from "lodash/get";
import _find from "lodash/find";
import _filter from "lodash/filter";
import _size from "lodash/size";

import { useTheme } from "../../hooks/useTheme";
import { BackgroundImage, MyWaypoint } from "../../components";
import { toCodeColor } from "../../utils";

const SectionVirtualTour = props => {
  const { data, t } = props;
  const [activeTypeId, setActiveTypeId] = useState(0);

  const backgroundType = _get(data, "backgroundType");
  const backgroundColor = toCodeColor(_get(data, "backgroundColor"));
  const backgroundImage = _get(data, "backgroundImage");
  let panoramas = _get(data, "panoramas");
  panoramas = _filter(panoramas, ["status", "Show"]);
  const currentType = _get(panoramas, activeTypeId);
  const iframe = _get(currentType, "iframe");

  const colors = useTheme();

  return (
    <MyWaypoint>
      <div
        className="project-section-virtuali-tour"
        style={{
          backgroundColor:
            backgroundType === "Color" ? backgroundColor : "#F4EFE8"
        }}
      >
        {backgroundType === "Image" && (
          <div className="background-image-block">
            <BackgroundImage image={backgroundImage} />
          </div>
        )}

        {_size(panoramas) > 1 && (
          <div className="button-group">
            {_map(panoramas, (item, index) => {
              const title = t(_get(item, "title"));
              return (
                <div
                  className="button"
                  key={index}
                  style={{
                    borderColor: colors.mainColor,
                    color:
                      activeTypeId === index
                        ? colors.buttonTextColor
                        : colors.mainColor,
                    backgroundColor:
                      activeTypeId === index ? colors.mainColor : "transparent"
                  }}
                  onClick={() => setActiveTypeId(index)}
                >
                  {title}
                </div>
              );
            })}
          </div>
        )}

        <div className="container-content">
          <iframe src={iframe} allowFullScreen=""></iframe>
        </div>
      </div>
    </MyWaypoint>
  );
};

export default SectionVirtualTour;
