import React, { useState } from "react";
import _get from "lodash/get";
import _size from "lodash/size";
import _filter from "lodash/filter";
import _sortBy from "lodash/sortBy";
import styled from "styled-components";
import anime from "animejs";

import { useTheme } from "../../hooks/useTheme";
import {
  BackgroundImage,
  Swiper,
  GalleryLightBoxZoom,
  MyWaypoint
} from "../../components";
import { toCodeColor } from "../../utils";

const SectionRoomType = props => {
  const { data, t } = props;
  const [activeIndex, setActiveIndex] = useState(null);

  const title = t(_get(data, "title"));
  let roomtypes = _get(data, "room-types");
  roomtypes = _filter(roomtypes, ["status", "Show"]);
  roomtypes = _sortBy(roomtypes, ["order"]);
  const backgroundType = _get(data, "backgroundType");
  const backgroundColor = toCodeColor(_get(data, "backgroundColor"));
  const backgroundImage = _get(data, "backgroundImage");

  const roomtypesLength = _size(roomtypes);

  const colors = useTheme();

  return (
    <MyWaypoint
      anime={[
        () => {
          anime({
            targets: ".project-section-room-type h2",
            opacity: [0, 1],
            translateY: [10, 0],
            duration: 700,
            easing: "easeInOutCubic",
            delay: 50
          });
        }
      ]}
    >
      <Section
        mainColor={colors.mainColor}
        backgroundColor={
          backgroundType === "Color" ? backgroundColor : "#ffffff"
        }
      >
        {backgroundType === "Image" && (
          <div className="background-image-block">
            <BackgroundImage image={backgroundImage} />
          </div>
        )}

        <div className="project-section-room-type">
          <h2
            style={{
              color: colors.mainColor,
              opacity: 0,
              transform: "translateY(10px)"
            }}
          >
            {title}
          </h2>
          <div className="desktop">
            <Swiper
              items={roomtypes}
              key={roomtypesLength}
              setting={{
                slidesPerView: 4,
                spaceBetween: 30,
                loop: false
              }}
              uiRenderer={(item, index) => {
                const title = t(_get(item, "title"));
                const roomType = _get(item, "roomType");
                const image = _get(item, "image");
                return (
                  <div
                    className="room-type-block"
                    key={index}
                    style={{ borderColor: colors.mainColor }}
                    onClick={() => {
                      setActiveIndex(index);
                    }}
                  >
                    <div className="image-room-type">
                      <BackgroundImage image={image} size="contain" />
                    </div>
                    {title && (
                      <div
                        className="title"
                        style={{ color: colors.mainColor }}
                      >
                        Type {title}
                      </div>
                    )}

                    <div className="type">{roomType}</div>
                  </div>
                );
              }}
            />
          </div>
          {activeIndex !== null && (
            <GalleryLightBoxZoom
              imageCaption={item => {
                const title = t(_get(item, "title"));
                const roomType = _get(item, "roomType");
                return (
                  <div className="title-block">
                    <p style={{ color: colors.mainColor }}>
                      type {title}
                      <span>{roomType}</span>
                    </p>
                  </div>
                );
              }}
              active={activeIndex}
              images={roomtypes}
              onCloseRequest={() => {
                setActiveIndex(null);
              }}
            />
          )}
          <div className="mobile">
            <Swiper
              items={roomtypes}
              key={roomtypesLength}
              setting={{
                loop: false,
                navigation: {
                  nextEl: ".button-next",
                  prevEl: ".button-prev"
                }
              }}
              uiRenderer={(item, index) => {
                const title = t(_get(item, "title"));
                const roomType = _get(item, "roomType");
                const image = _get(item, "image");
                return (
                  <div
                    className="room-type-block"
                    key={index}
                    style={{ borderColor: colors.mainColor }}
                    onClick={() => {
                      setActiveIndex(index);
                    }}
                  >
                    <div className="image-room-type">
                      <BackgroundImage image={image} size="contain" />
                    </div>
                    <div className="title" style={{ color: colors.mainColor }}>
                      Type {title}
                    </div>
                    <div className="type">{roomType}</div>
                  </div>
                );
              }}
            />
          </div>
        </div>
      </Section>
    </MyWaypoint>
  );
};

export default SectionRoomType;

const Section = styled.div`
  position: relative;
  background-color: ${props => props.backgroundColor};
  .background-image-block {
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  .desktop {
    @media (max-width: 540px) {
      display: none;
    }
  }
  .mobile {
    display: none;
    @media (max-width: 540px) {
      display: block;
    }
  }
  .project-section-room-type {
    position: relative;
    padding: 80px 0;
    @media (max-width: 540px) {
      padding: 60px 0;
    }
    h2 {
      position: relative;
      z-index: 1;
      font-family: TrajanPro-Bold;
      font-size: 54px;
      color: #f05a28;
      margin-bottom: 120px;
      text-align: center;
      text-transform: uppercase;
      @media (max-width: 540px) {
        font-size: 30px;
        margin-bottom: 45px;
      }
    }
    .room-type-block {
      border: solid 2px #f05a28;
      padding: 50px 34px;
      background-color: #ffffff;
      cursor: zoom-in;
      @media (max-width: 540px) {
        padding: 45px 20px;
        margin: 0 47px;
      }

      .image-room-type {
        width: 100%;
        height: 230px;
        padding-bottom: 40px;
      }
      .title {
        font-family: TrajanPro-Regular;
        font-size: 40px;
        text-align: center;
        color: #ed5828;
        text-transform: uppercase;
      }
      .type {
        font-family: DBHelvethaicaX-Med;
        font-size: 40px;
        text-align: center;
        color: #c8c8c8;
        padding-top: 5px;
      }
    }
    .swiper-container {
      padding-bottom: 100px;
      @media (max-width: 540px) {
        padding-bottom: 44px;
      }
    }
    .swiper-wrapper {
      padding-left: 100px;
      @media (max-width: 540px) {
        padding-left: 0;
      }
    }
    .swiper-pagination {
      bottom: 0;
      .swiper-pagination-bullet {
        background: #2c2f3c;
      }
      .swiper-pagination-bullet-active {
        background: #ed5828;
        background: ${props => props.mainColor};
      }
    }
    .button-next {
      width: 0;
      height: 0;
      border-top: 15px solid transparent;
      border-left: 23px solid #f05a28;
      border-left-color: ${props => props.mainColor};
      border-bottom: 15px solid transparent;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      right: 15px;
      z-index: 10;
      cursor: pointer;
      outline: none;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .button-prev {
      width: 0;
      height: 0;
      border-top: 15px solid transparent;
      border-right: 23px solid #f05a28;
      border-right-color: ${props => props.mainColor};
      border-bottom: 15px solid transparent;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      left: 15px;
      z-index: 10;
      cursor: pointer;
      outline: none;
      display: flex;
      align-items: center;
      justify-content: center;
    }
  }
`;
