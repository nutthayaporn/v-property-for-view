import React from "react";
import _get from "lodash/get";
import _map from "lodash/map";
import _find from "lodash/find";
import anime from "animejs";

import { Swiper, CardProject, MyWaypoint } from "..";
import { toCodeColor } from "../../utils";

const SectionRecommended = props => {
  const { data, projectStatus, t } = props;

  const topic = t(_get(data, "title"));
  const recommendedProjects = _get(data, "recommendedProject");

  const backgroundType = _get(data, "backgroundType");
  const backgroundColor = toCodeColor(_get(data, "backgroundColor"));
  const backgroundImage = _get(data, "backgroundImage");

  return (
    <MyWaypoint
      anime={[
        () => {
          anime({
            targets: ".project-section-recommended .project-block-wrapper",
            opacity: [0, 1],
            translateX: [50, 0],
            duration: 1000,
            easing: "easeInOutCubic",
            delay: function(el, i, l) {
              return i * 150;
            }
          });
        }
      ]}
    >
      <div
        className="project-section-recommended"
        style={{
          backgroundColor:
            backgroundType === "Color" ? backgroundColor : "#2c2f3c"
        }}
      >
        {backgroundImage === "Image" && (
          <div className="background-image-block">
            <BackgroundImage image={backgroundImage} />
          </div>
        )}
        <div className="container">
          <h2>{topic}</h2>
          <div className="container-content">
            <div className="desktop">
              <Swiper
                items={recommendedProjects}
                setting={{
                  slidesPerView: 2,
                  spaceBetween: 20,
                  loop: false,
                  breakpoints: {
                    820: {
                      slidesPerView: 3,
                    },
                    1200: {
                      slidesPerView: 4,
                    },
                  }
                }}
                uiRenderer={(project, index) => {
                  const logo = _get(project, "logo");
                  const image = _get(project, "thumbnail");
                  const captions = _get(project, "highlightInfos");
                  const slug = _get(project, "slug");
                  const linkOut = _get(project, "linkOut");
                  const projectStatusId = _get(project, "projectStatus._id");
                  const projectStatusTitle = t(
                    _get(project, "projectStatus.title")
                  );
                  const currentStatus = _find(projectStatus, [
                    "_id",
                    projectStatusId
                  ]);
                  const fontColorStatus = toCodeColor(
                    _get(currentStatus, "fontColor")
                  );
                  const backgroundColorStatus = toCodeColor(
                    _get(currentStatus, "backgroundColor")
                  );
                  let link = `/projects/${slug}`;
                  if (linkOut) {
                    link = linkOut;
                  }
                  return (
                    <div
                      className="project-block-wrapper"
                      key={index}
                      style={{
                        opacity: 0,
                        transform: "translateX(50px)"
                      }}
                    >
                      <a href={link} target="_blank">
                        <CardProject
                          image={image}
                          logo={logo}
                          status={projectStatusTitle}
                          captions={captions}
                          fontColorStatus={fontColorStatus}
                          backgroundColorStatus={backgroundColorStatus}
                        />
                      </a>
                    </div>
                  );
                }}
              />
            </div>
            <div className="mobile">
              {_map(recommendedProjects, (project, index) => {
                const logo = _get(project, "logo");
                const image = _get(project, "thumbnail");
                const captions = _get(project, "highlightInfos");
                const slug = _get(project, "slug");
                const linkOut = _get(project, "linkOut");
                const projectStatusId = _get(project, "projectStatus._id");
                const projectStatusTitle = t(
                  _get(project, "projectStatus.title")
                );
<<<<<<< HEAD
              }}
            />
          </div>
          <div className="mobile">
            <Swiper
              items={data}
              uiRenderer={(project, index) => {
                const slug = _get(project, "slug");
                const image = _get(project, "image.banner.mobile");
                const logo = _get(project, "logo");
                const status = _get(project, "tag");
                const captions = _get(project, "captions");
                return (
                  <div className="project-block-wrapper" key={index}>
                    <Link href={`/projects/${slug}`} as={`/projects/${slug}`}>
                      <a>
                        <CardProject
                          image={image}
                          logo={logo}
                          status={status}
                          captions={captions}
                        />
                      </a>
                    </Link>
                  </div>
                );
              }}
            />
=======
                const currentStatus = _find(projectStatus, [
                  "_id",
                  projectStatusId
                ]);
                const fontColorStatus = toCodeColor(
                  _get(currentStatus, "fontColor")
                );
                const backgroundColorStatus = toCodeColor(
                  _get(currentStatus, "backgroundColor")
                );
                let link = `/projects/${slug}`;
                if (linkOut) {
                  link = linkOut;
                }
                return (
                  <div className="project-block-wrapper" key={index}>
                    <a href={link} target="_blank">
                      <CardProject
                        image={image}
                        logo={logo}
                        status={projectStatusTitle}
                        captions={captions}
                        fontColorStatus={fontColorStatus}
                        backgroundColorStatus={backgroundColorStatus}
                      />
                    </a>
                  </div>
                );
              })}
            </div>
>>>>>>> 53d8fc6c01b2ebbbc4a94d9a832499cf181731e9
          </div>
        </div>
      </div>
    </MyWaypoint>
  );
};

export default SectionRecommended;

SectionRecommended.defaultProps = {
  t: () => {}
};
