import React from "react";
import _get from "lodash/get";
import _map from "lodash/map";
import _toNumber from "lodash/toNumber";
import { Progress } from "antd";
import styled from "styled-components";
import dayjs from "dayjs";
import anime from "animejs";

import { Swiper, BackgroundImage, MyWaypoint } from "..";
import { toCodeColor } from "../../utils";
import { useTheme } from "../../hooks/useTheme";

const SectionProgress = props => {
  const { data, t } = props;
  const topic = t(_get(data, "title"));
  let lastUpdated = _get(data, "updated");
  lastUpdated = dayjs(lastUpdated).format("D MMMM YYYY");
  const constructions = _get(data, "constructions");
  const total = _get(data, "overallPercent");
  const images = _get(data, "gallery");

  const backgroundType = _get(data, "backgroundType");
  const backgroundColor = toCodeColor(_get(data, "backgroundColor"));
  const backgroundImage = _get(data, "backgroundImage");

  const colors = useTheme();

  return (
    <MyWaypoint
      anime={[
        () => {
          anime({
            targets: ".project-section-progress h2",
            opacity: [0, 1],
            translateY: [10, 0],
            duration: 700,
            easing: "easeInOutCubic",
            delay: 50
          });
        }
      ]}
    >
      <Section
        id="section-progress"
        name="section-progress"
        mainColor={colors.mainColor}
        backgroundColor={
          backgroundType === "Color" ? backgroundColor : "#f5f0e9"
        }
      >
        <div className="project-section-progress">
          {backgroundType === "Image" && (
            <div className="background-image-block">
              <BackgroundImage image={backgroundImage} />
            </div>
          )}

          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <div className="left-block">
                  <h2 style={{ opacity: 0, transform: "translateY(10px)" }}>
                    {topic}
                  </h2>
                  <div className="block1">
                    <div className="circle-block">
                      <Progress type="circle" percent={_toNumber(total)} />
                    </div>
                    <div className="detail-block">
                      <p>Project Progress</p>
                      <p>
                        updated : <span>{lastUpdated}</span>
                      </p>
                    </div>
                  </div>
                  <div className="block2">
                    {_map(constructions, (item, index) => {
                      const title = t(_get(item, "title"));
                      const percent = _get(item, "percent");
                      return (
                        <div className="progress-item" key={index}>
                          <div className="title">
                            <p>{title}</p>
                            <p>{percent}%</p>
                          </div>
                          <div className="progress-line">
                            <Progress
                              strokeLinecap="square"
                              percent={_toNumber(percent)}
                            />
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
              <div className="col-md-6" style={{ alignSelf: "flex-end" }}>
                <div className="right-block">
                  <Swiper
                    items={images}
                    uiRenderer={(item, index) => {
                      const image = _get(item, "url");
                      return (
                        <div className="item" key={index}>
                          <BackgroundImage image={image} />
                        </div>
                      );
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Section>
    </MyWaypoint>
  );
};

export default SectionProgress;

const Section = styled.div`
  .project-section-progress {
    position: relative;
    background-color: ${props => props.backgroundColor};
    padding: 80px 0;
    @media (max-width: 540px) {
      padding: 50px 0;
    }
    .background-image-block {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
    .left-block {
      h2 {
        font-family: TrajanPro-Bold;
        font-size: 54px;
        line-height: 1.1;
        text-transform: uppercase;
        color: ${props => props.mainColor};
        @media (max-width: 820px) {
          font-size: 40px;
        }
        @media (max-width: 540px) {
          font-size: 30px;
          text-align: center;
        }
      }
      .block1 {
        margin: 50px 0;
        display: flex;
        align-items: center;
        @media (max-width: 540px) {
          flex-direction: column;
          margin: 40px 0;
        }
        .circle-block {
          .ant-progress-inner {
            width: 200px;
            height: 200px;
            @media (max-width: 540px) {
              width: 160px;
              height: 160px;
            }
          }
          .ant-progress-circle-path {
            stroke: ${props => props.mainColor} !important;
          }
          .ant-progress-circle-trail {
            stroke: #e3e3e3 !important;
          }
          .ant-progress-text {
            font-family: DBHelvethaicaX-55Regular;
            font-size: 60px;
            color: ${props => props.mainColor};
            @media (max-width: 540px) {
              font-size: 40px;
            }
          }
        }
        .detail-block {
          margin-left: 40px;
          @media (max-width: 540px) {
            margin-left: 0;
            margin-top: 30px;
          }
          p {
            font-family: DBHelvethaicaX-55Regular;
            font-size: 24px;
            color: ${props => props.mainColor};
            text-transform: uppercase;
            @media (max-width: 540px) {
              text-align: center;
            }
            span {
              font-family: DBHelvethaicaX-Li;
              font-size: 24px;
              color: #2c2f3c;
              text-transform: capitalize;
            }
          }
        }
      }
      .block2 {
        .progress-item {
          margin-bottom: 10px;
          @media (max-width: 540px) {
            margin-bottom: 5px;
          }
          .title {
            display: flex;
            justify-content: space-between;
            p {
              font-family: DBHelvethaicaX-55Regular;
              font-size: 22px;
              color: ${props => props.mainColor};
              text-transform: uppercase;
            }
          }
          .progress-line {
            .ant-progress-inner {
              border-radius: 0px;
              background: #e3e3e3;
            }
            .ant-progress-outer {
              margin: 0;
              padding: 0;
            }
            .ant-progress-text {
              display: none;
            }
            .ant-progress-success-bg,
            .ant-progress-bg {
              background-color: ${props => props.mainColor};
              @media (max-width: 540px) {
                height: 6px !important;
              }
            }
          }
        }
      }
    }
    .right-block {
      @media (max-width: 540px) {
        margin: 0 -30px;
        margin-top: 30px;
      }
      .item {
        width: 100%;
        height: 460px;
        @media (max-width: 820px) {
          height: 260px;
        }
        @media (max-width: 540px) {
          height: 226px;
        }
      }
      .swiper-container {
        padding-bottom: 80px;
        @media (max-width: 540px) {
          padding-bottom: 40px;
        }
      }
      .swiper-pagination {
        bottom: 40px;
        text-align: left;
        @media (max-width: 540px) {
          bottom: 0;
          text-align: center;
        }

        .swiper-pagination-bullet {
          background: #2c2f3c;
        }
        .swiper-pagination-bullet-active {
          /* background: #ed5828; */
          background-color: ${props => props.mainColor};
        }
      }
    }
  }
`;
