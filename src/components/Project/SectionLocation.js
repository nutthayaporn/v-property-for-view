import React, { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import _get from "lodash/get";
import _map from "lodash/map";
import _filter from "lodash/filter";
import anime from "animejs";

const Lightbox = dynamic(() => import("react-image-lightbox"), {
  ssr: false
});

import { useTheme } from "../../hooks/useTheme";
import {
  Image,
  BackgroundImage,
  GoogleMaps,
  MyWaypoint,
  Icon
} from "../../components";
import { scrollToElement } from "../../utils";

const SectionLocation = props => {
  const { data, t } = props;
  const [activeType, setActive] = useState("Graphic");
  const [openZoomImage, setOpenZoomImage] = useState(false);
  const [width, setWidth] = useState(0);

  useEffect(() => {
    setWidth(document.body.width || window.innerWidth);
  }, [width]);

  const lat = _get(data, "lat");
  const lng = _get(data, "lng");
  const graphicMaps = _get(data, "graphicMap");
  const description = t(_get(data, "description"));
  let locations = _get(data, "locations");
  locations = _filter(locations, ["status", "Show"]);
  const locationPin = _get(data, "locationPin");
  const title = t(_get(data, "title"));

  const colors = useTheme();

  return (
    <MyWaypoint
      anime={[
        // () => {
        //   anime({
        //     targets: ".project-section-location h2",
        //     opacity: [0, 1],
        //     translateY: [10, 0],
        //     duration: 700,
        //     easing: "easeInOutCubic",
        //     delay: 50
        //   });
        // },
        () => {
          anime({
            targets: ".project-section-location .description",
            opacity: [0, 1],
            duration: 1000,
            easing: "easeInOutCubic",
            delay: 100
          });
        },
        () => {
          anime({
            targets: ".project-section-location .location-point-block",
            opacity: [0, 1],
            translateX: [10, 0],
            duration: 1000,
            easing: "easeInOutCubic",
            delay: 150
          });
        }
      ]}
    >
      <div className="project-section-location">
        <div className="row no-gutters">
          <div className="col-12 col-lg-7">
            <div className="left-block">
              <div className="maps-block">
                {activeType === "Google" ? (
                  <div style={{ width: "100%", height: "100%" }}>
                    <GoogleMaps
                      lat={lat}
                      lng={lng}
                      logo={locationPin}
                      pinSize={60}
                      defaultZoom={16}
                    />
                  </div>
                ) : (
                  <>
                    <BackgroundImage image={graphicMaps} />
                    <div className="icon-zoom" onClick={() => setOpenZoomImage(true)}>
                      <Icon type="zoom-in" style={{ color: colors.mainColor, fontSize: 20 }}/>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
          <div
            className="col-12 col-lg-5"
            style={{ backgroundColor: "#ffffff" }}
          >
            <div className="container">
              <div className="right-block">
                <h2
                  style={{
                    color: colors.mainColor
                    // opacity: 0,
                    // transform: "translateY(10px)"
                  }}
                >
                  {title}
                </h2>
                <div className="description" style={{ opacity: 0 }}>
                  {description}
                </div>
                <div
                  className="location-point-block"
                  style={{ opacity: 0, transform: "translateX(10)" }}
                >
                  {_map(locations, (point, index) => {
                    const title = t(_get(point, "title"));
                    const image = _get(point, "image");
                    return (
                      <div className="point-block" key={index}>
                        <Image src={image} />
                        <div dangerouslySetInnerHTML={{ __html: title }} />
                      </div>
                    );
                  })}
                </div>
                {openZoomImage && (
                  <Lightbox
                    mainSrc={graphicMaps}
                    clickOutsideToClose={true}
                    enableZoom={true}
                    onCloseRequest={() => {
                      setOpenZoomImage(false);
                    }}
                  />
                )}
                <div className="button-group">
                  <div
                    className="button"
                    style={{
                      borderColor: colors.mainColor,
                      color:
                        activeType === "Graphic"
                          ? colors.buttonTextColor
                          : colors.mainColor,
                      backgroundColor:
                        activeType === "Graphic" ? colors.mainColor : ""
                    }}
                    onClick={() => {
                      setActive("Graphic");
                      if (width < 820) {
                        scrollToElement(".project-section-location");
                      }
                    }}
                  >
                    Graphic map
                  </div>
                  <div
                    className="button"
                    style={{
                      borderColor: colors.mainColor,
                      color:
                        activeType === "Google"
                          ? colors.buttonTextColor
                          : colors.mainColor,
                      backgroundColor:
                        activeType === "Google" ? colors.mainColor : ""
                    }}
                    onClick={() => {
                      setActive("Google");
                      if (width < 820) {
                        scrollToElement(".project-section-location");
                      }
                    }}
                  >
                    Google maps
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </MyWaypoint>
  );
};

export default SectionLocation;
