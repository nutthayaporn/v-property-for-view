import React, { useRef, useState } from "react";
import { Form, Input, Button, Select, Icon } from "antd";
import _get from "lodash/get";
import _map from "lodash/map";
import _find from "lodash/find";
import _split from "lodash/split";
import ReCAPTCHA from "react-google-recaptcha";
import styled from "styled-components";
import { useRouter } from "next/router";

import { BackgroundImage, MyWaypoint } from "../../components";
import { toCodeColor } from "../../utils";
import { useTheme } from "../../hooks/useTheme";
import { fetchFormSubmit } from "../../services/formSubmit";

const { Option } = Select;

const SectionRegister = (props) => {
  const [isLoading, setLoading] = useState(false);
  const router = useRouter();
  const recaptchaRef = useRef(null);
  let { data, projectData, emailReceiver, t } = props;
  const { getFieldDecorator, validateFields } = props.form;

  const submit = async (values) => {
    setLoading(true);

    const data = {
      page: "project",
      firstname: _get(values, "firstname"),
      telephone: _get(values, "telephone"),
      email: _get(values, "email"),
      budget: _get(values, "budget"),
      roomtype: _get(values, "roomtype"),
      projectid: _get(values, "projectid"),
      projectname: _get(values, "projectname"),
      to: _get(emailReceiver, "to"),
      cc: _get(emailReceiver, "cc"),
      bcc: _get(emailReceiver, "bcc"),
      utm_source: _get(router, "query.utm_source"),
      utm_medium: _get(router, "query.utm_medium"),
      utm_campaign: _get(router, "query.utm_campaign"),
    };
    const recaptchaValue = recaptchaRef.current.getValue();
    if (!recaptchaValue) {
      return;
    }
    const result = await fetchFormSubmit(data);
    if (result) {
      if (_get(result, "status") === "success") {
        const asPath = _get(router, "asPath");
        const path = _split(asPath, "?");
        const pathBase = _get(path, 0);
        const pathQuery = _get(path, 1);

        let url = `${pathBase}/thankyou`;
        if (pathQuery) {
          url += `?${pathQuery}`;
        }
        router.push(url);
      }
    } else {
      setLoading(false);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        submit(values);
      }
    });
  };

  const backgroundType = _get(data, "backgroundType");
  const backgroundColor = toCodeColor(_get(data, "backgroundColor"));
  const textColor = toCodeColor(_get(data, "textColor"));
  const backgroundImage = _get(data, "backgroundImage");
  const title = t(_get(data, "title"));
  const budgets = _get(data, "budgets");
  const roomTypes = _get(data, "roomTypes");

  const projectId = _get(projectData, "_id");
  const projectName = _get(projectData, "name.en");

  const colors = useTheme();

  return (
    // <MyWaypoint
    //   anime={[
    //     () => {
    //       anime({
    //         targets: ".project-section-register h2",
    //         opacity: [0, 1],
    //         translateY: [10, 0],
    //         duration: 700,
    //         easing: "easeInOutCubic",
    //         delay: 50
    //       });
    //     }
    //   ]}
    // >
    <Section
      backgroundColor={backgroundType === "Color" ? backgroundColor : "#ffffff"}
      textColor={textColor}
      buttonTextColor={colors.buttonTextColor}
      mainColor={colors.mainColor}
    >
      <div className="project-section-register">
        {backgroundType === "Image" && (
          <div className="background-image-block">
            <BackgroundImage image={backgroundImage} />
          </div>
        )}
        <div className="container">
          <div className="container-topic">
            <div className="topic-block">
              <h2
                style={{
                  color: colors.mainColor,
                  // opacity: 0,
                  // transform: "translateY(10px)"
                }}
              >
                {title}
              </h2>
            </div>
          </div>
          <div className="form-block">
            <Form onSubmit={handleSubmit} className="register-form">
              {getFieldDecorator("projectid", {
                initialValue: projectId,
              })(<Input type="hidden" />)}
              {getFieldDecorator("projectname", {
                initialValue: projectName,
              })(<Input type="hidden" />)}
              <Form.Item label="Name" colon={false}>
                {getFieldDecorator("firstname", {
                  rules: [
                    { required: true, message: "Please input your name" },
                  ],
                })(<Input />)}
              </Form.Item>
              <Form.Item label="Mobile" colon={false}>
                {getFieldDecorator("telephone", {
                  rules: [
                    { required: true, message: "Please input your mobile" },
                  ],
                })(<Input />)}
              </Form.Item>
              <Form.Item label="Email" colon={false}>
                {getFieldDecorator("email", {
                  rules: [
                    {
                      type: "email",
                      message: "The input is not valid email",
                    },
                    { required: true, message: "Please input your email" },
                  ],
                })(<Input />)}
              </Form.Item>
              <div className="row">
                <div className="col-md-6">
                  <Form.Item label="Budget" colon={false}>
                    {getFieldDecorator("budget", {
                      rules: [
                        { required: true, message: "This field is required" },
                      ],
                    })(
                      <Select
                        suffixIcon={
                          <Icon
                            type="caret-down"
                            style={{ color: textColor }}
                          />
                        }
                      >
                        {_map(budgets, (budget, index) => {
                          const title = _get(budget, "title");
                          // const value = _get(budget, "key");
                          return (
                            <Option value={title} key={index}>
                              {title}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-6">
                  <Form.Item label="Room Type" colon={false}>
                    {getFieldDecorator("roomtype", {
                      rules: [
                        { required: true, message: "This field is required" },
                      ],
                    })(
                      <Select
                        suffixIcon={
                          <Icon
                            type="caret-down"
                            style={{ color: textColor }}
                          />
                        }
                      >
                        {_map(roomTypes, (roomType, index) => {
                          const title = _get(roomType, "title");
                          // const value = _get(roomType, "key");
                          return (
                            <Option value={title} key={index}>
                              {title}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>
              <Form.Item>
                <div className="recapta-block">
                  <ReCAPTCHA
                    ref={recaptchaRef}
                    sitekey={process.env.googleRecaptaKey}
                  />
                </div>
              </Form.Item>

              <Form.Item style={{ textAlign: "center" }}>
                <Button
                  loading={isLoading}
                  type="primary"
                  htmlType="submit"
                  style={{ background: colors.mainColor }}
                >
                  {t("Register")}
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </Section>
    // </MyWaypoint>
  );
};

SectionRegister.getInitialProps = async () => ({});

SectionRegister.defaultProps = {};

const WrappedRegisterForm = Form.create({ name: "register" })(SectionRegister);

export default WrappedRegisterForm;

const Section = styled.div`
  .project-section-register {
    position: relative;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 40px 0;
    background-color: ${(props) => props.backgroundColor};
  }

  .background-image-block {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  .container-topic {
    position: relative;
    .topic-block {
      h2 {
        font-family: TrajanPro-Regular;
        font-size: 54px;
        text-align: center;
        text-transform: uppercase;
        @media (max-width: 820px) {
          font-size: 40px;
        }
        @media (max-width: 680px) {
          font-size: 30px;
        }
      }
    }
    .sub-topic-block {
      p {
        font-family: Gotham-Medium;
        font-size: 24px;
        font-weight: 500;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: center;
        color: #2c2f3c;
      }
    }
  }

  .form-block {
    width: 100%;
    max-width: 663px;
    margin: auto;
    margin-top: 40px;
    .ant-form-item {
      &:last-child {
        margin-bottom: 0;
        @media (max-width: 680px) {
          margin-bottom: 24px;
        }
      }
    }
    .register-form {
      label {
        font-family: DBHelvethaicaX-55Regular;
        font-size: 22px;
        color: #ffffff;
        color: ${(props) => props.textColor};
        text-transform: uppercase;
        @media (max-width: 680px) {
          font-size: 20px;
        }
        &:before {
          display: none;
        }
      }
      input {
        background: none;
        border: 0;
        border-bottom: 2px solid #979797;
        border-color: ${(props) => props.textColor};
        border-radius: 0;
        font-family: DBHelvethaicaX-55Regular;
        font-size: 24px;
        color: #ffffff;
        color: ${(props) => props.textColor};
        padding: 0;
        padding-left: 10px;
        &:focus {
          box-shadow: none;
        }
        @media (max-width: 680px) {
          font-size: 20px;
        }
      }
      .ant-select-selection {
        background: transparent;
        border: none;
        border-bottom: 2px solid #979797;
        border-color: ${(props) => props.textColor};
        border-radius: 0;
        box-shadow: none;
        .ant-select-selection-selected-value {
          font-family: DBHelvethaicaX-55Regular;
          font-size: 24px;
          color: #ffffff;
          color: ${(props) => props.textColor};
        }
        .ant-select-arrow {
          color: #979797;
        }
      }
      button {
        width: 203px;
        height: 48px;
        background-color: #f05a28;
        background-color: ${(props) => props.mainColor};
        border: 0;
        border-radius: 0;
        span {
          font-family: DBHelvethaicaX-55Regular;
          font-size: 24px;
          /* color: #ffffff; */
          color: ${(props) => props.buttonTextColor};
          text-transform: uppercase;
        }
      }
      .ant-form-explain {
        font-family: DBHelvethaicaX-55Regular;
        font-size: 20px;
        line-height: 1;
      }
      .recapta-block {
        display: flex;
        justify-content: center;
        margin-top: 30px;
        @media (max-width: 680px) {
          margin-top: 20px;
        }
      }
    }
  }
`;
