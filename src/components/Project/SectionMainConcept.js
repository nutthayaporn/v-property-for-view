import React, { useState, useEffect } from "react";
import _get from "lodash/get";
import _map from "lodash/map";
import _split from "lodash/split";
import anime from "animejs";

import { useTheme } from "../../hooks/useTheme";
import { BackgroundImage, MyWaypoint } from "../../components";
import { toCodeColor } from "../../utils";

const SectionMainConcept = props => {
  const { data, t } = props;
  const [heightConceptBlock, setHeightConceptBlock] = useState(0);

  const main = _get(data, "main-concepts[0]");
  const status = _get(main, "status");
  if (status !== "Show") return null;

  useEffect(() => {
    const mainConceptBlockHeight = document.querySelector(
      ".description-block-main-concept"
    ).offsetHeight;

    setHeightConceptBlock(mainConceptBlockHeight);
  }, [heightConceptBlock]);

  const backgroundType = _get(data, "backgroundType");
  const backgroundImage = _get(data, "backgroundImage");
  const backgroundColor = toCodeColor(_get(data, "backgroundColor"));

  const mainImage = _get(main, "image");
  const title = t(_get(main, "title"));
  const subTitle = t(_get(main, "subTitle"));
  const mainDescription = t(_get(main, "description"));
  const mainDescriptionPosition = _get(main, "descriptionPosition");

  const bottom = -heightConceptBlock + heightConceptBlock / 2;
  let styleMainDescription = { left: 0, bottom: bottom };
  if (mainDescriptionPosition === "Mid") {
    styleMainDescription = {
      left: 0,
      right: 0,
      margin: "auto",
      bottom: bottom
    };
  } else if (mainDescriptionPosition === "Right") {
    styleMainDescription = { left: "auto", right: "0", bottom: bottom };
  }

  const colors = useTheme();

  return (
    <div
      className="project-section-concept"
      style={{
        backgroundColor:
          backgroundType === "Color" ? backgroundColor : "#ffffff"
      }}
    >
      <div className="desktop">
        <div className="topic-block">
          {backgroundType === "Image" && (
            <div className="background-block">
              <BackgroundImage image={backgroundImage} />
            </div>
          )}
          <div className="container">
            <MyWaypoint
              anime={[
                () => {
                  anime({
                    targets: ".project-section-concept .topic-block h2",
                    opacity: [0, 1],
                    translateY: [10, 0],
                    duration: 700,
                    easing: "easeInOutCubic",
                    delay: 50
                  });
                },
                () => {
                  anime({
                    targets: ".project-section-concept .topic-block .sub-title",
                    opacity: [0, 1],
                    translateY: [10, 0],
                    duration: 700,
                    easing: "easeInOutCubic",
                    delay: 180
                  });
                }
              ]}
            >
              <div className="title-block">
                <h2
                  style={{
                    color: colors.mainColor,
                    opacity: 0,
                    transform: "translateY(10px)"
                  }}
                >
                  {title}
                </h2>
                <p
                  className="sub-title"
                  style={{ opacity: 0, transform: "translateY(10px)" }}
                >
                  {subTitle}
                </p>
                <div className="line" />
                <div className="image1">
                  <BackgroundImage image={mainImage} />
                  <div
                    className="description-block-main-concept"
                    style={styleMainDescription}
                  >
                    <MyWaypoint
                      anime={[
                        () => {
                          anime({
                            targets:
                              ".project-section-concept .description-block-main-concept .description",
                            opacity: [0, 1],
                            translateX: [-30, 0],
                            duration: 1000,
                            easing: "easeInOutCubic"
                          });
                        }
                      ]}
                    >
                      <p
                        className="description"
                        style={{ opacity: 0, transform: "translateX(-30px)" }}
                      >
                        {mainDescription}
                      </p>
                    </MyWaypoint>
                  </div>
                </div>
              </div>
            </MyWaypoint>
          </div>
        </div>
      </div>
      <div className="mobile">
        <div className="topic-block">
          <div className="block1">
            <h2 style={{ color: colors.mainColor }}>{title}</h2>
            <p className="sub-title">{subTitle}</p>
            <div className="line" />
          </div>
          <div className="image-block">
            <BackgroundImage image={mainImage} />
          </div>
          <div className="description-block">
            <div className="container">
              <p className="description">{mainDescription}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionMainConcept;
