import React, { useState, useEffect } from "react";
import _get from "lodash/get";
import _map from "lodash/map";
import _filter from "lodash/filter";
import _sortBy from "lodash/sortBy";
import { Select } from "antd";
import posed, { PoseGroup } from "react-pose";
import dynamic from "next/dynamic";

const { Option } = Select;

import { BackgroundImage, Icon, Image } from "../../../components";
import { toCodeColor } from "../../../utils";

const Lightbox = dynamic(() => import("react-image-lightbox"), {
  ssr: false
});

const SelectFloorPlan = props => {
  const { t, colors, data } = props;
  const [openZoomImage, setOpenZoomImage] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const [activeIndexMasterPlan, setActiveIndexMasterPlan] = useState(undefined);
  const [activeIndexFloorplan, setActiveIndexFloorplan] = useState(undefined);
  const [activeIndexRoomtype, setActiveIndexRoomtype] = useState(undefined);

  let floors = _get(data, "floor-plans");
  floors = _filter(floors, ["status", "Show"]);
  floors = _sortBy(floors, ["order"]);

  let roomTypes = _get(data, "unit-types");
  roomTypes = _filter(roomTypes, ["status", "Show"]);
  roomTypes = _sortBy(roomTypes, ["order"]);

  let masterplans = _get(data, "master-plans");
  masterplans = _filter(masterplans, ["status", "Show"]);
  masterplans = _sortBy(masterplans, ["order"]);

  useEffect(() => {
    setActiveIndexMasterPlan(0);
    const masterplanImage = _get(masterplans, `0.image`);
    setSelectedImage(masterplanImage);
  }, []);

  const backgroundType = _get(data, "backgroundType");
  const backgroundColor = toCodeColor(_get(data, "backgroundColor"));
  const backgroundImage = _get(data, "backgroundImage");

  const topic = t(_get(data, "title"));

  const handleChange = (index, type) => {
    let image = "";
    if (type === "floorplan") {
      setActiveIndexMasterPlan(undefined);
      setActiveIndexFloorplan(index);
      setActiveIndexRoomtype(undefined);
      image = _get(floors, `${index}.image`);
    } else if (type === "roomtype") {
      setActiveIndexMasterPlan(undefined);
      setActiveIndexFloorplan(undefined);
      setActiveIndexRoomtype(index);
      image = _get(roomTypes, `${index}.image`);
    } else if (type === "masterplan") {
      setActiveIndexMasterPlan(index);
      setActiveIndexFloorplan(undefined);
      setActiveIndexRoomtype(undefined);
      image = _get(masterplans, `${index}.image`);
    }
    setSelectedImage(image);
  };

  const Shade = posed.div({
    enter: { opacity: 1 },
    exit: { opacity: 0 }
  });

  return (
    <div
      className="project-section-floor-plan"
      style={{
        backgroundColor:
          backgroundType === "Color" ? backgroundColor : "#ffffff"
      }}
    >
      {backgroundType === "Image" && (
        <div className="background-image-block">
          <BackgroundImage image={backgroundImage} />
        </div>
      )}
      <div className="container">
        <h2
          style={{
            color: colors.mainColor,
            opacity: 0,
            transform: "translateY(10px)"
          }}
        >
          {topic}
        </h2>
        {openZoomImage && (
          <Lightbox
            mainSrc={selectedImage}
            clickOutsideToClose={true}
            enableZoom={true}
            onCloseRequest={() => {
              setOpenZoomImage(false);
            }}
          />
        )}
        <div className="container-button-group">
          <div className="row" style={{ width: "100%" }}>
            <div className="col-md-4">
              <div className="select-plan-block">
                <Select
                  placeholder="Master plan"
                  value={activeIndexMasterPlan}
                  defaultValue={activeIndexMasterPlan}
                  suffixIcon={
                    <Icon type="arrowDown" color={colors.mainColor} />
                  }
                  onChange={index => handleChange(index, "masterplan")}
                >
                  {_map(masterplans, (masterplan, index) => {
                    const title = t(_get(masterplan, "title"));
                    return (
                      <Option value={index} key={index}>
                        {title}
                      </Option>
                    );
                  })}
                </Select>
              </div>
            </div>
            <div className="col-md-4">
              <div className="select-plan-block">
                <Select
                  placeholder="Floor plan"
                  value={activeIndexFloorplan}
                  suffixIcon={
                    <Icon type="arrowDown" color={colors.mainColor} />
                  }
                  onChange={index => handleChange(index, "floorplan")}
                >
                  {_map(floors, (floor, index) => {
                    const title = t(_get(floor, "title"));
                    return (
                      <Option value={index} key={index}>
                        {title}
                      </Option>
                    );
                  })}
                </Select>
              </div>
            </div>
            <div className="col-md-4">
              <div className="select-plan-block">
                <Select
                  placeholder="Room Types"
                  value={activeIndexRoomtype}
                  suffixIcon={
                    <Icon type="arrowDown" color={colors.mainColor} />
                  }
                  onChange={index => handleChange(index, "roomtype")}
                >
                  {_map(roomTypes, (roomType, index) => {
                    const title = t(_get(roomType, "title"));
                    return (
                      <Option value={index} key={index}>
                        {title}
                      </Option>
                    );
                  })}
                </Select>
              </div>
            </div>
          </div>
        </div>
        <div className="image-floor-plan-block">
          <PoseGroup>
            <Shade key={selectedImage}>
              <Image key={selectedImage} src={selectedImage} alt="" onClick={() => setOpenZoomImage(true)}/>
            </Shade>
          </PoseGroup>
        </div>
      </div>
    </div>
  );
};

export default SelectFloorPlan;
