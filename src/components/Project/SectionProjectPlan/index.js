import React from "react";
import _get from "lodash/get";
import _map from "lodash/map";
import _filter from "lodash/filter";
import _sortBy from "lodash/sortBy";
import anime from "animejs";

import { useTheme } from "../../../hooks/useTheme";
import { MyWaypoint } from "../../../components";
import SelectFloorPlan from "./SelectFloorPlan";

const SectionProjectPlan = props => {
  const { t, data } = props;
  const colors = useTheme();

  return (
    <MyWaypoint
      anime={[
        () => {
          anime({
            targets: ".project-section-floor-plan h2",
            opacity: [0, 1],
            translateY: [10, 0],
            duration: 700,
            easing: "easeInOutCubic",
            delay: 50
          });
        }
      ]}
    >
      <SelectFloorPlan colors={colors} data={data} t={t} />
    </MyWaypoint>
  );
};

export default SectionProjectPlan;
