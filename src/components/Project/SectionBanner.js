import React from "react";
import _get from "lodash/get";
import _size from "lodash/size";
import _map from "lodash/map";
import _filter from "lodash/filter";

import {
  BackgroundImage,
  Swiper,
  MouseScrollWheel,
  Image,
  MyWaypoint,
} from "..";
import { scrollToElement, toCodeColor } from "../../utils";

const SectionBanner = (props) => {
  const { data, t } = props;
  let banners = _get(data, "banners");
  banners = _filter(banners, ["status", "Show"]);
  banners = _map(banners, (banner) => {
    return {
      ...banner,
      pcResource: _get(banner, "pcResource"),
      mobileResource: _get(banner, "mobileResource"),
      logo: _get(banner, "logo"),
      mediaType: _get(banner, "resourceType"),
      status: _get(banner, "status"),
      buttonStatus: _get(banner, "buttonStatus"),
      buttonBackgroundColor: _get(banner, "buttonBackgroundColor"),
      buttonTextColor: _get(banner, "buttonTextColor"),
      overlayPosition: _get(banner, "overlayPosition"),
      overlayTextColor: _get(banner, "overlayTextColor"),
      overlayBackgroundColor: _get(banner, "overlayBackgroundColor"),
      pcOverlayImage: _get(banner, "pcOverlayImage"),
    };
  });

  const renderMobile = (item, key) => {
    const mediaType = _get(item, "mediaType");
    const media = _get(item, "mobileResource");
    const logo = _get(item, "logo");
    const title = t(_get(item, "description1"));
    const description2 = t(_get(item, "description2"));
    const description3 = t(_get(item, "description3"));
    const description4 = t(_get(item, "description4"));

    const buttonStatus = _get(item, "buttonStatus");
    const buttonTextColor = toCodeColor(_get(item, "buttonTextColor"));
    const buttonBackgroundColor = toCodeColor(
      _get(item, "buttonBackgroundColor")
    );

    const overlayStatus = _get(item, "overlayStatus");
    const overlayTextColor = toCodeColor(_get(item, "overlayTextColor"));
    const overlayBackgroundColor = toCodeColor(
      _get(item, "overlayBackgroundColor")
    );
    const mobileOverlayImage = _get(item, "mobileOverlayImage");
    if (mobileOverlayImage) {
      return (
        <div className="mobile-overlay-image-block" key={key}>
          <BackgroundImage image={mobileOverlayImage} />
        </div>
      );
    }
    return (
      <div className="project-image-banner-block-mobile" key={key}>
        {mediaType === "Image" && <Image src={media} />}
        {overlayStatus === "Show" && (
          <div
            className="mobile-detail"
            style={{ backgroundColor: overlayBackgroundColor }}
          >
            <div className="container">
              <div className="project-banner-detail-block">
                <div className="logo-block">
                  <Image src={logo} alt="V Property Logo" />
                </div>
                <div className="description-block">
                  <div className="title" style={{ color: overlayTextColor }}>
                    {title}
                  </div>
                  <div
                    className="description"
                    style={{ color: overlayTextColor }}
                  >
                    {description2}
                  </div>
                  <div
                    className="description"
                    style={{ color: overlayTextColor }}
                  >
                    {description3}
                  </div>
                  <div
                    className="description"
                    style={{ color: overlayTextColor }}
                  >
                    {description4}
                  </div>
                  {buttonStatus && (
                    <div className="button-block">
                      <div
                        className="button-primary"
                        onClick={() =>
                          scrollToElement(".project-section-register")
                        }
                        style={{
                          backgroundColor: buttonBackgroundColor,
                        }}
                      >
                        <div
                          className="background"
                          style={{
                            backgroundColor: buttonBackgroundColor,
                          }}
                        />
                        <p style={{ color: buttonTextColor }}>Register</p>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  };

  const renderDesktop = (item, key) => {
    const mediaType = _get(item, "mediaType");
    const media = _get(item, "pcResource");
    const logo = _get(item, "logo");
    const title = t(_get(item, "description1"));
    const description2 = t(_get(item, "description2"));
    const description3 = t(_get(item, "description3"));
    const description4 = t(_get(item, "description4"));

    const buttonStatus = _get(item, "buttonStatus");
    const buttonTextColor = toCodeColor(_get(item, "buttonTextColor"));
    const buttonBackgroundColor = toCodeColor(
      _get(item, "buttonBackgroundColor")
    );

    const overlayStatus = _get(item, "overlayStatus");
    const overlayTextColor = toCodeColor(_get(item, "overlayTextColor"));
    const overlayBackgroundColor = toCodeColor(
      _get(item, "overlayBackgroundColor")
    );
    const overlayPosition = _get(item, "overlayPosition");
    const pcOverlayImage = _get(item, "pcOverlayImage");

    let styleOverlay = {
      left: 30,
      right: "auto",
      backgroundColor: overlayBackgroundColor,
    };
    if (overlayPosition === "Mid") {
      styleOverlay = {
        left: 0,
        right: 0,
        margin: "auto",
        backgroundColor: overlayBackgroundColor,
      };
    } else if (overlayPosition === "Right") {
      styleOverlay = {
        left: "auto",
        right: 30,
        backgroundColor: overlayBackgroundColor,
      };
    }
    return (
      <div className="project-image-banner-block" key={key}>
        {mediaType === "Image" && <BackgroundImage image={media} />}
        {overlayStatus === "Show" && (
          <div className="project-banner-detail-block" style={styleOverlay}>
            <div className="logo-block">
              <Image src={logo} alt="V Property Logo" />
            </div>
            <div className="description-block">
              <div className="title">{title}</div>
              <div className="description" style={{ color: overlayTextColor }}>
                {description2}
              </div>
              <div className="description" style={{ color: overlayTextColor }}>
                {description3}
              </div>
              <div className="description" style={{ color: overlayTextColor }}>
                {description4}
              </div>
              {buttonStatus === "Show" && (
                <div className="button-block">
                  <div
                    className="button-primary"
                    onClick={() => scrollToElement(".project-section-register")}
                    style={{ backgroundColor: buttonBackgroundColor }}
                  >
                    <div
                      className="background"
                      style={{
                        backgroundColor: buttonBackgroundColor,
                      }}
                    />

                    <p style={{ color: buttonTextColor }}>Register</p>
                  </div>
                </div>
              )}
            </div>
            {pcOverlayImage && (
              <div className="overlay-image-block">
                <Image src={pcOverlayImage} />
              </div>
            )}
          </div>
        )}

        <div className="mouse-scroll-block">
          <MouseScrollWheel toElement=".project-section-register" />
        </div>
      </div>
    );
  };

  return (
    <div className="project-section-banner">
      <div className="desktop">
        {_size(banners) > 1 ? (
          <Swiper
            items={banners}
            key={_size(banners)}
            uiRenderer={(item, key) => {
              return renderDesktop(item, key);
            }}
          />
        ) : (
          renderDesktop(_get(banners, 0), 0)
        )}
      </div>
      <div className="mobile">
        {_size(banners) > 1 ? (
          <Swiper
            items={banners}
            key={_size(banners)}
            uiRenderer={(item, key) => {
              return renderMobile(item, key);
            }}
          />
        ) : (
          renderMobile(_get(banners, 0), 0)
        )}
      </div>
    </div>
  );
};

export default SectionBanner;
