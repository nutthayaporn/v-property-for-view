import React, { useState, useEffect } from "react";
import _get from "lodash/get";
import _map from "lodash/map";
import _isEmpty from "lodash/isEmpty";
import _split from "lodash/split";
import _filter from "lodash/filter";
import _sortBy from "lodash/sortBy";
import _compact from "lodash/compact";
import anime from "animejs";

import { useTheme } from "../../hooks/useTheme";
import { BackgroundImage, Image, MyWaypoint } from "../../components";
import { toCodeColor } from "../../utils";

const SectionConcept = props => {
  const { data, t } = props;

  const startPosition = _get(data, "start");
  const isImageLeft = startPosition === "Left";
  let concepts = _get(data, "concepts");
  concepts = _filter(concepts, ["status", "Show"]);
  concepts = _sortBy(concepts, ["order"]);

  const colors = useTheme();

  return (
    <div className="project-section-concept">
      <div className="desktop">
        {_map(concepts, (concept, index) => {
          const backgroundType = _get(concept, "backgroundType");
          const backgroundColor = toCodeColor(_get(concept, "backgroundColor"));
          const backgroundImage = _get(concept, "background");

          let name = t(_get(concept, "title"));
          name = _split(name, " ");
          name = _compact(name);
          const title = t(_get(concept, "subTitle"));
          const image = _get(concept, "image");
          const description = t(_get(concept, "description"));
          const type = _get(concept, "type");
          let facilities = _get(concept, "textIcons");
          facilities = _filter(facilities, facility => !_isEmpty(facility));
          const editorDescription = t(_get(concept, "editorDescription"));

          const isEven = index % 2 === 0;
          let isReversed = true;
          if (isImageLeft) {
            if (isEven) {
              isReversed = false;
            } else {
              isReversed = true;
            }
          } else {
            if (isEven) {
              isReversed = true;
            } else {
              isReversed = false;
            }
          }

          return (
            <MyWaypoint
            key={index}
              isDefaultOpacity={false}
              anime={[
                () => {
                  anime({
                    targets: `.concept-block-${index} .content-block`,
                    opacity: [0, 1],
                    duration: 700,
                    delay: 50,
                    easing: "easeInOutCubic"
                  });
                }
              ]}
            >
              <div
                key={index}
                className={`concept-block concept-block-${index} ${
                  isReversed
                    ? "concept-block-second concept-block-left-position"
                    : ""
                } ${
                  !isReversed && index === 1
                    ? "concept-block-second-right-position"
                    : ""
                } ${index === 1 ? "concept-block-second" : ""}`}
                style={{
                  backgroundColor:
                    backgroundType === "Color" ? backgroundColor : "#f5f0e9"
                }}
              >
                {backgroundType === "Image" && (
                  <div className="background-concept-block">
                    <BackgroundImage image={backgroundImage} />
                  </div>
                )}
                <div
                  className="row"
                  style={{
                    flexDirection: isReversed ? "row-reverse" : "row"
                  }}
                >
                  <div className="col">
                    <div className="left-block">
                      <div className="image-block">
                        <BackgroundImage image={image} />
                      </div>
                    </div>
                  </div>
                  <div className="col">
                    <div className="right-block">
                      <div
                        className={`topic-concept-block ${
                          _isEmpty(name) ? "full-border" : ""
                        }`}
                      >
                        {_map(name, (val, index) => {
                          return (
                            <h2 key={index} style={{ color: colors.mainColor }}>
                              {val}
                            </h2>
                          );
                        })}
                      </div>

                      <div className="content-block" style={{ opacity: 0 }}>
                        {type === "Project" && (
                          <>
                            <p className="title">{title}</p>
                            <p className="description">{description}</p>
                          </>
                        )}

                        {type === "Facility" &&
                          _map(facilities, (facility, index) => {
                            const title = t(_get(facility, "title"));
                            const description = t(
                              _get(facility, "description")
                            );
                            const image = t(_get(facility, "image"));
                            return (
                              <div className="facility-item" key={index}>
                                <div className="icon-block">
                                  <Image src={image} />
                                </div>
                                <div className="facility-detail-block">
                                  <p className="name">{title}</p>
                                  <p className="description">{description}</p>
                                </div>
                              </div>
                            );
                          })}
                        {type === "Detail" && (
                          <div
                            dangerouslySetInnerHTML={{
                              __html: editorDescription
                            }}
                          />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </MyWaypoint>
          );
        })}
      </div>
      <div className="mobile">
        {_map(concepts, (concept, index) => {
          const backgroundType = _get(concept, "backgroundType");
          const backgroundColor = toCodeColor(_get(concept, "backgroundColor"));
          const backgroundImage = _get(concept, "background");

          let name = t(_get(concept, "title"));
          name = _split(name, " ");
          const title = t(_get(concept, "subTitle"));
          const image = _get(concept, "image");
          const description = t(_get(concept, "description"));
          const type = _get(concept, "type");
          let facilities = _get(concept, "textIcons");
          facilities = _filter(facilities, facility => !_isEmpty(facility));
          const editorDescription = t(_get(concept, "editorDescription"));
          return (
            <div
              className="concept-block"
              key={index}
              style={{
                backgroundColor:
                  backgroundType === "Color" ? backgroundColor : "#f5f0e9"
              }}
            >
              {backgroundType === "Image" && (
                <div className="background-concept-block">
                  <BackgroundImage image={backgroundImage} />
                </div>
              )}

              <div className="block1">
                {_map(name, (val, index) => {
                  return <h2 key={index}>{val}</h2>;
                })}
              </div>
              <div className="image-block">
                <BackgroundImage image={image} />
              </div>
              <div className="description-block">
                <div className="container">
                  {type === "Project" && (
                    <>
                      <p className="title">{title}</p>
                      <p className="description">{description}</p>
                    </>
                  )}
                  {type === "Facility" && (
                    <>
                      {_map(facilities, (facility, index) => {
                        const title = t(_get(facility, "title"));
                        const description = t(_get(facility, "description"));
                        const image = t(_get(facility, "image"));
                        return (
                          <div className="facility-item" key={index}>
                            <Image className="icon" src={image} />
                            <div className="facility-detail-block">
                              <p className="name">{title}</p>
                              <p className="description">{description}</p>
                            </div>
                          </div>
                        );
                      })}
                    </>
                  )}
                  {type === "Detail" && (
                    <div
                      dangerouslySetInnerHTML={{
                        __html: editorDescription
                      }}
                    />
                  )}
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SectionConcept;
