import React from "react";
import _chunk from "lodash/chunk";
import _get from "lodash/get";
import _size from "lodash/size";
import _map from "lodash/map";
import _filter from "lodash/filter";
import styled from "styled-components";
import anime from "animejs";

import { BackgroundImage, MyWaypoint, Gallery } from "../../components";
import { toCodeColor } from "../../utils";
import { useTheme } from "../../hooks/useTheme";

const SectionGallery = props => {
  const { data, t } = props;

  const backgroundType = _get(data, "backgroundType");
  const backgroundColor = toCodeColor(_get(data, "backgroundColor"));
  const backgroundImage = _get(data, "backgroundImage");

  const topic = t(_get(data, "title"));
  let images = _get(data, "galleries");
  images = _filter(images, ["status", "Show"]);

  const colors = useTheme();

  return (
    <MyWaypoint
      anime={[
        () => {
          anime({
            targets: ".project-section-gallery h2",
            opacity: [0, 1],
            translateY: [10, 0],
            duration: 700,
            easing: "easeInOutCubic",
            delay: 50
          });
        }
      ]}
    >
      <Section
        backgroundColor={
          backgroundType === "Color" ? backgroundColor : "rgb(238,237,232)"
        }
      >
        {backgroundType === "Image" && (
          <div className="background-image-block">
            <BackgroundImage image={backgroundImage} />
          </div>
        )}

        <div className="project-section-gallery">
          <h2
            style={{
              color: colors.mainColor,
              opacity: 0,
              transform: "translateY(10px)"
            }}
          >
            {topic}
          </h2>
          <div className="container">
            <Gallery items={images} t={t} color={colors.mainColor} />
          </div>
        </div>
      </Section>
    </MyWaypoint>
  );
};

export default SectionGallery;

const Section = styled.div`
  position: relative;
  background-color: ${props => props.backgroundColor};
  padding: 100px 180px;
  @media (max-width: 820px) {
    padding: 65px 20px;
  }
  @media (max-width: 680px) {
    padding: 50px 0;
  }
  .background-image-block {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  h2 {
    font-family: TrajanPro-Bold;
    font-size: 54px;
    text-align: center;
    text-transform: uppercase;
    margin-bottom: 68px;
    @media (max-width: 820px) {
      font-size: 40px;
    }
    @media (max-width: 680px) {
      font-size: 30px;
      margin-bottom: 40px;
    }
  }
  .project-section-gallery {
    position: relative;
    z-index: 1;
  }
`;
