import React from "react";
import { Menu, Dropdown, Icon } from "antd";
import _get from "lodash/get";
import _filter from "lodash/filter";
import _toUpper from "lodash/toUpper";
import _map from "lodash/map";

import { i18n, Link, withTranslation } from "../i18n";

const LanguageSwitcher = () => {
  const languages = process.env.languages;
  const language = i18n.language;

  const dropdownLanguages = _filter(languages, lang => lang !== language);
  const displayCurrentLanguage = _toUpper(language);
  return (
    <Dropdown
      overlayClassName="language-switcher"
      overlay={
        <Menu
          onClick={({ key }) => {
            i18n.changeLanguage(key);
          }}
        >
          {_map(dropdownLanguages, dropdownLanguage => {
            const displayLanguage = _toUpper(dropdownLanguage);
            return (
              <Menu.Item key={dropdownLanguage}>
                <a aria-label={displayLanguage}>{displayLanguage}</a>
              </Menu.Item>
            );
          })}
        </Menu>
      }
      trigger={["click"]}
    >
      <a aria-label={displayCurrentLanguage} className="ant-dropdown-link">
        {displayCurrentLanguage} <Icon type="caret-down" />
      </a>
    </Dropdown>
  );
};

LanguageSwitcher.getInitialProps = async () => ({
  namespacesRequired: ["common"]
});

export default withTranslation("common")(LanguageSwitcher);
