import React from "react";
import ImageGallery from "react-image-gallery";
import _map from 'lodash/map'
import _get from 'lodash/get'

const GalleryLightBox = (props) => {
  const { images, selectedIndex } = props;

  const items = _map(images, image => {
    const url = _get(image, 'url')
    return { original: url, thumbnail: url };
  });

  const lightBoxSettings = {
    showIndex: false,
    showBullets: true,
    infinite: true,
    showThumbnails: true,
    showFullscreenButton: false,
    showGalleryFullscreenButton: false,
    useBrowserFullscreen: false,
    showNav: true,
    isRTL: false,
    showPlayButton: false,
    slideDuration: 450,
    slideInterval: 7000,
    slideOnThumbnailOver: false,
    thumbnailPosition: "bottom",
    // renderThumbInner: myRenderThumbInner,
    autoPlay: false
    // startIndex: selectedIndex
  };
  return <ImageGallery items={items} {...lightBoxSettings} />;
};

export default GalleryLightBox;
