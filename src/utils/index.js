import fetch from "isomorphic-unfetch";
import _replace from "lodash/replace";
import _get from "lodash/get";
import _isBoolean from "lodash/isBoolean";

const scrollToElement = element => {
  const offsetTop = document.querySelector(element).offsetTop;
  const headerHeight = document.querySelector("header").offsetHeight;
  const top = offsetTop - headerHeight;
  window.scroll({
    top,
    left: 0,
    behavior: "smooth"
  });
};

const toCodeColor = value => {
  const r = _get(value, "r");
  const g = _get(value, "g");
  const b = _get(value, "b");
  const a = _get(value, "a");
  return `rgba(${r}, ${g}, ${b}, ${a})`;
};

const fetchJson = (url, options = {}) => {
  return fetch(url, options)
    .then(response => response.json())
    .catch(error => console.log("fetchJson error", error));
};

const fetchPostJson = (url, body) => {
  return fetchJson(url, {
    method: "POST",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json"
    },
    body
  });
};

const stripHtml = html => {
  if (_isBoolean(html)) return html;
  if (!html) return html;
  return _replace(html, /(<([^>]+)>)/gi, "");
};

export { scrollToElement, fetchJson, toCodeColor, fetchPostJson, stripHtml };
