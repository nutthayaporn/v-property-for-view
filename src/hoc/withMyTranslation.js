import React from "react";
import _find from "lodash/find";
import _get from "lodash/get";
import _isArray from "lodash/isArray";
import _isObject from "lodash/isObject";
import _isEmpty from "lodash/isEmpty";
import _isInteger from "lodash/isInteger";

import { i18n, withTranslation } from "../i18n";
import en from "../../public/static/locales/en/common";
import th from "../../public/static/locales/th/common";

const locales = {
  languages: [
    {
      key: "th",
      data: th
    },
    {
      key: "en",
      data: en
    }
  ]
};

const languages = process.env.languages;
const defaultLanguage = process.env.defaultLanguage;

export default Component => {
  class withMyTranslation extends React.Component {
    static async getInitialProps(ctx) {
      if (Component.getInitialProps) {
        return {
          ...(await Component.getInitialProps(ctx)),
          namespacesRequired: ["common"]
        };
      }
    }
    getLangData = language => {
      const langData = _get(
        _find(locales.languages, { key: language }),
        "data"
      );
      return langData;
    };
    getAnyLanguage = content => {
      let result = content;
      for (let index = 0; index < languages.length; index += 1) {
        const language = languages[index];
        const langData = this.getLangData(language);
        const contentLang = _get(langData, content);
        if (contentLang) {
          result = contentLang;
          break;
        }
      }
      return result;
    };
    getDefaultLanguage = content => {
      const langData = this.getLangData(defaultLanguage);
      const contentLang = _get(langData, content);
      if (!contentLang) return this.getAnyLanguage(content);
      return contentLang;
    };
    getDataFromObject = object => {
      const currentActive = i18n.language;
      let string = _get(object, currentActive, _get(object, defaultLanguage));
      if (!string) string = object[Object.keys(object)[0]];
      return string;
    };
    getDataFromString = content => {
      const currentActive = i18n.language;
      const langData = this.getLangData(currentActive);
      const contentLang = _get(langData, content);
      if (!contentLang) return this.getDefaultLanguage(content);
      return contentLang;
    };
    getDataWithLanguage = content => {
      if (_isArray(content)) return content;

      let string = content;
      if (_isObject(content)) {
        string = this.getDataFromObject(content);
      } else {
        string = this.getDataFromString(content);
      }
      if (string === true || _isInteger(string)) {
      } else {
        if (_isEmpty(string)) return null;
      }
      return string;
    };
    render() {
      return <Component {...this.props} t={this.getDataWithLanguage} />;
    }
  }
  return withTranslation("common")(withMyTranslation);
};
